//Product Slider on Homepage
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:30,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
		540:{
            items:1,
            nav:false
        },
		768:{
            items:2,
            nav:false
        },
        992:{
            items:2,
            nav:false
        },
        1200:{
            items:2,
            nav:true,
            loop:false
        }
    }
})



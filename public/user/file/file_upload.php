<?php
include('../php/function.php');
include('../login/db_info.php');
date_default_timezone_set('Asia/Seoul');


//설정
$uploads_dir = './course_material';
$allowed_ext = array('hwp','doc','docx','ppt','pptx','xls','xlsx','pdf','txt','png', 'PNG', 'jpg','jpeg','gif','mp4', 'PDF');
$field_name = 'selected_file';

//변수정리
$error 		= $_FILES[$field_name]['error'];
$name 		= $_FILES[$field_name]['name'];
$ext 		= array_pop(explode('.',$name));
$file_title = array_shift(explode('.',$name));
$date 		= date('Y-m-d');


//오류 확인
if($error != UPLOAD_ERR_OK) {
	switch($error){
		case UPLOAD_ERR_FORM_SIZE:
			function_alert("File too large. (Maximum file size: 500M) (Error: $error)");
			break;
		case UPLOAD_ERR_PARTIAL:
			function_alert("Error: File uploaded partially. (error: $error)");
			break;
		case UPLOAD_ERR_NO_FILE:
			function_alert("No file is selected. Please select the file. (error: $error)");
			break;
		case UPLOAD_ERR_NO_TMP_DIR:
			function_alert("Error: No such directory for temporary file. (error: $error)");
			break;
		case UPLOAD_ERR_CANT_WRITE:
			function_alert("Error: Can't create temporary file. (error: $error)");
			break;
		case UPLOAD_ERR_EXTENSION:
			function_alert("Error: File cannot be uploaded. (error: $error)");
			break;
		default:
			function_alert("File cannot be uploaded.");
			break;
	}
	mysqli_close($conn);
	function_previous_page();
	exit;
	
}

//확장자 확인
if (!in_array ($ext, $allowed_ext)){
	function_alert("Sorry. Such file extension is not allowed to be uploaded.");
	mysqli_close($conn);
	function_previous_page();
	exit;
}

//파일 저장 설정
$upload_file=$uploads_dir.'/'.$name;
//$fileinfo = pathinfo($upload_file);


if(file_exists($upload_file)){//서버에 똑같은 파일이 있을 경우 메시지 출력
	function_alert("There already is a file with the same name. Please confirm the file or change the file name.");
	mysqli_close($conn);
	function_previous_page();
}	
else{//서버에 파일 올리고 
	move_uploaded_file($_FILES[$field_name]['tmp_name'],$upload_file);
	
	$sql = "alter table file auto_increment =1";
	mysqli_query($conn,$sql);

	$sql = "INSERT INTO file (`name`, `ext`, `full_name`,`date`) VALUES ('$file_title', '$ext', '$name', '$date')";
	mysqli_query($conn,$sql);
	mysqli_close($conn);
	function_page_relocation('../course_material.php');
}




?>
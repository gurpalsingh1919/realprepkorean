<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function contactUs()
    {
        return view('home.contact');
    }
    public function aboutus()
    {
        return view('home.about');
    }
    public function news()
    {
        return view('home.news');
    }
    public function summerCampSat()
    {
        return view('home.academic-program.summer-camp-sat');
    }
    public function academicProgram()
    {
        return view('home.academic-program.index');
    }
    public function summerCampRealCore()
    {
        return view('home.academic-program.summer-camp-real-core');
    }
    public function writerCampRealcore()
    {
        return view('home.academic-program.writer-camp-real-core');
    }
    public function writerCampSat()
    {
        return view('home.academic-program.writer-camp-sat');
    }
    public function apSatSubjectTests()
    {
        return view('home.academic-program.ap-sat-subject-tests');
    }
    public function consultingProgram()
    {
        return view('home.consulting-program.index');
    }
    public function applicationConsultingRealService()
    {
        return view('home.consulting-program.application-consulting-real-service');
    }
    public function applicationConsultingPrepService()
    {
        return view('home.consulting-program.application-consulting-prep-service');
    }
    public function generalConsulting()
    {
        return view('home.consulting-program.general-consulting');
    }
    public function financialAidConsulting()
    {
        return view('home.consulting-program.financial-aid-consulting');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\rawTest;
use App\student;
use App\file;
use App\Board;
use App\answerTest;
use App\scoreTest;

use App\AnswerQuiz;
use App\ScoreQuiz;

use App\conversionTable2;
use App\conversionTable1;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;
use Carbon\Carbon;
use PDF;
use App\Http\Helpers\helpers;
use Illuminate\Support\Facades\Hash;
class DashboardController extends Controller
{
   public function index()
   {
   	return view('admin.dashboard');
   }
   public function allLectures()
   {

   	return view('admin.lecture.list');
   }
   public function assignMocktests()
   {
		$Users=User::where(['role_id'=>2,'status'=>1])->get();
   		$recentUsers=scoreTest::with('userDetail')->OrderBy('test_date','DESC')->take(5)->get();
   		//echo "<pre>";print_r($recentUsers->toArray());die;
   		return view('admin.mocktest.assign',compact('Users','recentUsers'));
   }

   public function mocktestList()
   {
      $users=scoreTest::with('userDetail')->OrderBy('test_date','DESC')->paginate();
      return view('admin.mocktest.list',compact('users'));
   }
   public function MocktestReviews(Request $request)
      {
         $date=$request->date;
         $test=$request->test;
         $user_id=$request->user;
         $scoreTest=scoreTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
         $total = $scoreTest->total;
         $rw   = $scoreTest->section_rw;
         $m = $scoreTest->section_m;
         $raw_r   = $scoreTest->raw_r;
         $raw_w   = $scoreTest->raw_w;
         $raw_m   = $scoreTest->raw_m;

         $test_number=rawTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
         $result= $this->function_test_to_letter($test_number);

         $temp_answer=answerTest::where('test',$test)->first();
         $result_answer    = $temp_answer;
         $answer  = $this->function_test_to_letter($temp_answer);
         $userDetail=User::find($user_id);
         return view('admin.mocktest.review',compact('total','rw','m','test','raw_r','raw_w','raw_m','result','temp_answer','answer','date','result_answer','userDetail'));
      }


      public function function_test_to_letter($num)
      {
         $letter = array();

         for ($i = 101 ; $i < 153 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 201 ; $i < 245 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 301 ; $i < 316 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 316 ; $i < 321 ; $i++){
            if($num->$i=='NULL'){
               $letter[$i] = '-'; 
            }
            else{ $letter[$i] = $num->$i;}
         }
         for ($i = 401 ; $i < 431 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 431 ; $i < 439 ; $i++){
            if($num->$i=='NULL'){
               $letter[$i] = '-'; 
            }
            else{ $letter[$i] = $num->$i;}
         }
         return $letter;
      }

  public function assignMocktestPost(Request $request)
  {
		$this->validate($request,[
                'test_id' => 'required',
                'students' => 'required'
               
                ]);
	 	if(isset($request->mocktest) && $request->mocktest=='Assign')
	 	{
	 		if(isset($request->students) && count($request->students)>0)
	 		{
	 			foreach ($request->students as $key => $value) 
	 			{
	 				$id=$value;
	 				$test_id=$request->test_id;
	 				$student=student::where('user_id',$id)->first();
	 				if(isset($student) && !empty($student))
	 				{
	 					$student->test=$test_id;
		 				$student->section_1='o';
		 				$student->section_2='o';
		 				$student->section_3='o';
		 				$student->section_4='o';
		 				$student->save();
		 			}
	 			}
	 			$message="Mock Test Assinged Successfully to Selected Students";
	            return redirect()->back()->with(['success' => $message]);
	 		}
	 	}
	 	else if(isset($request->mocktest) && $request->mocktest=='Cancel')
	 	{
	 		if(isset($request->students) && count($request->students)>0)
	 		{
	 			foreach ($request->students as $key => $value) 
	 			{
	 				$id=$value;
	 				$test_id=$request->test_id;
	 				$student=student::where('user_id',$id)->first();
	 				if(!empty($student))
	 				{
	 					$student->test=0;
		 				$student->section_1='x';
		 				$student->section_2='x';
		 				$student->section_3='x';
		 				$student->section_4='x';
		 				$student->save();
		 			}
	 			}
	 			$message="Mock Test Successfully Cancel to Selected Students";
	         return redirect()->back()->with(['success' => $message]);
	 		}
	 	}
	 	else
	   {
	   	$message="Something Went Wrong. Please Try Again After Some Time.";
	      return redirect()->back()->with(['success' => $message]);
	   }
   }
   public function dailyQuiz()
   {
		//$Users=User::where(['role_id'=>2,'status'=>1])->get();
		$Users=student::with(["user" => function($q){
		        $q->where(['role_id'=>2,'status'=>1]);
		    }])->get();
		//echo "<pre>";print_r($Users->toArray());die;
   		$user_recent_quiz=ScoreQuiz::with('userDetail')->OrderBy('quiz_date','DESC')->take(5)->get();
   		return view('admin.quiz.assign',compact('Users','user_recent_quiz'));
   }
   public function quizList()
   {
      $users_quizzes=ScoreQuiz::with('userDetail')->OrderBy('quiz_date','DESC')->paginate();
      return view('admin.quiz.list',compact('users_quizzes'));
   }
    public function quizReview(Request $request)
    {
      $date=$request->date;
      $quiz=$request->quiz;
      $user_id=$request->user;
      $ScoreQuiz=ScoreQuiz::where(['quiz'=>$quiz,'quiz_date'=>$date,'user_id'=>$user_id])->first();
      $userDetail=User::find($user_id);
      $result_answer=AnswerQuiz::where('quiz',$quiz)->first();
    
     return view('admin.quiz.quiz-review',compact('ScoreQuiz','result_answer','quiz','date','userDetail'));
    }
    public function assignDailyQuiz(Request $request)
   {
		
	 	if(isset($request->quiz) && $request->quiz=='Assign')
	 	{
	 		$this->validate($request,[
                'quiz_1' => 'required|numeric|min:1|max:54',
                'quiz_2' => 'required|numeric|min:1|max:54',
                'quiz_3' => 'required|numeric|min:1|max:54',
               	'students' => 'required'
                ]);

	 		if(isset($request->students) && count($request->students)>0)
	 		{
	 			foreach ($request->students as $key => $value) 
	 			{
	 				$id=$value;
	 				$quiz1=$request->quiz_1;
	 				$quiz2=$request->quiz_2;
	 				$quiz3=$request->quiz_3;
	 				$student=student::where('id',$id)->first();
	 				$student->quiz1=$quiz1;
	 				$student->quiz2=$quiz2;
	 				$student->quiz3=$quiz3;
	 				if($student->save())
	 				{
	 					$message="Quiz Assinged Successfully to Selected Students";
               			return redirect()->back()->with(['success' => $message]);
	 				}

	 			}
	 		}
	 	}
	 	else if(isset($request->quiz) && $request->quiz=='Cancel')
	 	{
	 		$this->validate($request,[
                'students' => 'required|array|min:1'
                ]);
	 		if(isset($request->students) && count($request->students)>0)
	 		{
	 			foreach ($request->students as $key => $value) 
	 			{
	 				$id=$value;
	 				$student=student::where('id',$id)->first();
	 				if(!empty($student))
	 				{
	 					$student->quiz1='0';
		 				$student->quiz2='0';
		 				$student->quiz3='0';
		 				$student->save();
		 			}
	 			}
	 			$message="Quiz Successfully Cancel to Selected Students";
	         return redirect()->back()->with(['success' => $message]);
	 		}
	 	}
	 	else
	   {
	   	$message="Something Went Wrong. Please Try Again After Some Time.";
	      return redirect()->back()->with(['success' => $message]);
	   }
   }

   public function courseMaterialList()
   {
		$courses=file::where('status','1')->OrderBy('date','DESC')->paginate(10);
   		return view('admin.courseMaterial.course-list',compact('courses'));
   }
   public function addCourseMaterial()
   {
   		return view('admin.courseMaterial.add-course');
   }
   public function addCourseMaterialPost(Request $request)
   {
   		$this->validate($request,[
                'course_file' => 'required|file|max:500|mimes:jpeg,pdf,png,doc,doc,pptx,ppt,xls,xlsx,txt,PNG,PDF,jpg,gif,mp4',
                ]);
   		if($request->hasFile('course_file'))
   		{
   			$file=$request->course_file->getClientOriginalName();
   			$new_file= new file();
   			$new_file->ext=$request->course_file->extension();
   			$new_file->name=pathinfo($file, PATHINFO_FILENAME);
   			$new_file->full_name=$file;
   			$new_file->date=date('Y-m-d');
   			$new_file->save();

   		 	$fileName = pathinfo($file, PATHINFO_FILENAME).'-'.time().'.'.$request->course_file->extension(); 
   		  	$request->course_file->move('public/user/file/course_material', $fileName); 
   		}
   
       

   		$message="<b>Course Material has been added successfully</b>";
       	return redirect()->back()->with(['success' => $message]);
   }
   public function deleteCourse($id)
   {
	   	if(isset($id))
	   	{
	   		$file = file::find($id);
	   		if($file)
	   		{
	   			$file->status = '0';
        		$file->save();
        		$message="<b>Course Material has been deleted successfully</b>";
       			return redirect()->back()->with(['success' => $message]);
	   		}
        	
	   	}
   		$message="<b>Something went wrong, Please try after sometime !!!</b>";
       	return redirect()->back()->with(['error' => $message]);
    
   }

   /************** Announcements Actions **********************/
   public function announcementsListing()
   {
		$announcements=Board::where('status','1')->OrderBy('updated_at','DESC')->paginate(10);
   		return view('admin.announcements.listing',compact('announcements'));
   }
   public function addAnnouncement()
   {
   		return view('admin.announcements.add');
   }
   public function addAnnouncementPost(Request $request)
   {
   		$this->validate($request,[
                	'title' => 'required',
                 	'content' => 'required',
                ]);
   		$message="<b>Something went wrong, Please try after sometime !!!</b>";
   		if(isset($request->title) && isset($request->content))
   		{
   			$new_Board= new Board();
   			$new_Board->name='RealPREP';
   			$new_Board->title=$request->title;
   			$new_Board->content=$request->content;
   			$new_Board->save();
   			$message="<b>Announcement added successfully</b>";
		}
   		return redirect()->back()->with(['success' => $message]);
   }
   public function deleteAnnouncement($id)
   {
	   	if(isset($id))
	   	{
	   		$file = Board::find($id);
	   		if($file)
	   		{
	   			$file->status = '0';
        		$file->save();
        		$message="<b>Announcement has been deleted successfully</b>";
       			return redirect()->back()->with(['success' => $message]);
	   		}
        	
	   	}
   		$message="<b>Something went wrong, Please try after sometime !!!</b>";
       	return redirect()->back()->with(['error' => $message]);
    
   }
   public function announcementsView($id)
   {
   		$Selectedboard='';
   		if(isset($id))
   		{
   			$Selectedboard=Board::find($id);
   		}
   		$announcements=Board::where('status','1')->OrderBy('updated_at','DESC')->paginate(10);
   		return view('admin.announcements.show',compact('announcements','Selectedboard'));
   }
   public function announcementsEdit(Request $request)
   {
   		$Selectedboard='';
   		if(isset($request->id))
   		{
   			$Selectedboard=Board::find($request->id);
   		}
   		return view('admin.announcements.edit',compact('Selectedboard'));
   }
   public function addAnnouncementEditPost(Request $request,$id)
   {
   		$this->validate($request,[
                	'title' => 'required',
                 	'content' => 'required',
                ]);
   		if(isset($id))
   		{
   			$Selectedboard=Board::find($id);
   		}
   		$message="<b>Something went wrong, Please try after sometime !!!</b>";
   		if(isset($request->title) && isset($request->content) && !empty($Selectedboard))
   		{
   			$Selectedboard->title=$request->title;
   			$Selectedboard->content=$request->content;
   			$Selectedboard->save();
   			$message="<b>Announcement Updated successfully</b>";
		}
   		return redirect()->back()->with(['success' => $message]);
   }
   public function assignLectures()
   {
      $Users=User::where(['role_id'=>2,'status'=>1])->get();
     
      return view('admin.lecture.assign',compact('Users'));
   }
   public function assignLecturesPost(Request $request)
   {
    
    if(isset($request->lecture) && $request->lecture=='Assign')
    {
      $this->validate($request,[
                'lecture_id' => 'required',
                'students' => 'required'
               
                ]);

      if(isset($request->students) && count($request->students)>0)
      {
        foreach ($request->students as $key => $value) 
        {
          $id=$value;
          $lecture_id=$request->lecture_id;
          $student=student::where('user_id',$id)->first();
          if(isset($student) && !empty($student))
          {
            $student->lecture=$lecture_id;
            $student->save();
          }
        }
        $message="Lecture Assinged Successfully to Selected Students";
              return redirect()->back()->with(['success' => $message]);
      }
    }
    else if(isset($request->lecture) && $request->lecture=='Cancel')
    {
      $this->validate($request,[
                //'lecture_id' => 'required',
                'students' => 'required'
               
                ]);

      if(isset($request->students) && count($request->students)>0)
      {
        foreach ($request->students as $key => $value) 
        {
          $id=$value;
          $lecture_id=$request->lecture_id;
          $student=student::where('user_id',$id)->first();
          if(!empty($student))
          {
            $student->lecture=0;
            $student->save();
          }
        }
        $message="Lecture Successfully Cancel to Selected Students";
           return redirect()->back()->with(['success' => $message]);
      }
    }
    else if(isset($request->review) && $request->review=='Assign')
    {
      $this->validate($request,[
                'review_id' => 'required',
                'students' => 'required'
               
                ]);
      if(isset($request->students) && count($request->students)>0)
      {
        foreach ($request->students as $key => $value) 
        {
          $id=$value;
          $review_id=$request->review_id;
          $student=student::where('user_id',$id)->first();
          if(isset($student) && !empty($student))
          {
            $student->review=$review_id;
            $student->save();
          }
        }
        $message="Lecture Assinged Successfully to Selected Students";
              return redirect()->back()->with(['success' => $message]);
      }
    }
    else if(isset($request->review) && $request->review=='Cancel')
    {
      $this->validate($request,[
                //'review_id' => 'required',
                'students' => 'required'
               
                ]);

      if(isset($request->students) && count($request->students)>0)
      {
        foreach ($request->students as $key => $value) 
        {
          $id=$value;
          $review_id=$request->review_id;
          $student=student::where('user_id',$id)->first();
          if(!empty($student))
          {
            $student->review=0;
            $student->save();
          }
        }
        $message="Lecture Successfully Cancel to Selected Students";
           return redirect()->back()->with(['success' => $message]);
      }
    }
    else
     {
      $message="Something Went Wrong. Please Try Again After Some Time.";
        return redirect()->back()->with(['success' => $message]);
     }
   }



  public function usersList()
  {
    $users=User::orderBy('id', 'DESC')->paginate();
   // echo "<pre>";print_r($users->toArray());die;
    return view('admin.users.list',compact('users'));
  }
  public function addNewUser()
  {
    return view('admin.users.add');
  }
  public function addNewUserPost(Request $request)
  {
    $this->validate($request,[
                'first_name' => 'required',
                //'last_name' => 'required',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8',
                ]);
        $data=array();
        $data['first_name']=$request->first_name;
        $data['last_name']=$request->last_name;
        $data['email']=$request->email;
        $data['password'] = Hash::make($request->password);
        $data['status'] ='1';
        $data['role_id'] =$request->role_id;
        if(User::create($data))
        {
            return redirect()->back()
                        ->with('success','Congratulation !!! User registered successfully.');
        }

  }


}

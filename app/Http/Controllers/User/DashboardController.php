<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\rawTest;
use App\student;
use App\file;
use App\Board;
use App\answerTest;
use App\scoreTest;

use App\AnswerQuiz;
use App\ScoreQuiz;

use App\conversionTable2;
use App\conversionTable1;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;
use Carbon\Carbon;
use PDF;
use App\Http\Helpers\helpers;
class DashboardController extends Controller
{
      public function index()
   	{
         $user_id=Auth::user()->id;
         $student=student::where('user_id',$user_id)->first();
   		return view('user.index',compact('student'));
   	}
   	public function myLectures()
   	{
   		if(Auth::check())
      {
   			$user_id=Auth::user()->id;
   			$student=student::where('user_id',$user_id)->first();
   			//echo "<pre>";print_r($student->toArray());die;
   			return view('user.lectures',compact('student'));
   		}
   	}
   	public function ViewLecture(Request $request)
   	{
   		if(Auth::check())
         {
   			$user_id=Auth::user()->id;
   			$student=student::where('user_id',$user_id)->first();
   			//echo "<pre>";print_r($student->toArray());die;
   			$type = $request->type;
				$file =  $request->file;
   			return view('user.view-lectures',compact('student','type','file'));
   		}
   	}
   	public function mockTest(Request $request)
   	{
   		if(Auth::check())
         {
   			$user_id=Auth::user()->id;
   			$student=student::where('user_id',$user_id)->first();
   			//echo "<pre>";print_r($student->toArray());die;
   			$type = $request->type;
				$file =  $request->file;
            $scoreTests=scoreTest::where(['user_id'=>$user_id])->get();
   			return view('user.mock-tests',compact('student','type','file','scoreTests'));
   		}
   	}
     

      public function getStartTest(Request $request)
      {
         if(Auth::check())
         {
            $user_id=Auth::user()->id;
            $student=student::where('user_id',$user_id)->first();

            $type       = $request->type;
            $test_no = $request->test;
            $test_section1 = $student->section_1;
            $test_section2 = $student->section_2;
            $test_section3 = $student->section_3;
            $test_section4 = $student->section_4;
            //echo "skfjhjskdhfhs".$test_section1;die;
            if ($test_section1 == 'o')
            {
               //function_page_relocation('../test/start.php?test='.$test_no);
               //return redirect()->route('user.load_mock_test',['test'=>$test_no]);
               return view('user.start-mock-tests',compact('student','test_no'));
            }
            elseif ($test_section2 == 'o')
            {
               return redirect()->route("user.load_mock_test",['test'=>$test_no,'section'=>2]);
            }
            elseif ($test_section3 == 'o')
            {
               return redirect()->route("user.load_mock_test",['test'=>$test_no,'section'=>3]);
            }
            else
            {
               return redirect()->route("user.load_mock_test",['test'=>$test_no,'section'=>4]);
            }

           
            //echo "<pre>";print_r($student->toArray());die;
            
            
         }
      }
      public function getLoadTest(Request $request)
      {
         $user_id=Auth::user()->id;
         $student=student::where('user_id',$user_id)->first();

         $test_no = $request->test;
         $section = $request->section;
         $test_section='section_'.$section;
         //echo "<pre>";print_r($test_section);die;
         if(isset($section) && isset($test_no) && $test_no !='' && $student->$test_section !='x')
         {
            $date = date("Y-m-d");
            $rawTest=rawTest::where(['test'=>$test_no,'test_date'=>$date,'user_id'=>$user_id])->get();
            if(!isset($rawTest) || count($rawTest)<=0 )
            {
               $rawTests=new rawTest;
               $rawTests->test=$test_no;
               $rawTests->test_date=$date;
               $rawTests->user_id=$user_id;
               $rawTests->save();
            }
         }
         else
         {
            $message="<b>You do not have access to this test. !!!</b>";
            return redirect()->route('user.mock_test')->with('error',$message);
         }
         if(isset($section) && $section =='1')
         {
            
            
            if($student->section_1=='o')
            {
               $student->section_1='x';
               $student->save();
            }
            $section='1';

            return view('user.omr-section-1',compact('student','test_no','date','section'));
         }
         else if(isset($section) && $section =='2')
         {
            if($student->section_2=='o')
            {
               $student->section_2='x';
               $student->save();
            }
            $section='2';
            return view('user.omr-section-2',compact('student','test_no','date','section'));
         }
         else if(isset($section) && $section =='3')
         {
            if($student->section_3=='o')
            {
               $student->section_3='x';
               $student->save();
            }
            $section='3';
            return view('user.omr-section-3',compact('student','test_no','date','section'));
         }
         else if(isset($section) && $section =='4')
         {
            if($student->section_4=='o')
            {
               $student->section_4='x';
               $student->test=0;
               $student->save();
            }
            $section='4';
            return view('user.omr-section-4',compact('student','test_no','date','section'));
         }
         else
         {
            $message="<b>You do not have access to this mock test.</b>";
            return redirect()->route('user.mock_test')->with('error',$message);
         }
         //echo "<pre>";print_r($request->all());die;
         
      }
      public function answerCheck(Request $request)
      {
         $test = $request->test;
         $section = $request->section;
         $date = date("Y-m-d");
         $user_id=Auth::user()->id;
         if($section == '')
         {
            $student=student::where('user_id',$user_id)->first();
            $rawTest=rawTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->get();
            if(empty($rawTest) )
            {
               $rawTests=new rawTest;
               $rawTests->test=$test;
               $rawTests->test_date=$date;
               $rawTests->user_id=$user_id;
               $rawTests->save();

            }
            return redirect()->route("user.load_mock_test",['test'=>$test,'section'=>1]);
         }
         elseif($section == '1')
         {
            $a1 = array();
            $data =  array();

            for ($i = 101; $i < 153; $i++)
            {
               if(!isset($_POST[$i]) || $_POST[$i] == '')
               {
                  $_POST[$i] = 0;
               }
               $a1[$i-100] = $_POST[$i];
               //$data[$i]=$_POST[$i];
               
            }
            
            //echo "<pre>";print_r($request->all());
            //$array = array_dot($data);
          //print_r($data );die;
           // $rawTest=rawTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->update($data);
            $sql =   "UPDATE raw_tests SET `101` = '$a1[1]', `102` = '$a1[2]', `103` = '$a1[3]', `104` = '$a1[4]', `105` = '$a1[5]', 
                         `106` = '$a1[6]', `107` = '$a1[7]', `108` = '$a1[8]', `109` = '$a1[9]', `110` = '$a1[10]',
                         `111` = '$a1[11]', `112` = '$a1[12]', `113` = '$a1[13]', `114` = '$a1[14]', `115` = '$a1[15]',
                         `116` = '$a1[16]', `117` = '$a1[17]', `118` = '$a1[18]', `119` = '$a1[19]', `120` = '$a1[20]',
                         `121` = '$a1[21]', `122` = '$a1[22]', `123` = '$a1[23]', `124` = '$a1[24]', `125` = '$a1[25]', 
                         `126` = '$a1[26]', `127` = '$a1[27]', `128` = '$a1[28]', `129` = '$a1[29]', `130` = '$a1[30]',
                         `131` = '$a1[31]', `132` = '$a1[32]', `133` = '$a1[33]', `134` = '$a1[34]', `135` = '$a1[35]',
                         `136` = '$a1[36]', `137` = '$a1[37]', `138` = '$a1[38]', `139` = '$a1[39]', `140` = '$a1[40]',
                         `141` = '$a1[41]', `142` = '$a1[42]', `143` = '$a1[43]', `144` = '$a1[44]', `145` = '$a1[45]',
                         `146` = '$a1[46]', `147` = '$a1[47]', `148` = '$a1[48]', `149` = '$a1[49]', `150` = '$a1[50]',`151` = '$a1[41]', `152` = '$a1[52]' WHERE `user_id` = '$user_id' AND `test` = '$test' AND `test_date` = '$date'";
               DB::update($sql);
               //echo $sql;die;
               //function_page_relocation('../test/break.php?test='.$test.'&break=1&date='.$date);
               return redirect()->route("user.test_break",['test'=>$test,'section'=>1,'break'=>1,'date'=>$date]);
         }
         elseif($section == '2')
         {
            $a2 = array();
            for ($i = 201; $i < 245; $i++)
            {
               if(!isset($_POST[$i]) || $_POST[$i] == '')
               {
                  $_POST[$i] = 0;
               }
               $a2[$i-200] = $_POST[$i];
               
            }
           $sql =    "UPDATE raw_tests SET `201` = '$a2[1]', `202` = '$a2[2]', `203` = '$a2[3]', `204` = '$a2[4]', `205` = '$a2[5]', 
                         `206` = '$a2[6]', `207` = '$a2[7]', `208` = '$a2[8]', `209` = '$a2[9]', `210` = '$a2[10]',
                         `211` = '$a2[11]', `212` = '$a2[12]', `213` = '$a2[13]', `214` = '$a2[14]', `215` = '$a2[15]',
                         `216` = '$a2[16]', `217` = '$a2[17]', `218` = '$a2[18]', `219` = '$a2[19]', `220` = '$a2[20]',
                         `221` = '$a2[21]', `222` = '$a2[22]', `223` = '$a2[23]', `224` = '$a2[24]', `225` = '$a2[25]', 
                         `226` = '$a2[26]', `227` = '$a2[27]', `228` = '$a2[28]', `229` = '$a2[29]', `230` = '$a2[30]',
                         `231` = '$a2[31]', `232` = '$a2[32]', `233` = '$a2[33]', `234` = '$a2[34]', `235` = '$a2[35]',
                         `236` = '$a2[36]', `237` = '$a2[37]', `238` = '$a2[38]', `239` = '$a2[39]', `240` = '$a2[40]',
                         `241` = '$a2[41]', `242` = '$a2[42]', `243` = '$a2[43]', `244` = '$a2[44]' WHERE `user_id` = '$user_id' AND `test` = '$test' AND `test_date` = '$date'";
               DB::update($sql);
               return redirect()->route("user.load_mock_test",['test'=>$test,'section'=>3]);
         }
         elseif($section == '3')
         {
            $a3 = array();
            $data=array();
            for ($i = 301; $i < 316; $i++)
            {
               if(!isset($_POST[$i]) || $_POST[$i] == '')
               {
                  $_POST[$i] = 0;
               }
               $a3[$i-300] = $_POST[$i]; 
                
            }
            
            //echo "<pre>";print_r($request->all());die;
            //section3 주관식 공백처리
            $s3 = array();
            $k = 0;
            for ($i = 3161; $i < 3211; $i +=6)
            {
               $j = $i+4;
               $answer = '';

               while($i<$j)
               {
                  if(!isset($_POST[$i]) || $_POST[$i] == '')
                  {
                     $_POST[$i] = 0;
                  }

                  $answer.=$_POST[$i];
                  $i++;
               }

               if ($answer == '' || strpos($answer,'/0')){$answer = 'NULL';}

               $s3[$k] = $answer;
               $k++; 
               
            }
            $sql =   "UPDATE raw_tests SET `301` = '$a3[1]', `302` = '$a3[2]', `303` = '$a3[3]', `304` = '$a3[4]', `305` = '$a3[5]',
                         `306` = '$a3[6]', `307` = '$a3[7]', `308` = '$a3[8]', `309` = '$a3[9]', `310` = '$a3[10]', 
                         `311` = '$a3[11]', `312` = '$a3[12]', `313` = '$a3[13]', `314` = '$a3[14]', `315` = '$a3[15]',
                         `316` = '$s3[0]', `317` =  '$s3[1]', `318` = '$s3[2]', `319` = '$s3[3]', `320` = '$s3[4]'  WHERE `user_id` = '$user_id' AND `test` = '$test' AND `test_date` = '$date'";
               DB::update($sql);
             return redirect()->route("user.test_break",['test'=>$test,'section'=>1,'break'=>2,'date'=>$date]);
         }
         elseif($section == '4')
         {
            $a4 = array();
               for ($i = 401; $i < 431; $i++){
                  if(!isset($_POST[$i]) || $_POST[$i] == '')
                  {
                     $_POST[$i] = 0;
                  }
                  $a4[$i-400] = $_POST[$i]; 
                   $data[$i]=$_POST[$i];       
               }

               //section4 주관식 공백처리
               $s4 = array();
               $k = 0;
               for ($i = 4311; $i < 4391; $i +=6){
                  $j = $i+4;
                  $answer = '';

                  while($i<$j)
                  {
                     if(!isset($_POST[$i]) || $_POST[$i] == '')
                     {
                        $_POST[$i] = 0;
                     }
                     $answer.=$_POST[$i];
                     $i++;
                  }

                  if ($answer == '' || strpos($answer,'/0')){$answer = 'NULL';}
                  
                  $s4[$k] = $answer;
                  $k++;
               }
               $sql =   "UPDATE raw_tests SET `401` = '$a4[1]', `402` = '$a4[2]', `403` = '$a4[3]', `404` = '$a4[4]', `405` = '$a4[5]',
                         `406` = '$a4[6]', `407` = '$a4[7]', `408` = '$a4[8]', `409` = '$a4[9]', `410` = '$a4[10]', 
                         `411` = '$a4[11]', `412` = '$a4[12]', `413` = '$a4[13]', `414` = '$a4[14]', `415` = '$a4[15]',
                         `416` = '$a4[16]', `417` = '$a4[17]', `418` = '$a4[18]', `419` = '$a4[19]', `420` = '$a4[20]', 
                         `421` = '$a4[21]', `422` = '$a4[22]', `423` = '$a4[23]', `424` = '$a4[24]', `425` = '$a4[25]',
                         `426` = '$a4[26]', `427` = '$a4[27]', `428` = '$a4[28]', `429` = '$a4[29]', `430` = '$a4[30]', 
                         `431` = '$s4[0]', `432` = '$s4[1]', `433` = '$s4[2]', `434` = '$s4[3]', `435` = '$s4[4]', `436` = '$s4[5]', `437` = '$s4[6]', `438` = '$s4[7]'         WHERE `user_id` = '$user_id' AND `test` = '$test' AND `test_date` = '$date'";
                  DB::update($sql);
               /****** find results **********/
               $test_number=rawTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
               $answer_number=answerTest::where('test',$test)->first();
               if(isset($test_number) && isset($answer_number)>0)
               {
                 
                  $final_number=$this->function_test_score_calculation($test_number, $answer_number);
                  //echo "<pre>";print_r($final_number);die;
                  $raw_r = $final_number['raw_r'];
                  $raw_w = $final_number['raw_w'];
                  $raw_m = $final_number['raw_m'];
                   $scoreTest=new scoreTest;
                  $scoreTest->test=$test;
                  $scoreTest->user_id=$user_id;
                  $scoreTest->test_date=$date;
                  $scoreTest->raw_r=$raw_r;
                  $scoreTest->raw_w=$raw_w;
                  $scoreTest->raw_m=$raw_m;
                  $scoreTest->save();

                  $conversion_1r=conversionTable1::where('raw_score',$raw_r)->first();
                  $test_r=$conversion_1r->r;
                  $conversion_1w=conversionTable1::where('raw_score',$raw_w)->first();
                  $test_w=$conversion_1w->w;
                  $conversion_1m=conversionTable1::where('raw_score',$raw_m)->first();
                  $test_m=$conversion_1m->m;

                  $rw = $test_r + $test_w;
                  $conversion_2r=conversionTable2::where('raw_score',$rw)->first();
                  $section_rw=$conversion_2r->rw;
                  $conversion_2r=conversionTable2::where('raw_score',$raw_m)->first();
                  $section_m=$conversion_2r->m;

                  $total  = $section_rw + $section_m;

                 
                  $scoreTest->test_r=$test_r;
                  $scoreTest->test_w=$test_w;
                  $scoreTest->test_m=$test_m;
                  $scoreTest->section_rw=$section_rw;
                  $scoreTest->section_m=$section_m;
                  $scoreTest->total=$total;

                  $scoreTest->save();
                   return redirect()->route("user.test_result",['test'=>$test,'date'=>$date]);

               }
               
               
         }
      }
      public function function_test_score_calculation($test_number, $answer_number)
      {
         $raw_r = 0;
         $raw_w = 0;
         $raw_m = 0;
         $final_number = array();
          
         //section1 점수계산
         for ($i=101; $i<153; $i++)
         {
            if($test_number->$i == $answer_number->$i)
            {
               ++$raw_r;
            }  
         }
         
         //section2 점수계산
         for ($i=201; $i<245; $i++){
            if($test_number[$i] == $answer_number[$i]){
               ++$raw_w;
            }  
         }
         //section3 객관식 점수계산
         for ($i=301; $i<316; $i++){
            if($test_number->$i == $answer_number->$i){
               ++$raw_m;
            }  
         }
         
         //section4 객관식 점수계산
         for ($i=401; $i<431; $i++){
            if($test_number->$i == $answer_number->$i){
               ++$raw_m;
            }  
         }
         
         //section3 주관식 점수계산
         for ($i=316; $i<321; $i++){
            if(strpos($answer_number->$i, ',,') === false){ // ,, 없을 때: 즉 부등호 없는 답일 경우
               if(strpos($answer_number->$i,$test_number->$i) !== false){
                  ++$raw_m;
               }
            }
            else{//부등호 홀수인 경우는 안됨. 무조건 쌍부등호로 생각하고 짬.
               $exstr = explode(',,',$answer_number->$i);
               
               
               for ($k = 0 ; $k < count($exstr); $k++){//정답 분수 나누고 float 처리
                  if (strpos($exstr[$k], '/') !== false){ // 답이 분수일 경우
                     $exline = explode('/',$exstr[$k]);
                     $exstr[$k] = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
                  }
                  elseif (strpos($exstr[$k],'.') !== false){ //답이 소수일 경우 str으로 받은 값 float 처리
                     $exstr[$k] = (float)$exstr[$k];
                  }
               
               }

               if (strpos($test_number->$i,'/') !== false){//학생답 분수 나누고 float 처리
                     $exline = explode('/',$test_number->$i);
                     $test_number->$i = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
               }
               elseif (strpos($exstr[$k],'.') !== false){ $test_number->$i = (float)$test_number->$i;}//학생답 소수 float 처리

               
               for($j = 0 ; $j < count($exstr); $j+=2){
                  if (($exstr[$j] < $test_number->$i) && ($test_number->$i < $exstr[$j+1])){
                     ++$raw_m;
                     break;
                  }
               }
            }  
         }
         
         //section4 주관식 점수계산
         for ($i=431; $i<439; $i++)
         {
            if(strpos($answer_number->$i, ',,') === false){ // ,, 없을 때: 즉 부등호 없는 답일 경우
               if(strpos($answer_number->$i,$test_number->$i) !== false){
                  ++$raw_m;
               }
            }
            else
            {//부등호 홀수인 경우는 안됨. 무조건 쌍부등호로 생각하고 짬.
               $exstr = explode(',,',$answer_number->$i);
               
               //echo "<pre>".$i;print_r($exstr);echo $test_number->$i;
               for ($k = 0 ; $k < count($exstr); $k++)
               {//정답 분수 나누고 float 처리
                  if (strpos($exstr[$k], '/') !== false){ // 답이 분수일 경우
                     $exline = explode('/',$exstr[$k]);
                     $exstr[$k] = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
                  }
                  elseif (strpos($exstr[$k],'.') !== false){ //답이 소수일 경우 str으로 받은 값 float 처리
                     $exstr[$k] = (float)$exstr[$k];
                  }
               
               }

               if (strpos($test_number->$i,'/') !== false){//학생답 분수 나누고 float 처리
                     $exline = explode('/',$test_number->$i);
                     $test_number->$i = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
               }
               elseif (strpos($exstr[$k-1],'.') !== false)
               { 
                  $test_number->$i = (float)$test_number->$i;
               }

               
               for($j = 0 ; $j < count($exstr); $j+=2){
                  if (($exstr[$j] < $test_number->$i) && ($test_number->$i < $exstr[$j+1])){
                     ++$raw_m;
                     break;
                  }
               }
            }  
         }
         
         $final_number['raw_r'] = $raw_r;
         $final_number['raw_w'] = $raw_w;
         $final_number['raw_m'] = $raw_m;
         return  $final_number;
      }

      public function Testbreak(Request $request)
      {
         $test_no=$request->test;
         $break=$request->break;
         $date=$request->date;
         //$user_id=Auth::user()->id;
         //$student=student::where('user_id',$user_id)->first();
         //echo "<pre>";print_r($request->all());die;
         return view('user.break',compact('test_no','date','break'));
      }
      public function testResult(Request $request)
      {
         $date=$request->date;
         $test=$request->test;
         $user_id=Auth::user()->id;
         $scoreTest=scoreTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
         $total = $scoreTest->total;
         $rw   = $scoreTest->section_rw;
         $m = $scoreTest->section_m;
         return view('user.result',compact('total','rw','m','test','date'));
      }
      public function reviewTestResult(Request $request)
      {
         $date=$request->date;
         $test=$request->test;
         $user_id=Auth::user()->id;
         $scoreTest=scoreTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
         $total = $scoreTest->total;
         $rw   = $scoreTest->section_rw;
         $m = $scoreTest->section_m;
         $raw_r   = $scoreTest->raw_r;
         $raw_w   = $scoreTest->raw_w;
         $raw_m   = $scoreTest->raw_m;

         $test_number=rawTest::where(['test'=>$test,'test_date'=>$date,'user_id'=>$user_id])->first();
         $result= $this->function_test_to_letter($test_number);

         $temp_answer=answerTest::where('test',$test)->first();
         $result_answer    = $temp_answer;
         $answer  = $this->function_test_to_letter($temp_answer);
         //echo "<pre>";print_r($result);print_r($answer);die;
         return view('user.mock-test-review',compact('total','rw','m','test','raw_r','raw_w','raw_m','result','temp_answer','answer','date','result_answer'));
      }
      public function function_test_to_letter($num)
      {
         $letter = array();

         for ($i = 101 ; $i < 153 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 201 ; $i < 245 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 301 ; $i < 316 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 316 ; $i < 321 ; $i++){
            if($num->$i=='NULL'){
               $letter[$i] = '-'; 
            }
            else{ $letter[$i] = $num->$i;}
         }
         for ($i = 401 ; $i < 431 ; $i++){
            switch($num->$i){
               case 0: $letter[$i] = '-'; break;
               case 1: $letter[$i] = 'A'; break;
               case 2:  $letter[$i] = 'B'; break;
               case 3: $letter[$i] = 'C'; break;
               case 4: $letter[$i] = 'D'; break;
            }
         }
         for ($i = 431 ; $i < 439 ; $i++){
            if($num->$i=='NULL'){
               $letter[$i] = '-'; 
            }
            else{ $letter[$i] = $num->$i;}
         }
         return $letter;
      }
      public function allMocktestReviews()
      {
          $user_id=Auth::user()->id;
         $scoreTests=scoreTest::where(['user_id'=>$user_id])->get();
         return view('user.all-mocktest-review',compact('scoreTests'));
      }

      /************* My Quiz section *****************/
      public function userDailyQuiz(Request $request)
      {
         if(Auth::check())
         {
            $user_id=Auth::user()->id;
            $student=student::where('user_id',$user_id)->first();
            //echo "<pre>";print_r($student->toArray());die;
            $type = $request->type;
            $quiz =  $request->quiz;
            $ScoreQuiz=ScoreQuiz::where(['user_id'=>$user_id])->get();
            return view('user.quiz',compact('student','type','quiz','ScoreQuiz'));
         }
      }
      public function startDailyQuiz(Request $request)
      {
         if(Auth::check())
         {
            //echo "<pre>";print_r($request->all());die;
            $user_id=Auth::user()->id;
            $student=student::where('user_id',$user_id)->first();
            if ($student->status == '1' && isset($request->type) && isset($request->quiz))
            {
               $type = $request->type;
               $quiz =  $request->quiz;
               return view('user.start-quiz',compact('student','type','quiz'));
            }
            else
            {
               $message="<b>Sorry, you don\'t have permission to access this quiz.</b>";
               return redirect()->back()->with(['success' => $message]);
            }
            
         }
      }
      public function loadQuizOmr(Request $request)
      {
         $user_id=Auth::user()->id;
         $student=student::where('user_id',$user_id)->first();
         $type = $request->type;
         $quiz =  $request->quiz;
         if (($student->quiz1 == $quiz) || ($student->quiz2 == $quiz) || ($student->quiz3 == $quiz))
         {
            //echo "<pre>";print_r($request->all());die;
            
            if ($student->status == '1' && $student->$type>0)
            {
               
               $student->$type=0;
               $student->save();
                       
               $date = date("Y-m-d");
               $alreadyCreated=ScoreQuiz::where(['user_id'=>$user_id,'quiz'=>$quiz,'quiz_date'=>$date])->first();
               if(empty($alreadyCreated))
               {
                  $quizScore=new ScoreQuiz;
                  $quizScore->quiz_date=$date;
                  $quizScore->quiz=$quiz;
                  $quizScore->user_id=$user_id;
                  $quizScore->save();
               }
               
               return view('user.quiz-omr',compact('student','type','quiz','date'));
            }
            else
            {
               $message="<b>Sorry, you don\'t have permission to access this quiz.</b>";
               return redirect()->back()->with(['success' => $message]);
            }
         }
         else
         {
            $message="<b>You do not have access to this quiz.</b>";
            return redirect()->route('user.quiz')->with('error',$message);
         }
        
      }
      public function submitQuizAnswers(Request $request)
      {
         $quiz = $request->quiz;
         $type = $request->type;
         $date = date("Y-m-d");
         $user_id=Auth::user()->id;
         $updateData=array();
         for ($i=1; $i<101; $i++){
            if(!isset($_POST[$i]) || $_POST[$i] == '')
            {
              $_POST[$i] = 0;
            }
            
         }
         //echo "<pre>";print_r($_POST);die;

         $sql =    "UPDATE score_quizzes SET `1`='".$_POST['1']."', `2`='".$_POST['2']."', `3`='".$_POST['3']."', `4`='".$_POST['4']."', `5`='".$_POST['5']."', 
                           `6`='".$_POST['6']."', `7`='".$_POST['7']."', `8`='".$_POST['8']."', `9`='".$_POST['9']."', `10`='".$_POST['10']."', 
                           `11`='".$_POST['11']."', `12`='".$_POST['12']."', `13`='".$_POST['13']."', `14`='".$_POST['14']."', `15`='".$_POST['15']."', 
                           `16`='".$_POST['16']."', `17`='".$_POST['17']."', `18`='".$_POST['18']."', `19`='".$_POST['19']."', `20`='".$_POST['20']."', 
                           `21`='".$_POST['21']."', `22`='".$_POST['22']."', `23`='".$_POST['23']."', `24`='".$_POST['24']."', `25`='".$_POST['25']."', 
                           `26`='".$_POST['26']."', `27`='".$_POST['27']."', `28`='".$_POST['28']."', `29`='".$_POST['29']."', `30`='".$_POST['30']."', 
                           `31`='".$_POST['31']."', `32`='".$_POST['32']."', `33`='".$_POST['33']."', `34`='".$_POST['34']."', `35`='".$_POST['35']."', 
                           `36`='".$_POST['36']."', `37`='".$_POST['37']."', `38`='".$_POST['38']."', `39`='".$_POST['39']."', `40`='".$_POST['40']."', 
                           `41`='".$_POST['41']."', `42`='".$_POST['42']."', `43`='".$_POST['43']."', `44`='".$_POST['44']."', `45`='".$_POST['45']."', 
                           `46`='".$_POST['46']."', `47`='".$_POST['47']."', `48`='".$_POST['48']."', `49`='".$_POST['49']."', `50`='".$_POST['50']."', 
                           `51`='".$_POST['51']."', `52`='".$_POST['52']."', `53`='".$_POST['53']."', `54`='".$_POST['54']."', `55`='".$_POST['55']."', 
                           `56`='".$_POST['56']."', `57`='".$_POST['57']."', `58`='".$_POST['58']."', `59`='".$_POST['59']."', `60`='".$_POST['60']."', 
                           `61`='".$_POST['61']."', `62`='".$_POST['62']."', `63`='".$_POST['63']."', `64`='".$_POST['64']."', `65`='".$_POST['65']."', 
                           `66`='".$_POST['66']."', `67`='".$_POST['67']."', `68`='".$_POST['68']."', `69`='".$_POST['69']."', `70`='".$_POST['70']."', 
                           `71`='".$_POST['71']."', `72`='".$_POST['72']."', `73`='".$_POST['73']."', `74`='".$_POST['74']."', `75`='".$_POST['75']."', 
                           `76`='".$_POST['76']."', `77`='".$_POST['77']."', `78`='".$_POST['78']."', `79`='".$_POST['79']."', `80`='".$_POST['80']."', 
                           `81`='".$_POST['81']."', `82`='".$_POST['82']."', `83`='".$_POST['83']."', `84`='".$_POST['84']."', `85`='".$_POST['85']."', 
                           `86`='".$_POST['86']."', `87`='".$_POST['87']."', `88`='".$_POST['88']."', `89`='".$_POST['89']."', `90`='".$_POST['90']."', 
                           `91`='".$_POST['91']."', `92`='".$_POST['92']."', `93`='".$_POST['93']."', `94`='".$_POST['94']."', `95`='".$_POST['95']."', 
                           `96`='".$_POST['96']."', `97`='".$_POST['97']."', `98`='".$_POST['98']."', `99`='".$_POST['99']."', `100` ='".$_POST['100']."'
               
         WHERE `user_id`='$user_id' AND `quiz`='$quiz' AND `quiz_date`='$date'";

          DB::update($sql);
         

         /****** find results **********/
         $quiz_number=ScoreQuiz::where(['user_id'=>$user_id,'quiz'=>$quiz,'quiz_date'=>$date])->first();
         $answer_number=AnswerQuiz::where('quiz',$quiz)->first();

         $final_number = $this->function_quiz_score_calculation($quiz_number, $answer_number);

         $quiz_number->score=$final_number;
         $quiz_number->save();
         return redirect()->route("user.quiz_result",['quiz'=>$quiz,'date'=>$date]);
      }
      public function function_quiz_score_calculation($quiz_number, $answer_number)
      {
         $score = 0;
         
         for ($i=1; $i<101; $i++)
         {
            if($quiz_number[$i] == $answer_number[$i])
            { 
               ++$score;
            }   
         }
         return  $score;
      }
      public function quizResult(Request $request)
      {
         $date=$request->date;
         $quiz=$request->quiz;
         $user_id=Auth::user()->id;
         $quiz_number=ScoreQuiz::where(['user_id'=>$user_id,'quiz'=>$quiz,'quiz_date'=>$date])->first();
         $score = $quiz_number->score;
         
         return view('user.quiz-result',compact('score','quiz','date'));
      }
      public function quizReview(Request $request)
      {
         $date=$request->date;
         $quiz=$request->quiz;
         $user_id=Auth::user()->id;
         $ScoreQuiz=ScoreQuiz::where(['quiz'=>$quiz,'quiz_date'=>$date,'user_id'=>$user_id])->first();
        
        $result_answer=AnswerQuiz::where('quiz',$quiz)->first();
        
         return view('user.quiz-review',compact('ScoreQuiz','result_answer','quiz','date'));
      }
      public function allCourseMaterials()
      {
         $user_id=Auth::user()->id;
         $allfiles=file::where('status','1')->OrderBy('date','DESC')->paginate(10);
         return view('user.course-materials',compact('allfiles'));
      }
      public function downloadCourseMaterial(Request $request)
      {
         
         if (Auth::check() && isset($request->file))
         {
            $filedetail=file::find($request->file);
           if(isset($filedetail->full_name) && $filedetail->full_name !='' && $request->has('download'))
            {
               $name=$filedetail->name.'.pdf';
               $pathToFile= public_path(). "/user/file/course_material/".$filedetail->full_name;
              // echo $pathToFile.'----'.$name;die;
               $headers = array(
              'Content-Type: application/pdf',
               );
               return response()->download($pathToFile, $name, $headers);
            }
        }
      }
      public function userAnnoucements()
      {
         $boards=Board::where('status','1')->get();
         return view('user.announcements',compact('boards'));
      }


}

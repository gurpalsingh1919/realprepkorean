<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreQuiz extends Model
{
    public function userDetail()
    {
       return  $this->hasOne('App\User', 'id', 'user_id');
    }
}

@extends('layouts.home-app')

@section('content')
<section class="position-relative contentContainer">
  <div class="container">
    <div class="loginBlock shadowBox">
      <div class="row">
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle"><div class="regLeftSide">
            <div class="mb-3"><img src="{{ asset('home/images/Logo-RP-Web.png') }}" alt="" /></div>
          <h4 class="line lineCenter">Already have an Account</h4>
          <p>Measure yourself against other students with a REAL SAT practice test. Diagnose your strengths and weaknesses</p>
           <a href="{{route('login')}}" class="customButton01">Sign In</a>
        </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
            @if($errors->all())
                @foreach ($errors->all() as $index=>$error)
                
                  @if($index==0)
                  <div class="col-md-12">
                    <div class="alert alert-danger">Please Fix the below errors and try again.</div></div>
                  @endif
                @endforeach
            @endif
            @if(session('error')) 
                <div class="col-md-12 error alert alert-danger alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Error : </strong>   {{ session('error') }}
                </div>
            @endif
            @if(session('success')) 
                <div class="col-md-12 error alert alert-success alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session('success') !!}
                </div>
            @endif
          <div class="middleContentOuter">
            <div class="verticalMiddle">
              <div class="loginForm contactForm">
                <h3 class=""><strong>Sign Up</strong></h3>
                <p class="text-muted mb-4">The most personalized Real Prep study platform. </p>
                
                <form class="form-signin" method="POST" action="{{ route('register') }}">
                        @csrf
                  <div class="input-row">
                    <input id="first_name" placeholder="First Name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                    @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    <input id="last_name" placeholder="Last Name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                    @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    <input id="email" placeholder="Email address" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    <input class="text-uppercase commonButton custom-btn1" type="submit" value="Create your account">
                  </div>
                  <div class="redirectLinks">
                  <p>By clicking Create Account, you agree to our <a href="#">Terms of Service.</a></p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

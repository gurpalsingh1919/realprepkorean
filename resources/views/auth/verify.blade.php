@extends('layouts.home-app')

@section('content')
<section class="position-relative contentContainer">
  <div class="container">
    <div class="loginBlock shadowBox">
      <div class="row">
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle">
                <div class="regLeftSide">
                    <div class="mb-3">
                        <img src="{{ asset('home/images/lock.png') }}" alt="" />
                    </div>
                     @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    <p class="noLine">{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.</p>
                   <a href="{{route('login')}}" class="customButton01">Sign In</a>
                </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</section>
@endsection

@extends('layouts.home-app')

@section('content')
<section class="position-relative contentContainer">
  <div class="container">
    <div class="loginBlock shadowBox">
      <div class="row">
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle"><div class="regLeftSide">
            <div class="mb-3"><img src="{{ asset('home/images/lock.png') }}" alt="" /></div>
          <h4 class="lineCenter">Don't have an Account</h4>
          <p class="noLine">Measure yourself against other students with a REAL SAT practice test. Diagnose your strengths and weaknesses</p>
           <a href="{{route('register')}}" class="customButton01">Sign Up</a>
        </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle">
              <div class="loginForm contactForm">
                <h3 class="">Reset your password</h3>
                <p class="text-muted mb-4">The most personalized Real Prep study platform. </p>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="form-signin" method="POST" action="{{ route('password.email') }}">
                    @csrf
                  <div class="input-row">
                    <input id="email" placeholder="Enter your E-mail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    <input class="text-uppercase commonButton custom-btn1" type="submit" value="{{ __('Send Password Reset Link') }}">

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

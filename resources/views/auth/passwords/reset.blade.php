@extends('layouts.home-app')

@section('content')
<section class="position-relative contentContainer">
  <div class="container">
    <div class="loginBlock shadowBox">
      <div class="row">
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle"><div class="regLeftSide">
            <div class="mb-3"><img src="{{ asset('home/images/lock.png') }}" alt="" /></div>
          <h4 class="lineCenter">Already have an Account</h4>
          <p class="noLine">Measure yourself against other students with a REAL SAT practice test. Diagnose your strengths and weaknesses</p>
           <a href="{{route('login')}}" class="customButton01">Sign In</a>
        </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="middleContentOuter">
            <div class="verticalMiddle">
              <div class="loginForm contactForm">
                <h3 class="">Reset your password</h3>
                <p class="text-muted mb-4">The most personalized Real Prep study platform. </p>
                             
                <form class="form-signin" method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                  <div class="input-row">
                    <input id="email" type="email" placeholder="Enter your email" class="@error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                       <input id="password" type="password" placeholder="Password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="input-row">
                      <input id="password-confirm" placeholder="Confirm password" type="password" name="password_confirmation" required autocomplete="new-password">
                  </div>
                  <div class="input-row">
                    <input class="text-uppercase commonButton custom-btn1" type="submit" value="{{ __('Reset Password') }}">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

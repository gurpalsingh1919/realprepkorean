@extends('layouts.home-app')

@section('content')
<section class="position-relative contentContainer">
  <div class="container">
    <div class="loginBlock shadowBox">
      <div class="row">
        <div class="col-md-6">
          <div class="bannerSlider shadow-none m-0 p-0">
            <div id="bannerSlider" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#bannerSlider" data-slide-to="0" class="active"></li>
                <li data-target="#bannerSlider" data-slide-to="1"></li>
                <li data-target="#bannerSlider" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
               <div class="carousel-item active">
                  <div class="bannerIcon">
                    <img src="{{ asset('home/images/banner-icon-01.jpeg') }}" alt="" />
                  </div>
                  <div class="bannerSliderInfo">
                    <h4 class="noLine">Test Explanation</h4>
                    <p>REAL answers to your questions from REAL teachers without leaving your desk.</p>
                  </div>
                  <div class="clearfix"></div>
                  <img src="{{ asset('home/images/slider-thumb-01.jpeg') }}" alt="" class="imgResponsive" /> 
               </div>
               <div class="carousel-item">
                  <div class="bannerIcon">
                     <img src="{{ asset('home/images/banner-icon-01.jpeg') }}" alt="" /></div>
                  <div class="bannerSliderInfo">
                    <h4 class="noLine">Test Explanation</h4>
                    <p>REAL answers to your questions from REAL teachers without leaving your desk.</p>
                  </div>
                  <div class="clearfix"></div>
                  <img src="{{ asset('home/images/slider-thumb-02.jpeg') }}" alt="" class="imgResponsive" /> 
               </div>
                <div class="carousel-item">
                  <div class="bannerIcon">
                     <img src="{{ asset('home/images/banner-icon-01.jpeg') }}" alt="" /></div>
                  <div class="bannerSliderInfo">
                    <h4 class="noLine">Test Explanation</h4>
                    <p>REAL answers to your questions from REAL teachers without leaving your desk.</p>
                  </div>
                  <div class="clearfix"></div>
                  <img src="{{ asset('home/images/slider-thumb-03.jpeg') }}" alt="" class="imgResponsive" /> </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
         @if($errors->all())
                @foreach ($errors->all() as $index=>$error)
                
                  @if($index==0)
                  <div class="col-md-12">
                    <div class="alert alert-danger">Please Fix the below errors and try again.</div></div>
                  @endif
                @endforeach
            @endif
            @if(session('error')) 
                <div class="col-md-12 error alert alert-danger alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Error : </strong>   {{ session('error') }}
                </div>
            @endif
            @if(session('success')) 
                <div class="col-md-12 error alert alert-success alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session('success') !!}
                </div>
            @endif
          <div class="middleContentOuter">
            <div class="verticalMiddle">
              <div class="loginForm contactForm">
                <h3 class=""><strong>Sign In</strong></h3>
                <p class="text-muted mb-4">The most personalized Real Prep study platform. </p>
                <form class="form-signin" method="POST" action="{{ route('login') }}">
                        @csrf
               
                  <div class="input-row">
                    <input type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email address" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-row">
                    
                    <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="custom-control custom-checkbox mb-3">
                    <!-- <input type="checkbox" class="custom-control-input" id="customCheck1"> -->
                     <input class="custom-control-input" type="checkbox" name="remember" id="customCheck1" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck1">Remember password</label>
                  </div>
                  <div class="input-row">
                    <input class="text-uppercase custom-btn1" type="submit" value="Sign in">
                  </div>
                  <div class="redirectLinks">
                  <p>Don't have an account? <a href="{{route('register')}}">click here</a> to create one.</p>
                  <p>Forgot Password? <a href="{{route('password.request')}}">click here</a> to reset password.</p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

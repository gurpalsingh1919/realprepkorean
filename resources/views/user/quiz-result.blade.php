@extends('layouts.user-app')
@section('content')
<style type="text/css">
   .content.quiz
   {
      padding: 0px !important;
   }
   .content.quiz .score
   {
      font-size: 25px;
      font-weight: bold;
      line-height: 35px;
   }
   .result .finish-quiz
   {
      display: inline-block;
      width: 200px;
      height: 51px;
      font-weight: bold;
   } 
   .result .finish-quiz a:hover {
    color: #fff;
    text-decoration: none;
   }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />
<div class="row" id="cancel-row" >
  <div class="col-lg-12 layout-spacing">
    <img src="{{ asset('home/images/logo.png') }}" class = 'tnq_logo img-fluid ml-4'>
    <div class = 'result test' style="margin-top: 300px;">
        
      @if($score < 80)
         <img src="{{ asset('home/images/fail.PNG') }}">
         <div class = 'pf'> You did not pass the quiz. </div>
      @else
         <img src="{{ asset('home/images/pass.PNG') }}">
         <div class = 'pf'> Well done. You passed! </div>

      @endif
         <div class = 'content quiz'> Your score: 
            <span class = 'score'><?php echo '   '.$score; ?> </span>
         </div>
         <div class = 'content quiz'> Passing score: 
            <span class = 'score'>80 </span>
         </div>
      @if ($score >= 80)
         <div class = 'finish-quiz nav-link formButton'>
            <a href="{{route('user.quiz_review',['quiz'=>$quiz,'date'=>$date])}}" 
             class = 'cursor'> REVIEW QUIZ </a>
         </div>
      @endif

      <div class = 'finish-quiz nav-link formButton transparentformButton'>
         <a href="{{route('user.quiz')}}" class = 'cursor'> FINISH </a>
      </div>
    </div>
  </div>
</div>
@endsection('content')
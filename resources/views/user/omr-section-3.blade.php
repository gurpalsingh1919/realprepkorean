@extends('layouts.user-app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />

  <?php $test=$student->test; ?>
  <div class="row" id="cancel-row">
    <div class="col-lg-12 layout-spacing">
      <div class = " col-lg-7 col-md-7 pdf_viewer tests">
       <div class ='window'>
          <iframe src ='{{ asset("user/file/test/".$test."-3.pdf")}}' width="100%" height="100%"></iframe>
        </div>
      </div>
      <div class ="col-lg-5 col-md-5 wrap omr tests">
        <form action="{{route('user.answer_check')}}" name = 'form' method="post">
          @csrf
          <input type = 'hidden' name = 'section' value ="{{$section}}">
          <input type = 'hidden' name = 'test' value = "{{$test}}">
          <input type = 'hidden' name = 'date' value = "{{$date}}">
          
          <div class = 'test omr'>
            <div class = 'logo'>
              <img src="{{ asset('home/images/logo.png') }}" class = 'omr_logo img-fluid' >
            </div>
            
            <div class = 'time'>
              <div class = 'time_left' id="countdown">Time left 25 : 00 </div>
            </div>
          
            <div class = 'page'>
              <div class = 'table'>
                <div class = 'row'>
                  <div class = 'cell'>
                    <?php for ($i = 1 ; $i < 9; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                  </div>
                  <div class = 'cell'>
                    <?php for ($i = 9 ; $i < 16; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+300;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                    
                  </div>
                </div>
              </div>
              

              
              
              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>16</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3161' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3161' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=3162; $i < 3164; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3164' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3164' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>17</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3171' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3171' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=3172; $i < 3174; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle slash'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3174' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3174' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>
              
              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>18</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3181' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3181' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=3182; $i < 3184; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3184' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3184' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>19</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3191' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3191' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=3192; $i < 3194; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle slash'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3194' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3194' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>

              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>20</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3201' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3201' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=3202; $i < 3204; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '3204' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '3204' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'></div>
                  <div class = 'cell'></div>
                  <div class = 'cell'></div>
                  <div class = 'cell'></div>
                  <div class = 'cell'></div>
               <button type="submit">Submit</button>
                  </div>
                </div>
              </div>
          </div>  
        </form>
      </div>
    </div>
  </div>
 
 <script type = "text/javascript">
        var SetTime = 1500;   // 최초 설정 시간(기본 : 1500초)

        function msg_time() { // 1초씩 카운트
          if((SetTime % 60)<10){  
            if((SetTime / 60)<10){ m = "0" + Math.floor(SetTime / 60) + " : 0" + (SetTime % 60); }  // 남은 시간 계산
            else { m = Math.floor(SetTime / 60) + " : 0" + (SetTime % 60);}
          }
          else{ 
            if((SetTime / 60)<10){ m =  "0" + Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
            else{ m =  Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
          }
          var msg = "Time left " + m;
          
          document.all.countdown.innerHTML = msg;   // div 영역에 보여줌 
              
          SetTime--;          // 1초씩 감소
          
          if (SetTime < 0) {      // 시간이 종료 되었으면..
            clearInterval(tid);   // 타이머 해제
            document.form.submit();
          }
        }

        window.onload = function TimerStart(){ tid=setInterval('msg_time()',1000) };
        
        
        
        var beforeChecked = -1;

        $(function(){
          $(document).on("click", "input[type=radio]", function(e) {
            var index = $(this).parent().index("label");
       
            if(beforeChecked == index) { 
              beforeChecked = -1;
              $(this).prop("checked", false);
            }
            else{    
              beforeChecked = index;
            }
          });
        });
      </script>
@endsection('content')
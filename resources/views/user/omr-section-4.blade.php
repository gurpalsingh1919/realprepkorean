@extends('layouts.user-app')
@section('content')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />
  <?php $test=$student->test; ?>
  <div class="row" id="cancel-row">
    <div class="col-lg-12 layout-spacing">
      <div class = " col-lg-7 col-md-7 pdf_viewer tests">
       <div class ='window'>
          <iframe src ='{{ asset("user/file/test/".$test_no."-4.pdf")}}' width="100%" height="100%"></iframe>
        </div>
      </div>
      <div class ="col-lg-5 col-md-5 wrap omr tests">
        <form action="{{route('user.answer_check')}}" name = 'form' method="post">
          @csrf
          <input type = 'hidden' name = 'section' value ="{{$section}}">
          <input type = 'hidden' name = 'test' value = "{{$$test_no}}">
          <input type = 'hidden' name = 'date' value = "{{$date}}">
          
          <div class = 'test omr'>
            <div class = 'logo'>
              <img src="{{ asset('home/images/logo.png') }}" class = 'omr_logo img-fluid' >
            </div>
            
            <div class = 'time'>
              <div class = 'time_left' id="countdown">Time left 55 : 00 </div>
            </div>
          
            <div class = 'page'>
              <div class = 'table'>
                <div class = 'row'>
                  <div class = 'cell'>
                    <?php for ($i = 1 ; $i < 16; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle last'><input type = "radio" name= "<?php echo $i+400;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                  </div>
                  <div class = 'cell'>
                    <?php for ($i = 16 ; $i < 31; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+400;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle last'><input type = "radio" name= "<?php echo $i+400;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                  </div>
                  
                </div> 
              </div>

          
              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>31</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4311' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4311' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4312; $i < 4314; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4314' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4314' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>32</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4321' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4321' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4322; $i < 4324; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4324' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4324' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>

              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>33</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4331' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4331' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4332; $i < 4334; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4334' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4334' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>34</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4341' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4341' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4342; $i < 4344; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4344' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4344' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>
    
              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>35</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4351' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4351' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4352; $i < 4354; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4354' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4354' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>36</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4361' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4361' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4362; $i < 4364; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4364' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4364' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>
      
              <div class = 'table sec'>
                <div class = 'row'>
                  <div class = 'cell'><div class = 'num'>37</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4371' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4371' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4372; $i < 4374; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4374' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4374' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>

                  <div class = 'cell'><div class = 'num'>38</div></div>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4381' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4381' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                  <?php for ($i=4382; $i < 4384; $i++){?>
                    <div class = 'cell'>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '/' ><span>/</span></label></div>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '<?php echo $i;?>' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                  <?php } echo '</div>';}?>
                  <div class = 'cell'>
                    <div><label class='circle period'><input type='radio' name = '4384' value = '.' ><span>.</span></label></div>
                    <?php for ($j=0; $j<10;$j++){?>
                      <div><label class='circle'><input type='radio' name = '4384' value = '<?php echo $j;?>'><span><?php echo $j;?></span></label></div>
                    <?php }?>
                  </div>
                </div>
              </div>
 
            </div>
          </div>  
        </form>
      </div>
    </div>
  </div>
 
<script type = "text/javascript">
  var SetTime = 3300;   // 최초 설정 시간(기본 : 3300초)

  function msg_time() { // 1초씩 카운트
    if((SetTime % 60)<10){  
      if((SetTime / 60)<10){ m = "0" + Math.floor(SetTime / 60) + " : 0" + (SetTime % 60); }  // 남은 시간 계산
      else { m = Math.floor(SetTime / 60) + " : 0" + (SetTime % 60);}
    }
    else{ 
      if((SetTime / 60)<10){ m =  "0" + Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
      else{ m =  Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
    }
    var msg = "Time left " + m;
    
    document.all.countdown.innerHTML = msg;   // div 영역에 보여줌 
        
    SetTime--;          // 1초씩 감소
    
    if (SetTime < 0) {      // 시간이 종료 되었으면..
      clearInterval(tid);   // 타이머 해제
      alert("Time's up. The test is over.");
      document.form.submit();
    }
  }

  window.onload = function TimerStart(){ tid=setInterval('msg_time()',1000) };
  
  
  
  var beforeChecked = -1;

  $(function(){
    $(document).on("click", "input[type=radio]", function(e) {
      var index = $(this).parent().index("label");
 
      if(beforeChecked == index) { 
        beforeChecked = -1;
        $(this).prop("checked", false);
      }
      else{    
        beforeChecked = index;
      }
    });
  });
</script>
@endsection('content')
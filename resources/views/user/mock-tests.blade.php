@extends('layouts.user-app')
@section('content')
<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Mock Test</b></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
            <div class="row">
              <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                  <h2><b>Today's Test</b></h2><hr/>
              </div>           
            </div>
          </div>
          @if($errors->all())
            @foreach ($errors->all() as $index=>$error)
             <div class="alert alert-danger">{{$error}}
              </div>
            @endforeach
          @endif
          @if(session('error'))
            <div class="error alert alert-danger alert-dismissable">
                <a href="#"  class="close" data-dismiss="alert"
                   aria-label="close">&times;</a>
                <strong>Error : </strong> {!! session('error') !!}
            </div>
          @endif
          @if(session('success'))
            <div class="error alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert"
                   aria-label="close">&times;</a>
                {!! session('success') !!}
            </div>
          @endif
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Test</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                 @if(isset($student->test) && $student->test >0)
                  <tr class="tablerow">
                    <td>1</td>
                    <td class="user-email">Test {{$student->test}}</td>
                    <td class="user-mobile nav-item">
                      <form action = "{{route('user.start_mock_test')}}" method = "get" >
                       
                        <input type = 'hidden' name = 'type' value = 'test'           >
                        <input type = 'hidden' name = 'test' value = '{{$student->test}}'    >
                        <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                      </form>

                      
                    </td>
                  </tr>
                @else
                  <tr class="tablerow"><td colspan="3" class="text-center" >No Test Assigned for Today !!</td></tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                      <h2><b>Previous Test</b></h2>
                  </div>
              </div><hr/>
          </div>
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Test</th>
                  <th><span class = 'rw'>Evidence Based </span> <br> Reading & Writing </th>
                  <th>Math</th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(count($scoreTests) >0)
                  @foreach($scoreTests as $test)
                    <tr class="tablerow">
                      <td>{{$loop->iteration}}</td>
                      <td class="user-name">{{ \Carbon\Carbon::parse($test->test_date)->format('F jS, Y')}}</td>
                      <td class="user-email">{{$test->test}}</td>
                      <td class="user-phone">{{$test->section_rw}}</td>
                      <td class="user-mobile">{{$test->section_m}}</td>
                      <td class="user-mobile">{{$test->total}}</td>
                      <td class="user-mobile nav-item">
                          <a href="{{route('user.test_review',['test'=>$test->test,'date'=>$test->test_date])}}" class = 'button-lnk nav-link formButton transparentformButton'> Review </a>
                        </td>
                    </tr>
                   @endforeach
                @else
                  <tr colspan="5">No Result found !!</tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
@extends('layouts.user-app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />
  <?php $test=$student->test; ?>
  <div class="row" id="cancel-row">
    <div class="col-lg-12 layout-spacing">
      <div class = " col-lg-7 col-md-7 pdf_viewer tests">
       <div class ='window'>
          <iframe src ='{{ asset("user/file/test/".$test."-2.pdf")}}' width="100%" height="100%"></iframe>
        </div>
      </div>
      <div class ="col-lg-5 col-md-5 wrap omr tests">
        <form action="{{route('user.answer_check')}}" name = 'form' method="post">
          @csrf
          <input type = 'hidden' name = 'section' value ="{{$section}}">
          <input type = 'hidden' name = 'test' value = "{{$test}}">
          <input type = 'hidden' name = 'date' value = "{{$date}}">
          
          <div class = 'test omr'>
            <div class = 'logo'>
              <img src="{{ asset('home/images/logo.png') }}" class = 'omr_logo img-fluid' >
            </div>
            
            <div class = 'time'>
              <div class = 'time_left' id="countdown">Time left 35 : 00 </div>
            </div>
          
            <div class = 'page'>
              <div class = 'table'>
                <div class = 'row'>
                  <div class = 'cell'>
                    <?php for ($i = 1 ; $i < 23; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle last'><input type = "radio" name= "<?php echo $i+200;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                  </div>
                  <div class = 'cell'>
                    <?php for ($i = 23 ; $i < 45; $i++){?>
                      <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "1"><span>A</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "2"><span>B</span> </label>
                        <label class = 'circle'><input type = "radio" name= "<?php echo $i+200;?>" value = "3"><span>C</span> </label>
                        <label class = 'circle last'><input type = "radio" name= "<?php echo $i+200;?>" value = "4"><span>D</span> </label> </div>
                    <?php }?>
                  </div>
                </div> 
              </div>
            </div>
          </div>  
          
        </form>
      </div>
    </div>
  </div>
 <script type = "text/javascript">
        var SetTime = 2100;   // 최초 설정 시간(기본 : 2100초)

        function msg_time() { // 1초씩 카운트
          if((SetTime % 60)<10){  
            if((SetTime / 60)<10){ m = "0" + Math.floor(SetTime / 60) + " : 0" + (SetTime % 60); }  // 남은 시간 계산
            else { m = Math.floor(SetTime / 60) + " : 0" + (SetTime % 60);}
          }
          else{ 
            if((SetTime / 60)<10){ m =  "0" + Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
            else{ m =  Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
          }
          var msg = "Time left " + m;
          
          document.all.countdown.innerHTML = msg;   // div 영역에 보여줌 
              
          SetTime--;          // 1초씩 감소
          
                  
          if (SetTime < 0) {      // 시간이 종료 되었으면..
            clearInterval(tid);   // 타이머 해제
            document.form.submit();
          }
        }

        window.onload = function TimerStart(){ tid=setInterval('msg_time()',1000) };
        
        
        
        var beforeChecked = -1;

        $(function(){
          $(document).on("click", "input[type=radio]", function(e) {
            var index = $(this).parent().index("label");
       
            if(beforeChecked == index) { 
              beforeChecked = -1;
              $(this).prop("checked", false);
            }
            else{    
              beforeChecked = index;
            }
          });
        });
      </script>
@endsection('content')
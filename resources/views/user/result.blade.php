@extends('layouts.user-app')
@section('content')
<style type="text/css">
   .content.quiz
   {
      padding: 0px !important;
   }
   .content.quiz .score
   {
      font-size: 25px;
      font-weight: bold;
      line-height: 35px;
   }
   .result .finish-quiz
   {
      display: inline-block;
      width: 200px;
      height: 51px;
      font-weight: bold;
   } 
   .result .finish-quiz a:hover {
    color: #fff;
    text-decoration: none;
   }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />
<div class="row" id="cancel-row" >
  <div class="col-lg-12 layout-spacing">
    <img src="{{ asset('home/images/logo.png') }}" class = 'tnq_logo img-fluid ml-4'>
    <div class = 'result test' style="margin-top: 300px;">
        <div class = 'content quiz'> You scored </div>
        <div class = 'score'><?php echo $total; ?></div>
        <div class = 'section_score rw'> <?php echo $rw;?></div>
        <div class = 'section_score math'> <?php echo $m;?> </div>
        <div class = 'section rw'> <span class = 'rw'>Evidence Based</span> <br> Reading & Writing</div>
        <div class = 'section math'> Math </div>
        

        <!-- <div class = 'review'>
            <a href="{{route('user.test_review',['test'=>$test,'date'=>$date])}}" 
                class = 'cursor'> REVIEW TEST </a></div>
        <div class = 'finish'><a href="{{route('user.mock_test')}}" class = 'cursor'> FINISH </a></div> -->
        <div class = 'finish-quiz nav-link formButton mt-4'>
            <a href="{{route('user.test_review',['test'=>$test,'date'=>$date])}}" 
             class = 'cursor'> REVIEW TEST </a>
         </div>
        <div class = 'finish-quiz nav-link formButton transparentformButton mt-4'>
         <a href="{{route('user.mock_test')}}" class = 'cursor'> FINISH </a>
      </div>
    </div>
  </div>
</div>
@endsection('content')
<div class="modernSidebar-nav header navbar">
   <div class="">
     <nav id="modernSidebar">
         <ul class="menu-categories pl-0 m-0" id="topAccordion">
            <li class="menu">
              <a href="{{route('user.dashboard')}}" class="dropdown-toggle">
                  <div class="{{(Route::current()->getName()=='user.dashboard')? 'active':''}}">
                      <i class="flaticon-computer-6"></i>
                      <span>Dashboard</span>
                  </div>
              </a>
              
            </li>

            <li class="menu">
              <a href="{{route('user.my_lectures')}}" class="dropdown-toggle collapsed">
                  <div class="{{(Route::current()->getName()=='user.my_lectures')? 'active':''}}">
                      <i class="flaticon-3d-cube"></i>
                      <span>Lecture</span>
                  </div>
              </a>
             <!--  <div class="submenu list-unstyled collapse eq-animated eq-fadeInUp" id="uiAndComponents" data-parent="#topAccordion">
                  <div class="submenu-scroll">
                      <ul class="list-unstyled">
                          <li>
                              <a href="#ui-features" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">UI KIT</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="ui-features">
                                  <li>
                                      <a href="ui_helper_classes.html"> Helper Classes </a>
                                  </li>
                                  <li>
                                      <a href="ui_color_library.html"> Color Library </a>
                                  </li>
                                  <li>
                                      <a href="ui_grid.html"> Grid System </a>
                                  </li>
                                  <li>
                                      <a href="ui_typography.html"> Typography </a>
                                  </li>
                                  <li>
                                      <a href="ui_shadows.html"> Shadow </a>
                                  </li>
                                  <li>
                                      <a href="ui_miscellaneous.html"> Miscellaneous </a>
                                  </li>
                                  <li>
                                      <a href="ui_pagination.html">Pagination</a>
                                  </li>
                                  <li>
                                      <a href="ui_alert.html"> Alerts </a>
                                  </li>
                                  <li>
                                      <a href="ui_tooltips_and_popovers.html"> Tooltips And Popovers </a>
                                  </li>
                                  <li>
                                      <a href="ui_loader.html"> Loaders </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#ui-buttons" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Buttons <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="ui-buttons" data-parent="#ui-features">
                                          <li>
                                              <a href="ui_buttons.html"> Bootstrap </a>
                                          </li>
                                          <li>
                                              <a href="ui_creative.html"> Creative </a>
                                          </li>
                                          <li>
                                              <a href="ui_switches.html"> Switches </a>
                                          </li>
                                          <li>
                                              <a href="ui_social_button.html"> Social </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="ui_sweetalert.html"> Sweet Alerts </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#ui-font-icons" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Font Icons <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="ui-font-icons" data-parent="#ui-features">
                                          <li>
                                              <a href="ui_flaticon_icon.html"> Flaticon </a>
                                          </li>
                                          <li>
                                              <a href="ui_linea_icon.html"> Linea </a>
                                          </li>
                                          <li>
                                              <a href="ui_themify_icon.html"> Themify </a>
                                          </li>
                                          <li>
                                              <a href="ui_pixeden_icon.html"> Pixeden </a>
                                          </li>
                                          <li>
                                              <a href="ui_fontawesome_icon.html"> Fontawesome </a>
                                          </li>
                                          <li>
                                              <a href="ui_metro_icon.html"> Metro </a>
                                          </li>
                                          <li>
                                              <a href="ui_weather_icon.html"> Weather </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="ui_tabs.html"> Tabs </a>
                                  </li>
                                  <li>
                                      <a href="ui_accordion.html"> Accordions  </a>
                                  </li>
                                  <li>
                                      <a href="ui_modal.html"> Modals </a>
                                  </li>
                                  <li>
                                      <a href="ui_sliders.html"> Range Sliders </a>
                                  </li>
                                  <li>
                                      <a href="ui_timeline.html"> Timeline </a>
                                  </li>
                                  <li>
                                      <a href="ui_tree.html"> Tree View </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#ui-progress" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Progress Bar <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="ui-progress" data-parent="#ui-features">
                                          <li>
                                              <a href="ui_bootstrap_progress_bar.html"> Bootstrap </a>
                                          </li>
                                          <li>
                                              <a href="ui_material_progress_bar.html"> Custom </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#ui-notification" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Notifications <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="ui-notification" data-parent="#ui-features">
                                          <li>
                                              <a href="ui_toastr.html"> Toastr </a>
                                          </li>
                                          <li>
                                              <a href="ui_notification.html"> Creative </a>
                                          </li>
                                          <li>
                                              <a href="ui_snackbar.html"> Snackbar </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="ui_session_timeout.html"> Session Timeout </a>
                                  </li>
                                  <li>
                                      <a href="ui_media_object.html"> Media Object </a>
                                  </li>
                                  <li>
                                      <a href="ui_list_group.html"> List Group </a>
                                  </li>
                                  <li>
                                      <a href="ui_nestable_list.html"> Nested List </a>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                      <ul class="list-unstyled">
                          <li>
                              <a href="#components" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">COMPONENTS</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="components">
                                  <li>
                                      <a href="component_portlet.html"> Portlets </a>
                                  </li>
                                  <li>
                                      <a href="component_keypad.html"> Keypad </a>
                                  </li>

                                  <li class="sub-sub-submenu-list">
                                      <a href="#component-carousel" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Carousel <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="component-carousel" data-parent="#components">      
                                          <li>
                                              <a href="component_bootstrap_carousel.html">Bootstrap</a>
                                          </li>
                                          <li>
                                              <a href="component_carousel.html">Metro</a>
                                          </li>
                                          <li>
                                              <a href="component_sliders.html">Swiper</a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="component_rating.html"> Rating </a>
                                  </li>
                                  <li>
                                      <a href="component_blockui.html"> Block UI </a>
                                  </li>
                                  <li>
                                      <a href="component_popup.html"> Popup </a>
                                  </li>
                                  <li>
                                      <a href="component_scroll_bars_basic.html"> Scrollbar </a>
                                  </li>
                                  <li>
                                      <a href="component_todo_list.html"> Todo List </a>
                                  </li>
                                  <li>
                                      <a href="component_scrollspy.html"> Scrollspy </a>
                                  </li>
                                  <li>
                                      <a href="component_countdown.html"> Countdown </a>
                                  </li>

                                  <li>
                                      <a href="component_counter.html"> Counter </a>
                                  </li>
                                  <li>
                                      <a href="component_datetime_picker.html"> Date &amp; Time Picker </a>
                                  </li>
                                  <li>
                                      <a href="component_lightbox.html"> LightBox </a>
                                  </li>
                                  <li>
                                      <a href="component_color_picker.html"> Color Picker </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#component-media" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Media <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="component-media" data-parent="#components"> 
                                          <li>
                                              <a href="component_video_player.html"> Video Player </a>
                                          </li>
                                          <li>
                                              <a href="component_audio_player.html"> Audio Player </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="component_clipboard.html"> Clipboard </a>
                                  </li>
                                  <li>
                                      <a href="component_typeahead.html"> Typeahead </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#component-search" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Search <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="component-search" data-parent="#components"> 
                                          <li>
                                              <a href="component_search_multiple.html"> Multiple </a>
                                          </li>
                                          <li>
                                              <a href="component_search_web.html"> Web </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#component-animation" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Animations <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="component-animation" data-parent="#components"> 
                                          <li>
                                              <a href="component_animation_buttons.html"> Buttons </a>
                                          </li>
                                          <li>
                                              <a href="component_animation_css.html"> CSS </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#component-img-crop" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Image Cropping <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="component-img-crop" data-parent="#components"> 
                                          <li>
                                              <a href="component_image_cropping_basic.html"> Basic </a>
                                          </li>
                                          <li>
                                              <a href="component_image_cropping_advanced.html"> Advanced </a>
                                          </li>
                                      </ul>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                      <ul class="list-unstyled">
                          <li>
                              <a href="#element" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">ELEMENTS</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="element">
                                  <li>
                                      <a href="element_steps.html"> Steps </a>
                                  </li>
                                  <li>
                                      <a href="element_ribbon.html"> Ribbons </a>
                                  </li>
                                  <li>
                                      <a href="element_testimonial.html"> Testimonials </a>
                                  </li>
                                  <li>
                                      <a href="element_team.html"> Team </a>
                                  </li>
                                  <li>
                                      <a href="element_contacts.html"> Contacts </a>
                                  </li>
                                  <li>
                                      <a href="element_blog.html"> Blog Elements </a>
                                  </li>
                                  <li>
                                      <a href="element_pricing_table.html"> Pricing Tables </a>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div> -->
            </li>

            <li class="menu">
              <a href="{{route('user.mock_test')}}" class="dropdown-toggle collapsed">
                  <div class="{{(Route::current()->getName()=='user.mock_test')? 'active':''}}">
                      <i class="flaticon-table"></i>
                      <span>Mock Test</span>
                  </div>
              </a>
              <!-- <div class="collapse submenu list-unstyled eq-animated eq-fadeInUp" id="mock-test" data-parent="#topAccordion">
                  <ul class="submenu-scroll">
                      <li>
                          <ul class="list-unstyled sub-submenu" id="dashboards">
                              
                              <li>
                                  <a href="{{route('user.mock_test')}}"> <div><i class="flaticon-computer-4"></i> My Test</div> </a>
                              </li>
                              <li>
                                 
                                   <a href="{{route('user.all_test_review')}}"> <div><i class="flaticon-stats"></i> Review Test</div> </a>
                                 
                              </li>
                          </ul>
                      </li>
                  </ul>
              </div> -->
              
            </li>

            <li class="menu">
              <a href="{{route('user.quiz')}}" class="dropdown-toggle collapsed">
                  <div class="{{(Route::current()->getName()=='user.quiz')? 'active':''}}">
                      <i class="flaticon-copy"></i>
                      <span>Daily Quiz </span>
                  </div>
              </a>
              <!-- <div class="submenu list-unstyled collapse eq-animated eq-fadeInUp" id="pages" data-parent="#topAccordion">
                  <div class="submenu-scroll">
                      <ul class="list-unstyled">
                          <li>
                              <a href="#ecommerce" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">ECOMMERCE</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="ecommerce">
                                  <li>
                                      <a href="ecommerce_orders.html"> Orders </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_product.html"> Products </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_product_catalog.html"> Product Catalog </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#product-details" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" data-parent="#ecommerce"> Product Details <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="product-details">
                                          <li>
                                              <a href="ecommerce_product_details_1.html"> Product Details 1 </a>
                                          </li>
                                          <li>
                                              <a href="ecommerce_product_details_2.html"> Product Details 2 </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="ecommerce_addedit_product.html"> Add/Edit Products </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_addedit_categories.html"> Add/Edit Categories </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_view_cart.html"> View Cart </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_view_payments.html"> View Payments </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_view_customers.html"> View Customers </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_checkout.html"> Checkout </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_invoices.html"> Invoice </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_shipments.html"> Shipments </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_products_cart.html"> Products in Cart </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_coupons.html"> Coupons </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_low_stock.html"> Low Stock </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_best_sellers.html"> Best Sellers </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_refunds.html"> Refunds </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_search_terms.html"> Search Terms </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_newsletters.html"> Newsletters </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_wizards.html"> Payment Wizard </a>
                                  </li>
                                  <li>
                                      <a href="ecommerce_reviews.html"> Reviews </a>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                      <ul class="list-unstyled">
                          <li>
                              <a href="#page" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">PAGES</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="page">
                                  <li>
                                      <a href="pages_blank_page.html"> Blank Page</a>
                                  </li>
                                  <li>
                                      <a href="pages_helpdesk.html"> Helpdesk </a>
                                  </li>
                                  <li>
                                      <a href="pages_contact_us.html"> Contact Form </a>
                                  </li>
                                  <li>
                                      <a href="pages_faq.html"> FAQ </a>
                                  </li>
                                  <li>
                                      <a href="pages_blog.html"> Blog </a>
                                  </li>                            
                                  <li>
                                      <a href="pages_privacy.html"> Privacy Policy </a>
                                  </li>
                                  <li>
                                      <a href="pages_cookie_consent.html"> Cookie Consent </a>
                                  </li>
                                  <li>
                                      <a href="pages_landing_page.html" target="_blank"> Landing Page </a>
                                  </li>
                                  <li>
                                      <a href="pages_coming_soon.html"> Coming Soon </a>
                                  </li>

                                  <li class="sub-sub-submenu-list">
                                      <a href="#pages-error" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Error <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="pages-error" data-parent="#pages"> 
                                          <li>
                                              <a href="pages_error404.html"> 404 1 </a>
                                          </li>
                                          <li>
                                              <a href="pages_error404-2.html"> 404 2 </a>
                                          </li>
                                          <li>
                                              <a href="pages_error500.html"> 500 1 </a>
                                          </li>
                                          <li>
                                              <a href="pages_error500-2.html"> 500 2 </a>
                                          </li>
                                          <li>
                                              <a href="pages_error503.html"> 503 1 </a>
                                          </li>
                                          <li>
                                              <a href="pages_error503-2.html"> 503 2 </a>
                                          </li>
                                          <li>
                                              <a href="pages_maintenence.html"> Maintanence </a>
                                          </li>
                                      </ul>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                      <ul class="list-unstyled">
                          <li>
                              <a href="#users" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle"> <span class="">USERS</span> </a>
                              <ul class="list-unstyled sub-submenu collapse show eq-animated eq-fadeInUp" id="users"> 
                                  <li>
                                      <a href="user_profile.html"> Profile </a>
                                  </li>
                                  <li>
                                      <a href="user_account_setting.html"> Account Settings </a>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#user-login" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Login <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="user-login" data-parent="#users"> 
                                          <li>
                                              <a href="user_login_1.html"> Login 1 </a>
                                          </li>
                                          <li>
                                              <a href="user_login_2.html"> Login 2 </a>
                                          </li>
                                          <li>
                                              <a href="user_login_3.html"> Login 3 </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#user-register" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Register <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="user-register" data-parent="#users"> 
                                          <li>
                                              <a href="user_register_1.html"> Register 1 </a>
                                          </li>
                                          <li>
                                              <a href="user_register_2.html"> Register 2 </a>
                                          </li>
                                          <li>
                                              <a href="user_register_3.html"> Register 3 </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#user-passRecovery" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Password Recovery <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="user-passRecovery" data-parent="#users"> 
                                          <li>
                                              <a href="user_pass_recovery_1.html"> Password Recovery 1 </a>
                                          </li>
                                          <li>
                                              <a href="user_pass_recovery_2.html"> Password Recovery 2 </a>
                                          </li>
                                          <li>
                                              <a href="user_pass_recovery_3.html"> Password Recovery 3 </a>
                                          </li>
                                      </ul>
                                  </li>
                                  <li class="sub-sub-submenu-list">
                                      <a href="#user-lockscreen" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Lockscreen <i class="flaticon-right-arrow"></i> </a>
                                      <ul class="collapse list-unstyled sub-submenu eq-animated eq-fadeInUp" id="user-lockscreen" data-parent="#users"> 
                                          <li>
                                              <a href="user_lockscreen_1.html"> Lockscreen 1 </a>
                                          </li>
                                          <li>
                                              <a href="user_lockscreen_2.html"> Lockscreen 2 </a>
                                          </li>
                                          <li>
                                              <a href="user_lockscreen_3.html"> Lockscreen 3 </a>
                                          </li>
                                      </ul>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div> -->
            </li>

            <li class="menu">
              <a href="{{route('user.course_material')}}" class="dropdown-toggle collapsed" >
                  <div class="{{(Route::current()->getName()=='user.course_material')? 'active':''}}">
                      <i class="flaticon-mail-19"></i>

                      <span>Course Material</span>
                  </div>
              </a>
            </li>

            <li class="menu">
              <a href="{{route('user.announcements')}}" class="dropdown-toggle collapsed" >
                  <div class="{{(Route::current()->getName()=='user.announcements')? 'active':''}}">
                      <i class="flaticon-table"></i>

                      <span>Announcements</span>
                  </div>
              </a>
            </li>
            <li class="menu">
              <a href="{{ route('logout') }}" class="dropdown-toggle collapsed" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <div class="">
                      <i class="flaticon-power-off"></i>

                      <span>Logout</span>
                  </div>
              </a>
            </li>
         </ul>
     </nav>
   </div>
</div>
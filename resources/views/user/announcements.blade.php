@extends('layouts.user-app')
@section('content')

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Announcements</b></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 layout-spacing"></div>
      <div class="col-lg-8 layout-spacing">
          <div class="statbox widget box box-shadow">
              <div class="widget-header">
                  <div class="row">
                      <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                          <h4>Announcements</h4>
                      </div>
                  </div>
              </div> 
              <div class="widget-content widget-content-area creative2-accordion-content">
                  

                    @if(count($boards) >0)
                      @foreach($boards as $board)
                      <div id="creativeAccordion-{{$board->id}}">
                        <div class="card mb-3">
                          <div class="card-header" id="creative2headingOne-{{$board->id}}">
                            <h5 class="mb-0 mt-0">
                              <span role="menu" class="" data-toggle="collapse" data-target="#creative2CollapseOne-{{$board->id}}" aria-expanded="true" aria-controls="creative2CollapseOne-{{$board->id}}">
                                  <span class="icon">
                                       <i class="flaticon-plus-1"></i>
                                  </span>
                                  <span class="text ml-2">
                                      <b>{{$board->title}}</b>
                                  </span>
                                  <span class="text float-right mt-2">
                                      <b>{{ \Carbon\Carbon::parse($board->date)->isoFormat('MMM Do YY')}}</b>
                                  </span>
                              </span>
                            </h5>
                          </div>

                          <div id="creative2CollapseOne-{{$board->id}}" class="collapse" aria-labelledby="creative2headingOne-{{$board->id}}" data-parent="#creativeAccordion-{{$board->id}}">
                            <div class="card-body">
                              <h4 class="mb-4">{{$board->title}}</h4>
                                  <p class="mb-3">
                                    {!! nl2Br($board->content) !!}                                            
                                  </p>

                                 
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    @else
                      <div class="text-center">No Result found !!</div>
                    @endif
                 
              </div>
          </div>
      </div>
      <div class="col-lg-2 layout-spacing"></div>
    </div>
    
  </div>
</div>

@endsection('content')
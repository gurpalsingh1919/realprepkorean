@extends('layouts.user-app')
@section('content')

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
       <!-- <div class="page-title">
          <h3>Lectures</h3>
       </div> -->
    </div>
    <div class="row mt-4" id="cancel-row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Today's Test</h4>
                   
                  </div>                      
              </div>
          </div>
          <div class="widget-content widget-content-area">
              <div class="row">
                  <div class="col-lg-12">
                     @if($student->test >0)
                      <section class="pricing-section bg-11">
                        <div class="pricing pricing--norbu">
                           <div class = 'start test'>
                              <?php $test = $student->test;?>
                              
                              <div class = "table test">  
                                <div class = 'row'>
                                  <div class = 'cell no col-md-12 text-center'> Test <?php echo $test;?> <a href = "{{route('user.mock_test')}}" class = 'close'>X</a></div>
                                </div>
                                <div class = 'row'>
                                  <div class = 'cell content'>If you click the button below, your test will begin. Start the test when you are ready. <br> If you leave during the test, your answers will not be saved. Stay on the page when you finish the section.</div><br/>
                                </div>
                                <div class = 'row'>
                                  <div class = 'cell warning'>Warning: Once you click the 'GET STARTED' button, your have only one chance to access each section! <br> Do not reload or leave the page after the test begins.</div>
                                </div>
                                <div class = 'row mt-4'>
                                  <div class = 'cell button col-md-12 text-center'>
                                    <a href="{{route('user.load_mock_test',['test'=>$test,'section'=>'1'])}}" class ='button'> GET STARTED </a></div>
                                </div>
                              </div>
                            </div>
                        </div>
                      </section> 

                     
                      
                      @endif   
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type = "text/javascript">
      function redirect()
      {
        setTimeout(function(){
          location.replace("../php/test_answer_check.php?test=<?php echo $test;?>");},1000);}
      
      function locationReplace(){
        location.replace("../index.php");
      }
    </script>
@endsection('content')
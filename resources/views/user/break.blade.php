@extends('layouts.user-app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />

  
    <div class="row" id="cancel-row">
      <div class="col-lg-12 layout-spacing">
        <img src="{{ asset('home/images/logo.png') }}" class = 'tnq_logo img-fluid ml-4'><!-- logo -->
    
    <div class = 'break' style="margin-top: 170px;">
      <div class = 'box'>
        <p class = 'content title'>Break</p>
        <p id="countdown" class = 'countdown'>10 : 00 </p>
      </div>
    </div>
      <input type="hidden" name="section_2" id="section_2" value='{{route("user.load_mock_test",["test"=>$test_no,"section"=>2])}}'>
      <input type="hidden" name="section_4" id="section_4" value='{{route("user.load_mock_test",["test"=>$test_no,"section"=>4])}}'>
      
    </div>
    </div>
<script type = "text/javascript">
    var SetTime = 599;    // 최초 설정 시간(기본 : 599초)
    var brk = "<?php echo $break;?>";
      function msg_time() { // 1초씩 카운트
        if((SetTime % 60)<10){  m = "0" + Math.floor(SetTime / 60) + " : 0" + (SetTime % 60); // 남은 시간 계산
        }
        else{ m = "0" + Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
        
        var msg = m;
        document.all.countdown.innerHTML = msg;   // div 영역에 보여줌 
            
        SetTime--;          // 1초씩 감소
        
        if (SetTime < 0) {      // 시간이 종료 되었으면..
          clearInterval(tid); 
          var section_2 = $("#section_2").val();
          var section_4 = $("#section_4").val();  // 타이머 해제
          if (brk ==1)  
          { 
            window.location.href = section_2;
          }
          else if (brk ==2)
          { 
            window.location.href = section_4;
          }
        } 
      }

      window.onload = function TimerStart(){ tid=setInterval('msg_time()',1000) };
      
    </script>
@endsection('content')
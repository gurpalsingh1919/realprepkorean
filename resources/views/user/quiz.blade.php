@extends('layouts.user-app')
@section('content')
<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Quizzes</b></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
            <div class="row">
              <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                  <h2><b>Today's Quizzes</b></h2><hr/>
              </div>           
            </div>
          </div>
          @if($errors->all())
            @foreach ($errors->all() as $index=>$error)
             <div class="alert alert-danger">{{$error}}
              </div>
            @endforeach
          @endif
          @if(session('error'))
            <div class="error alert alert-danger alert-dismissable">
                <a href="#"  class="close" data-dismiss="alert"
                   aria-label="close">&times;</a>
                <strong>Error : </strong> {!! session('error') !!}
            </div>
          @endif
          @if(session('success'))
            <div class="error alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert"
                   aria-label="close">&times;</a>
                {!! session('success') !!}
            </div>
          @endif
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Quiz</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($student->quiz1) && $student->quiz1 >0)
                  <tr class="tablerow">
                    <td>1</td>
                    <td class="user-email">Day {{$student->quiz1}}</td>
                    <td class="user-mobile nav-item">
                      <form action = "{{route('user.start_quiz')}}" method = "get" >
                       
                        <input type = 'hidden' name = 'type' value = 'quiz1'           >
                        <input type = 'hidden' name = 'quiz' value = '{{$student->quiz1}}'>
                        <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz1'> -->
                        <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                      </form>

                      
                    </td>
                  </tr>
                @endif
                @if(isset($student->quiz2) && $student->quiz2 >0)
                  <tr class="tablerow">
                    <td>2</td>
                    <td class="user-email">Day {{$student->quiz2}}</td>
                    <td class="user-mobile nav-item">
                      <form action = "{{route('user.start_quiz')}}" method = "get" >
                       
                        <input type = 'hidden' name = 'type' value = 'quiz2'>
                        <input type = 'hidden' name = 'quiz' value = '{{$student->quiz2}}'>
                        <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz2'> -->
                        <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                      </form>

                      
                    </td>
                  </tr>
                @endif
                @if(isset($student->quiz3) && $student->quiz3 >0)
                  <tr class="tablerow">
                    <td>3</td>
                    <td class="user-email">Day {{$student->quiz3}}</td>
                    <td class="user-mobile nav-item">
                      <form action = "{{route('user.start_quiz')}}" method = "get" >
                       
                        <input type = 'hidden' name = 'type' value = 'quiz3'           >
                        <input type = 'hidden' name = 'quiz' value = '{{$student->quiz3}}'    >
                        <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz3'> -->
                        <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                      </form>

                      
                    </td>
                  </tr>
                @endif
                @if((isset($student->quiz1) && $student->quiz1 =='0') && (isset($student->quiz2) && $student->quiz2 =='0') && (isset($student->quiz3) && $student->quiz3 =='0'))
                  <tr class="tablerow"><td colspan="3" class="text-center" >No Quiz Assigned for Today !!</td></tr>
                @endif 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                      <h2><b>Previous Quizzes</b></h2>
                  </div>
              </div><hr/>
          </div>
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Quiz</th>
                  <th>Score</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(count($ScoreQuiz) >0)
                  @foreach($ScoreQuiz as $quiz)
                    <tr class="tablerow">
                      <td>{{$loop->iteration}}</td>
                      <td class="user-name">{{ \Carbon\Carbon::parse($quiz->test_date)->format('F jS, Y')}}</td>
                      <td class="user-email">Day {{$quiz->quiz}}</td>
                      <td class="user-mobile">{{$quiz->score}}</td>
                      <td class="user-mobile nav-item">
                          <a href="{{route('user.quiz_review',['quiz'=>$quiz->quiz,'date'=>$quiz->quiz_date])}}" class = 'button-lnk nav-link formButton transparentformButton'> Review </a>
                        </td>
                    </tr>
                   @endforeach
                @else
                  <tr class="tablerow"><td colspan="3" class="text-center" >
                   No Quizzes Found !!</td></tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
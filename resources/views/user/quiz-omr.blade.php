@extends('layouts.user-app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/omr.css') }}" />
  
  <div class="row" id="cancel-row">
    <div class="col-lg-12 layout-spacing">
      <div class = " col-lg-7 col-md-7 pdf_viewer quiz">
       <div class ='window'>
          <iframe src ='{{ asset("user/file/quiz/".$quiz.".pdf")}}' width="100%" height="100%"></iframe>
        </div>
      </div>
      <div class ="col-lg-5 col-md-5 wrap omr quiz">
        <form action="{{route('user.quiz_answer_check')}}" name = 'form' method="post">
          @csrf
          <input type = 'hidden' name = 'quiz' value ="{{$quiz}}">
          <input type = 'hidden' name = 'type' value = "{{$type}}">
          <input type = 'hidden' name = 'date' value = "{{$date}}">

          <div class = 'quiz omr'>
              <div class = 'logo'><img src="{{ asset('home/images/logo.png') }}" class = 'omr_logo img-fluid'><!-- logo --></div>
              
              <div class = 'time'>
                <div class = 'time_left' id="countdown">Time left 30 : 00 </div>
              </div>
              
              <div class = 'float-right'>
                <input type = "submit" class = 'nav-link formButton transparentformButton' value = "Submit">
              </div>
              <div class = 'page'>
                <div class = 'table part1'>
                  <div class = 'row'>
                    <div class = 'cell section'>PART 1 <span class = 'subtitle'> FIND THE CORRECT MEANING </span></div>
                  </div>
                  <div class = 'row part1'>
                    <div class = 'cell'>
                      <?php for ($i = 1 ; $i < 26; $i++){?>
                        <div> <span class = 'number'><?php if ($i<10){echo $i.'&ensp;';}  else{echo$i;}?> </span>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "1"><span>A</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "2"><span>B</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "3"><span>C</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "4"><span>D</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "5"><span>E</span> </label> </div>
                      <?php }?>
                    </div>
                  </div>
                </div>
                
                
                <div class = 'table part2'>
                  <div class = 'row'>
                    <div class = 'cell section'> PART 2 <span class = 'subtitle'> FIND THE CORRECT WORD   </span></div>
                  </div>
                  <div class = 'row part2'>
                    <div class = 'cell'>
                      <?php for ($i = 26 ; $i < 51; $i++){?>
                        <div> <span class = 'number'><?php echo $i;?> </span>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "1" ><span>A</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "2" ><span>B</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "3" ><span>C</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "4" ><span>D</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "5" ><span>E</span> </label></div>
                      <?php }?>
                    </div>
                  </div>
                </div>
                
                
                <div class = 'table part3'>
                  <div class = 'row'>
                    <div class = 'cell section'> PART 3 <span class = 'subtitle'> SYNONYMS          </span></div>
                  </div>
                  <div class = 'row part3'>
                    <div class = 'cell part3'>
                      <?php for ($i = 51 ; $i < 66; $i++){?>
                        <div> <span class = 'number'><?php echo $i;?> </span>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "1" ><span>A</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "2" ><span>B</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "3" ><span>C</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "4" ><span>D</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "5" ><span>E</span> </label></div>
                      <?php }?>
                    </div>
                  </div>
                </div>
                
                
                <div class = 'table part4'>
                  <div class = 'row'>
                    <div class = 'cell section'> PART 4 <span class = 'subtitle'> MATCHING QUESTIONS    </span></div>
                  </div>
                  <div class = 'row part4'>
                    <div class = 'cell'>
                      <?php for ($i = 66 ; $i < 91; $i++){?>
                        <div> <span class = 'number'><?php echo $i;?> </span>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "1" ><span>A</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "2" ><span>B</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "3" ><span>C</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "4" ><span>D</span> </label>
                          <label class = 'circle'><input type = "radio" name= "<?php echo $i;?>" value = "5" ><span>E</span> </label></div>
                      <?php }?>
                    </div>
                  </div>
                </div>
                
                
                <div class = 'table part5'>
                  <div class = 'row'>
                    <div class = 'cell section'> PART 5 <span class = 'subtitle'> SENTENCE COMPLETIONS    </span></div>
                  </div>
                  <div class = 'row part5'>
                    <div class = 'cell'>
                      <?php for ($i = 91; $i < 101; $i++){?>
                        <div> <span class = 'number'><?php if ($i!= 100){echo $i.'&ensp;';} else{echo$i;}?> </span>
                          <label class = 'circle part5'><input type = "radio" name= "<?php echo $i;?>" value = "1" ><span>A</span> </label>
                          <label class = 'circle part5'><input type = "radio" name= "<?php echo $i;?>" value = "2" ><span>B</span> </label>
                          <label class = 'circle part5'><input type = "radio" name= "<?php echo $i;?>" value = "3" ><span>C</span> </label>
                          <label class = 'circle part5'><input type = "radio" name= "<?php echo $i;?>" value = "4" ><span>D</span> </label>
                          <label class = 'circle part5'><input type = "radio" name= "<?php echo $i;?>" value = "5" ><span>E</span> </label></div>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        </form>
      </div>
    </div>
  </div>
<script type = "text/javascript">
          var SetTime = 1800;   // 최초 설정 시간(기본 : 초)

          function msg_time() { // 1초씩 카운트
            if((SetTime % 60)<10){  
              if((SetTime / 60)<10){ m = "0" + Math.floor(SetTime / 60) + " : 0" + (SetTime % 60); }  // 남은 시간 계산
              else { m = Math.floor(SetTime / 60) + " : 0" + (SetTime % 60);}
            }
            else{ 
              if((SetTime / 60)<10){ m =  "0" + Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
              else{ m =  Math.floor(SetTime / 60) + " : " + (SetTime % 60); }
            }
            
            var msg = "Time left " + m;
            
            document.all.countdown.innerHTML = msg;   // div 영역에 보여줌 
                
            SetTime--;          // 1초씩 감소
            
            if (SetTime < 0) {      // 시간이 종료 되었으면..
              clearInterval(tid);   // 타이머 해제
              alert("Time's up. The quiz is over.");
              document.form.submit();
            }
          }

          window.onload = function TimerStart(){ tid=setInterval('msg_time()',1000) };
          
          
          
          
          var beforeChecked = -1;

          $(function(){
            $(document).on("click", "input[type=radio]", function(e) {
              var index = $(this).parent().index("label");
         
              if(beforeChecked == index) { 
                beforeChecked = -1;
                $(this).prop("checked", false);
              }
              else{    
                beforeChecked = index;
              }
            });
          });
        </script>
@endsection('content')
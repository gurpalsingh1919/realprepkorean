@extends('layouts.user-app')
@section('content')
<style type="text/css">
  /*#container {
    position: relative;
    width: 1160px; 
    margin: 0 auto;
    padding: 10px 0;
}*/
.table .row{
    display: table-row !important;
    box-sizing: border-box;
}
</style>

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Quiz Review</b></h2>
      </div>
    </div>
    
    <div class="row layout-spacing">
      <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-10 col-md-10 col-sm-10 col-10 text-center">
                      <h2><b>Day {{$quiz}}</b><br/><span>{{ \Carbon\Carbon::parse($date)->format('F jS, Y')}}</span></h2>
                  </div>
                  <div class="col-xl-2 col-md-2 col-sm-2 col-2 text-center mt-3">
                      <a href="{{route('user.quiz')}}" class = 'nav-link formButton transparentformButton'><span class="flaticon-arrow-back"></span> Back </a>
                  </div>
              </div><hr/>
          </div>
           @inject('helper', 'App\Http\Helpers\helpers');
          
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <div class = 'review test'>
                <div class = 'table'>
                  <div class = 'row header'>
                    <div class = 'cell header part section'>
                      PART 1
                    </div>
                    <?php for ($i = 1; $i <26; $i++){?>
                      <div class = 'cell header'> {{$i}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part'>
                      Student
                    </div>
                    <?php for ($i = 1; $i <26; $i++){?>
                     
                      <div class = 'cell'> 
                        {{$helper->function_num_to_letter($ScoreQuiz->$i)}}
                      </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part answer'>
                      Answer
                    </div>
                    <?php for ($i = 1; $i <26; $i++){?>
                      <div class = 'cell answer'> {{$helper->function_num_to_letter($result_answer->$i)}} </div>
                    <?php }?>
                  </div>  
                    
                  <div class = 'row'>
                    <div class = 'cell part ox'>
                      O/X
                    </div>
                    <?php 
                    $count1 = 0;
                    for ($i = 1; $i <26; $i++){
                      if ($ScoreQuiz->$i == $result_answer->$i){ 
                        echo '<div class = "cell o"> O </div>';
                        ++$count1;
                      } 
                      else{ echo '<div class = "cell x"> X </div>';} ?>
                    <?php }?>
                  </div>  
                </div>
                
                <div class = 'table'>
                  <div class = 'row header'>
                    <div class = 'cell header  part section'>
                      PART 2
                    </div>
                    <?php for ($i = 26; $i <51; $i++){?>
                      <div class = 'cell header'> {{$i}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell  part'>
                      Student
                    </div>
                    <?php for ($i = 26; $i <51; $i++) {?>
                      <div class = 'cell'> {{$helper->function_num_to_letter($ScoreQuiz->$i)}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell  part answer'>
                      Answer
                    </div>
                    <?php for ($i = 26; $i <51; $i++){?>
                      <div class = 'cell answer'> {{$helper->function_num_to_letter($result_answer->$i)}} </div>
                    <?php }?>
                  </div>  
                    
                  <div class = 'row'>
                    <div class = 'cell  part ox'>
                      O/X
                    </div>
                    <?php 
                    $count2 = 0;
                    for ($i = 26; $i <51; $i++){
                      if ($ScoreQuiz->$i == $result_answer->$i){ 
                        echo '<div class = "cell o"> O </div>';
                        ++$count2;
                      } 
                      else{ echo '<div class = "cell x"> X </div>';} ?>
                    <?php }?>
                  </div>    
                </div>
                
                  
                <div class = 'table'>
                  <div class = 'row header part'>
                    <div class = 'cell header part section'>
                      PART 3
                    </div>
                    <?php for ($i = 51; $i <66; $i++){?>
                      <div class = 'cell header part3'> {{$i}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part'>
                      Student
                    </div>
                    <?php for ($i = 51; $i <66; $i++) {?>
                      <div class = 'cell'> {{$helper->function_num_to_letter($ScoreQuiz->$i)}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part answer'>
                      Answer
                    </div>
                    <?php for ($i = 51; $i <66; $i++){?>
                      <div class = 'cell answer'> {{$helper->function_num_to_letter($result_answer->$i)}} </div>
                    <?php }?>
                  </div>  
                    
                  <div class = 'row'>
                    <div class = 'cell part ox'>
                      O/X
                    </div>
                    <?php 
                    $count3 = 0;
                    for ($i = 51; $i <66; $i++){
                      if ($ScoreQuiz->$i == $result_answer->$i){ 
                        echo '<div class = "cell o"> O </div>';
                        ++$count3;
                      } 
                      else{ echo '<div class = "cell x"> X </div>';} ?>
                    <?php }?>
                  </div>              
                </div>
                
                <div class = 'table'>
                  <div class = 'row header part'>
                    <div class = 'cell header part section'>
                      PART 4
                    </div>
                    <?php for ($i = 66; $i <91; $i++){?>
                      <div class = 'cell header'> {{$i}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part'>
                      Student
                    </div>
                    <?php for ($i = 66; $i <91; $i++) {?>
                      <div class = 'cell'> {{$helper->function_num_to_letter($ScoreQuiz->$i)}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part answer'>
                      Answer
                    </div>
                    <?php for ($i = 66; $i <91; $i++){?>
                      <div class = 'cell answer'> {{$helper->function_num_to_letter($result_answer->$i)}} </div>
                    <?php }?>
                  </div>  
                    
                  <div class = 'row'>
                    <div class = 'cell part ox'>
                      O/X
                    </div>
                    <?php 
                    $count4 = 0;
                    for ($i = 66; $i <91; $i++){
                     if ($ScoreQuiz->$i == $result_answer->$i){  
                        echo '<div class = "cell o"> O </div>';
                        ++$count4;
                      } 
                      else{ echo '<div class = "cell x"> X </div>';} ?>
                    <?php }?>
                  </div>  
                </div>
                
                <div class = 'table'>
                  <div class = 'row header part'>
                    <div class = 'cell header part section'>
                      PART 5
                    </div>
                    <?php for ($i = 91; $i <101; $i++){?>
                      <div class = 'cell header part5'> {{$i}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part'>
                      Student
                    </div>
                    <?php for ($i = 91; $i <101; $i++) {?>
                      <div class = 'cell'> {{$helper->function_num_to_letter($ScoreQuiz->$i)}} </div>
                    <?php }?>
                  </div>
                  
                  <div class = 'row'>
                    <div class = 'cell part answer'>
                      Answer
                    </div>
                    <?php for ($i = 91; $i <101; $i++){?>
                      <div class = 'cell answer'> {{$helper->function_num_to_letter($result_answer->$i)}} </div>
                    <?php }?>
                  </div>  
                    
                  <div class = 'row'>
                    <div class = 'cell part ox'>
                      O/X
                    </div>
                    <?php 
                    $count5 = 0;
                    for ($i = 91; $i <101; $i++){
                      if ($ScoreQuiz->$i == $result_answer->$i){ 
                        echo '<div class = "cell o"> O </div>';
                        ++$count5;
                      } 
                      else{ echo '<div class = "cell x"> X </div>';} ?>
                    <?php }?>
                  </div>  
                </div>
                  
                    <div class = 'summary test'>
                      <div class = 'table'>
              <div class = 'row header'>
                <div class = 'cell header section'> PART 1 <br> <span class = 'subtitle'> Find the Corrent Meaning </span></div>
                <div class = 'cell header section'> PART 2 <br> <span class = 'subtitle'> Find the Correct Word </span></div>
                <div class = 'cell header section'> PART 3 <br> <span class = 'subtitle'> Synonyms </span></div>
                <div class = 'cell header section'> PART 4 <br> <span class = 'subtitle'> Matching questions </span></div>
                <div class = 'cell header section'> PART 5 <br> <span class = 'subtitle'> Sentence Completions </span></div>
                <div class = 'cell header score'>   SCORE </div>
              </div>
              <div class = 'row'>
                <div class = 'cell'> <div class = 'count'> <?php echo $count1;?></div> <div class = 'section_score'> /25 </div></div>
                <div class = 'cell'> <div class = 'count'> <?php echo $count2;?></div> <div class = 'section_score'> /25 </div></div>
                <div class = 'cell'> <div class = 'count'> <?php echo $count3;?></div> <div class = 'section_score'> /15 </div></div>
                <div class = 'cell'> <div class = 'count'> <?php echo $count4;?></div> <div class = 'section_score'> /25 </div></div>
                <div class = 'cell'> <div class = 'count'> <?php echo $count5;?></div> <div class = 'section_score'> /10 </div></div>
                <div class = 'cell count_score'> {{$ScoreQuiz->score}}    </div>
              </div>
            </div>
                    </div>
                
                  </div>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php function function_ox_return($answer_number, $test_number){
  
  if(strpos($answer_number, ',,') === false){
    if(strpos($answer_number,$test_number) !== false){
      return 'O';
    }
    else{ return 'X';}
  }
  else{//나중엔 부등호 양측 말고 홀수가 되도 for문 잘 돌릴 수 있게 변경
    $exstr = explode(',,',$answer_number);
    
    
    for ($k = 0 ; $k < count($exstr); $k++){//정답 분수 나누고 float 처리
      if (strpos($exstr[$k], '/') !== false){
        $exline = explode('/',$exstr[$k]);
        $exstr[$k] = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
      }
      else{ $exstr[$k] = (float)$exstr[$k];}//정답 소수 float 처리
    }

    if (strpos($test_number,'/') !== false){//학생답 분수 나누고 float 처리
        $exline = explode('/',$test_number);
        $test_number = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
    }
    else{ $test_number = (float)$test_number;}//학생답 소수 float 처리

    
    for($j = 0 ; $j < count($exstr); $j+=2){
      if (($exstr[$j] < $test_number) && ($test_number < $exstr[$j+1])){
        return 'O';
        break;
      }
      else{ return 'X';}
    }
  } 
}?>
@endsection('content')
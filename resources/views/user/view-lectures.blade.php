@extends('layouts.user-app')
@section('content')
<style type="text/css">
  .speed {
    margin-top: -20px;
    margin-bottom: 10px;
    vertical-align: middle;
}
.speed .speed_button {
    width: 80px;
    margin: 0 10px;
}
.view_lecture .video {
    height: 80vh;
}
.view_lecture .cell {
    vertical-align: middle;
}
.view_lecture.table {
    text-align: center;
    height: calc(100% + -60px);
}
#container {
    position: relative;
    /*width: 1160px;*/
    margin: 0 auto;
    padding: 10px 0;
}
.table {
    display: table;
    box-sizing: border-box;
}
</style>
<div id="content" class="main-content">
   <div class="container">
      <div class="page-header">
         <div class="page-title">
            <h2><b>My Lectures</b></h2>
         </div>
      </div>
      <div class="row" id="cancel-row">
        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-10 col-md-10 col-sm-10 col-10 text-center mt-2">
                           @if($student->lecture !=0)
                              <h2><b>Today's Lecture</b></h2>
                           @else
                              <h2><b>Review <span class = 'test_no'> TEST {{$student->review}}</span></b></h2>
                           @endif
                        </div>   
                        <div class="col-xl-2 col-md-2 col-sm-2 col-2 text-center mt-3">
                          <a href="{{route('user.my_lectures')}}" class = 'nav-link formButton transparentformButton'><span class="flaticon-arrow-back"></span> Back </a>
                        </div>                    
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="pricing-section bg-11">
                                <div id="container" class = 'view_lecture table' >
                                  <div class = 'cell'>
                                    <div class = 'speed'>
                                      <button onclick = 'setPlaySpeedNormal()'  type = 'button' class = 'btn btn-gradient-warning mr-2'> Normal  </button>
                                      <button onclick = 'setPlaySpeedTwo()'     type = 'button' class = 'speed_button btn btn-gradient-primary'> 1.25  </button>
                                      <button onclick = 'setPlaySpeedFive()'    type = 'button' class = 'speed_button btn btn-gradient-primary'> 1.5   </button>
                                      <button onclick = 'setPlaySpeedSeven()'   type = 'button' class = 'speed_button btn btn-gradient-primary'> 1.75  </button>
                                      <button onclick = 'setPlaySpeedDouble()'  type = 'button' class = 'speed_button btn btn-gradient-primary'> 2.0   </button>
                                    </div>
                                    <?php if ($type == 'lecture'){ $path = "https://s3.ap-northeast-2.amazonaws.com/video.lecture/".$file.".mp4";}
                                    else{$path = "https://s3.ap-northeast-2.amazonaws.com/video.review/".$file.".mp4";}
                                    ?>
                                      <video id = "myVideo" src=<?php echo $path;?> class = 'video' controls controlsList="nodownload"> Your broswer does not support video system. Please contact RealPREP administrator to solve the issue.</video>
                                  </div>
                                </div>
                            </section>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
   </div>
</div>
<script type = "text/javascript">
      var vid = document.getElementById("myVideo");
      function setPlaySpeedNormal(){
        vid.playbackRate = 1;
      }
      function setPlaySpeedTwo(){
        vid.playbackRate = 1.25;
      }
      function setPlaySpeedFive(){
        vid.playbackRate = 1.5;
      }
      function setPlaySpeedSeven(){
        vid.playbackRate = 1.75;
      }
      function setPlaySpeedDouble(){
        vid.playbackRate = 2;
      }
      
    </script>
@endsection('content')
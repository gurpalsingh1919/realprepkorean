@extends('layouts.user-app')
@section('content')

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
       <!-- <div class="page-title">
          <h3>Lectures</h3>
       </div> -->
    </div>
    <div class="row mt-4" id="cancel-row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h2><b>Quizzes</b></h2>
                   
                  </div>                      
              </div>
          </div>
          <div class="widget-content widget-content-area">
            <div class="row">
              <div class="col-lg-12">
                <section class="pricing-section bg-11">
                    <div class="pricing pricing--norbu">
                       <div class = 'start test'>
                         
                          
                          <div class = "table test">  
                            <div class = 'row'>
                              <div class = 'cell no col-md-12 text-center'> Day <?php echo $quiz;?> <a href = "{{route('user.quiz')}}" class = 'close'>X</a></div>
                            </div>
                            <div class = 'row'>
                              <div class = 'cell content'>If you click the button below, your quiz will begin. Start the quiz when you are ready.</div><br/>
                            </div>
                            <div class = 'row'>
                              <div class = 'cell warning'>Warning: Once you click the 'GET STARTED' button, your access to this quiz is no longer available! <br> Do not reload or leave the page after the quiz begins.</div>
                            </div>
                            <div class = 'row mt-4'>
                              <div class = 'cell button col-md-12 text-center'>
                                <a href="{{route('user.load_quiz_omr',['type'=>$type,'quiz'=>$quiz])}}" class ='button'> GET STARTED </a></div>
                            </div>
                          </div>
                        </div>
                    </div>
                </section> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection('content')
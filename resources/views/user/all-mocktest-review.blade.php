@extends('layouts.user-app')
@section('content')

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Mock Test</b></h2>
      </div>
    </div>
    
    <div class="row layout-spacing">
      <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center">
                      <h2><b>Review</b></h2>
                  </div>
              </div><hr/>
          </div>
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Test</th>
                  <th><span class = 'rw'>Evidence Based </span> <br> Reading & Writing </th>
                  <th>Math</th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(count($scoreTests) >0)
                  @foreach($scoreTests as $test)
                    <tr class="tablerow">
                      <td>{{$loop->iteration}}</td>
                      <td class="user-name">{{ \Carbon\Carbon::parse($test->test_date)->format('F jS, Y')}}</td>
                      <td class="user-email">{{$test->test}}</td>
                      <td class="user-phone">{{$test->section_rw}}</td>
                      <td class="user-mobile">{{$test->section_m}}</td>
                      <td class="user-mobile">{{$test->total}}</td>
                      <td class="user-mobile nav-item">
                          <a href="{{route('user.test_review',['test'=>$test->test,'date'=>$test->test_date])}}" class = 'nav-link formButton transparentformButton'> Review </a>
                        </td>
                    </tr>
                   @endforeach
                @else
                  <tr colspan="5">No Result found !!</tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
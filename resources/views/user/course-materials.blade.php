@extends('layouts.user-app')
@section('content')

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Course Material</b></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
            <div class="row">
              <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                  <h2><b>My Course Materials</b></h2><hr/>
              </div>           
            </div>
          </div>
          <div class="widget-content widget-content-area">
            <table class="table-striped">
              <thead>
                <tr class="text-center">
                  <th span="1" style="width: 5%;">#</th>
                  <th span="1" style="width: 35%;">Name</th>
                  <th span="1" style="width: 20%;">Date</th>
                  <th span="1" style="width: 50%;">Action</th>
                </tr>
              </thead>
              <tbody>
                @if(count($allfiles) >0)
                  @foreach($allfiles as $file)
                    <tr class="tablerow">
                      <td>{{$loop->iteration}}</td>
                       <td>{{$file->name}}</td>
                      <td class="user-name">{{ \Carbon\Carbon::parse($file->date)->format('F jS, Y')}}</td>
                      
                      <td class="user-mobile nav-item d-flex text-center">
                          <a href="{{asset('user/file/course_material/'.$file->full_name)}}" target="_blank" class = 'button-lnk nav-link formButton transparentformButton'>View </a>
                          <a href="{{ route('user.download_course_material',['download'=>'pdf','file'=>$file->id]) }}" class = 'button-lnk nav-link formButton ml-2'>Download</a>
                        </td>
                    </tr>
                   @endforeach
                @else
                  <tr colspan="5">No Result found !!</tr>
                @endif
              </tbody>
            </table>
            <div class="widget-content widget-content-area text-center">
              <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                {!! $allfiles->links() !!}
              </ul>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
@extends('layouts.user-app')
@section('content')
<style type="text/css">
  /*#container {
    position: relative;
    width: 1160px; 
    margin: 0 auto;
    padding: 10px 0;
}*/
.table .row{
    display: table-row !important;
    box-sizing: border-box;
}
</style>

<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>Mock Test</b></h2>
      </div>
    </div>
    
    <div class="row layout-spacing">
      <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
          <div class="widget-header">
              <div class="row">
                  <div class="col-xl-10 col-md-10 col-sm-10 col-10 text-center">
                      <h2><b>Test {{$test}}</b><br/><span>{{ \Carbon\Carbon::parse($date)->format('F jS, Y')}}</span></h2>
                  </div>
                  <div class="col-xl-2 col-md-2 col-sm-2 col-2 text-center mt-3">
                      <a href="{{route('user.mock_test')}}" class = 'nav-link formButton transparentformButton'><span class="flaticon-arrow-back"></span> Back </a>
                  </div>
              </div><hr/>
          </div>
          <div class="widget-content widget-content-area">
            <table class="table-striped">
             <div class = 'review test'>
                    <!--<div class = 'header'>
                      <img src = "../image/test_report.PNG" class = 'img'>
                    </div>-->
                    <!-- <div class="col-xl-12"> <h2><b>{{Auth::user()->name}}</b></h2></div>
                    
                    <div class="col-md-12">
                      <h2><div class = 'day'> Test {{$test}} </div>
                      <div class = 'date'> {{$date}} </div></h2>
                    </div>
                       -->
                    <div class = 'table'>
                      <div class = 'row'>
                        <div class = 'cell header section'> Section #1</div>
                        <?php for ($i = 1 ; $i < 23; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 101 ; $i < 123; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 101 ; $i < 123; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 101 ; $i < 123; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }?>
                      </div>  
                        
                      <div class = 'row'>
                        <div class = 'cell header'> </div>
                        <?php for ($i = 23 ; $i < 45; $i++){?>
                          <div class = 'cell header'>  <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 123 ; $i < 145; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 123 ; $i < 145; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 123 ; $i < 145; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }?>
                      </div>  
                      
                      <div class = 'row'>
                        <div class = 'cell header'> </div>
                        <?php for ($i = 45 ; $i < 53; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }
                        for ($i = 0 ; $i < 14 ; $i++){?>
                          <div class ='cell empty' style = 'border-top: 1px solid white;'></div>
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 145 ; $i < 153; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }
                        for ($i = 0 ; $i < 14 ; $i++){?>
                          <div class ='cell empty'></div>
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 145 ; $i < 153; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }
                        for ($i = 0 ; $i < 14 ; $i++){?>
                          <div class ='cell empty'></div>
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 145 ; $i < 153; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?>  
                        <?php }
                        for ($i = 0 ; $i < 14 ; $i++){?>
                          <div class ='cell empty'></div>
                        <?php }?>
                      </div>  
                    </div>
                
                    <div class = 'table'> 
                      <div class = 'row'>
                        <div class = 'cell header section'> Section #2</div>
                        <?php for ($i = 1 ; $i < 23; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 201 ; $i < 223; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 201 ; $i < 223; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 201 ; $i < 223; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }?>
                      </div>  
                        
                      <div class = 'row'>
                        <div class = 'cell header'> </div>
                        <?php for ($i = 23 ; $i < 45; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 223 ; $i < 245; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 223 ; $i < 245; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 223 ; $i < 245; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }?>
                      </div>  
                    </div>
                    
                    
                    <div class = 'table'> 
                      <div class = 'row'>
                        <div class = 'cell header section'> Section #3 </div>
                        <?php for ($i = 1 ; $i < 21; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 301 ; $i < 316; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }
                        for ($i = 316 ; $i < 321 ; $i++){?>
                          <div class = 'cell math'> <?php echo $result[$i]; ?> </div>
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 301 ; $i < 316; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }
                        for ($i = 516 ; $i < 521 ; $i++){?>
                          <div class = 'cell math'> <?php echo htmlentities($result_answer->$i); ?>  </div>
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 301 ; $i < 316; $i++){
                          if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }
                        for ($i = 316 ; $i <321 ; $i++){
                          if (function_ox_return($answer[$i],$result[$i]) == 'O') {echo "<div class = 'cell math o'> O </div>";} else {echo "<div class = 'cell math x'> X </div>";} ?>
                        <?php }?>
                      </div>  
                    </div>
                    
                    <div class = 'table t4'> 
                      <div class = 'row'>
                        <div class = 'cell header section'> Section #4</div>
                        <?php for ($i = 1 ; $i < 23; $i++){?>
                          <div class = 'cell header'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 401 ; $i < 423; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 401 ; $i < 423; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }?> 
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> O/X </div> 
                        <?php for ($i = 401 ; $i < 423; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';} ?> 
                        <?php }?>
                      </div>  
                      
                    </div>
                    <div class = 'table'>
                      <div class = 'row'>
                        <div class = 'cell header header4'> </div>
                        <?php for ($i = 23 ; $i < 31; $i++){?>
                          <div class = 'cell header header_o'> <?php echo $i;?> </div> 
                        <?php }
                        for ($i = 31 ; $i < 39 ; $i++){?>
                          <div class = 'cell header header_s'> <?php echo $i;?> </div> 
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Student </div> 
                        <?php for ($i = 423 ; $i < 431; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div> 
                        <?php }
                        for ($i = 431 ; $i < 439 ; $i++){?>
                          <div class = 'cell'> <?php echo $result[$i]; ?> </div>
                        <?php }?>             
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section'> Answer </div> 
                        <?php for ($i = 423 ; $i < 431; $i++){?>
                          <div class = 'cell'> <?php echo $answer[$i]; ?> </div> 
                        <?php }
                        for ($i = 531 ; $i < 539 ; $i++){?>
                          <div class = 'cell'> <?php echo htmlentities($result_answer->$i); ?> </div>
                        <?php }?>
                      </div>
                      
                      <div class = 'row'>
                        <div class = 'cell section o'> O/X </div> 
                        <?php for ($i = 423 ; $i < 431; $i++){?>
                          <?php if ($result[$i] == $answer[$i]){ echo '<div class = "cell o"> O </div>';} else{ echo '<div class = "cell x"> X </div>';}  
                        }
                        for ($i = 431 ; $i <439 ; $i++){
                          if (function_ox_return($answer[$i],$result[$i]) == 'O') {echo "<div class = 'cell o'> O </div>";} else {echo "<div class = 'cell  x'> X </div>";} ?>
                        <?php }?>
                      </div>  
                    </div>
                  
                    <div class = 'summary test'>
                      <div class = 'table'>
                        <div class = 'row header'>
                          <div class = 'cell header raw'> Reading <br> <span class = 'subtitle'> Raw score </span></div>
                          <div class = 'cell header raw'> Writing & Language <br> <span class = 'subtitle'> Raw score </span></div>
                          <div class = 'cell header raw'> Math <br> <span class = 'subtitle'> Raw score </span></div>
                          <div class = 'cell header subject rw'> <span class = 'subtitle rw'> Evidence-based </span><br> Reading & Writing</div>
                          <div class = 'cell header subject'> Math </div>
                          <div class = 'cell header score'>   TOTAL SCORE </div>
                        </div>
                        <div class = 'row'>
                          <div class = 'cell scores'> <div class = 'count'> <?php echo $raw_r;?></div> <div class = 'section_score'> /52 </div></div>
                          <div class = 'cell scores'> <div class = 'count'> <?php echo $raw_w;?></div> <div class = 'section_score'> /44 </div></div>
                          <div class = 'cell scores'> <div class = 'count'> <?php echo $raw_m;?></div> <div class = 'section_score'> /58 </div></div>
                          <div class = 'cell scores'> <div class = 'count'> <?php echo $rw;?></div> <div class = 'section_score'> /800 </div></div>
                          <div class = 'cell scores'> <div class = 'count'> <?php echo $m;?></div> <div class = 'section_score'> /800 </div></div>
                          <div class = 'cell scores count_score'> <?php echo $total;?>    </div>
                        </div>
                      </div>
                    </div>
                
                  </div>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php function function_ox_return($answer_number, $test_number){
  
  if(strpos($answer_number, ',,') === false){
    if(strpos($answer_number,$test_number) !== false){
      return 'O';
    }
    else{ return 'X';}
  }
  else{//나중엔 부등호 양측 말고 홀수가 되도 for문 잘 돌릴 수 있게 변경
    $exstr = explode(',,',$answer_number);
    
    
    for ($k = 0 ; $k < count($exstr); $k++){//정답 분수 나누고 float 처리
      if (strpos($exstr[$k], '/') !== false){
        $exline = explode('/',$exstr[$k]);
        $exstr[$k] = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
      }
      else{ $exstr[$k] = (float)$exstr[$k];}//정답 소수 float 처리
    }

    if (strpos($test_number,'/') !== false){//학생답 분수 나누고 float 처리
        $exline = explode('/',$test_number);
        $test_number = number_format((float)$exline[0]/(float)$exline[1],4,'.','');
    }
    else{ $test_number = (float)$test_number;}//학생답 소수 float 처리

    
    for($j = 0 ; $j < count($exstr); $j+=2){
      if (($exstr[$j] < $test_number) && ($test_number < $exstr[$j+1])){
        return 'O';
        break;
      }
      else{ return 'X';}
    }
  } 
}?>
@endsection('content')
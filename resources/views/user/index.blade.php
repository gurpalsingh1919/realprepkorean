@extends('layouts.user-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
      <div class="page-header">
         <div class="page-title">
            <h3>Dashboard</h3>
         </div>
      </div>
      <div class="row">
          @if($errors->all())
        @foreach ($errors->all() as $index=>$error)
         <div class="alert alert-danger">{{$error}}
          </div>
        @endforeach
      @endif
      @if(session('error'))
        <div class="error alert alert-danger alert-dismissable">
            <a href="#"  class="close" data-dismiss="alert"
               aria-label="close">&times;</a>
            <strong>Error : </strong> {!! session('error') !!}
        </div>
      @endif
      @if(session('success'))
        <div class="error alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a>
            {!! session('success') !!}
        </div>
      @endif

      </div>
      <div class="row layout-spacing ">
         <a href="{{route('user.my_lectures')}}" class="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
             <div class="widget-content-area  data-widgets br-4">
                 <div class="widget  t-sales-widget">
                     <div class="media">
                         <div class="icon ml-2">
                             <i class="flaticon-3d-cube"></i>
                         </div>
                         <div class="media-body text-right">
                             <p class="widget-text mb-0">My Lectures</p>
                             
                         </div>
                     </div>
                     <!-- <p class="widget-total-stats mt-2">94% New Sales</p> -->
                 </div>
             </div>
         </a>
         <a href="{{route('user.mock_test')}}" class="col-xl-3 mb-xl-0 col-lg-6 mb-4 col-md-6 col-sm-6">
             <div class="widget-content-area  data-widgets br-4">
                 <div class="widget  t-sales-widget">
                     <div class="media">
                         <div class="icon ml-2">
                             <i class="flaticon-table"></i>
                         </div>
                         <div class="media-body text-right">
                             <p class="widget-text mb-0">Mock Test</p>
                             <!-- <p class="widget-numeric-value">24,017</p> -->
                         </div>
                     </div>
                     <!-- <p class="widget-total-stats mt-2">552 New Orders</p> -->
                 </div>
             </div>
         </a>
         <a href="{{route('user.quiz')}}" class="col-xl-3 col-lg-6 col-md-6 col-sm-6 mb-sm-0 mb-4">
             <div class="widget-content-area  data-widgets br-4">
                 <div class="widget  t-sales-widget">
                     <div class="media">
                         <div class="icon ml-2">
                             <i class="flaticon-copy"></i>
                         </div>
                         <div class="media-body text-right">
                             <p class="widget-text mb-0">Daily Quiz</p>
                             <!-- <p class="widget-numeric-value">92,251</p> -->
                         </div>
                     </div>
                     <!-- <p class="widget-total-stats mt-2">390 New Customers</p> -->
                 </div>
             </div>
         </a>
         <a href="{{route('user.course_material')}}" class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
             
             <div class="widget-content-area  data-widgets br-4">
                 <div class="widget  t-sales-widget">
                     <div class="media">
                         <div class="icon" style="max-width: 30%;">
                             <i class="flaticon-mail-19"></i>
                         </div>
                         <div class="media-body text-right">
                             <p class="widget-text mb-0">Course Material</p>
                             <!-- <p class="widget-numeric-value">9.5 M</p> -->
                         </div>
                     </div>
                     <!-- <p class="widget-total-stats mt-2">$2.1 M This Week</p> -->
                 </div>
             </div>
         </a>
      </div>
      <div class="row">
        <div class="col-lg-6 layout-spacing">
          <div class="statbox widget box box-shadow">
            <div class="widget-header">
              <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                    <h2><b>Today's Test</b></h2><hr/>
                </div>           
              </div>
            </div>
            <div class="widget-content widget-content-area">
              <table class="table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Test</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                   @if(isset($student->test) && $student->test >0)
                    <tr class="tablerow">
                      <td>1</td>
                      <td class="user-email">Test {{$student->test}}</td>
                      <td class="user-mobile nav-item">
                        <form action = "{{route('user.start_mock_test')}}" method = "post" >
                          @csrf
                          <input type = 'hidden' name = 'type' value = 'test'           >
                          <input type = 'hidden' name = 'test' value = '{{$student->test}}'    >
                          <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                        </form>

                        
                      </td>
                    </tr>
                  @else
                    <tr class="tablerow"><td colspan="3" class="text-center" >No Test Assigned for Today !!</td></tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
             <div class="widget-header">
               <div class="row">
                 <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                     <h2><b>Today's Quizzes</b></h2><hr/>
                 </div>           
               </div>
             </div>
             <div class="widget-content widget-content-area">
               <table class="table-striped">
                 <thead>
                   <tr>
                     <th>#</th>
                     <th>Quiz</th>
                     <th>Action</th>
                   </tr>
                 </thead>
                 <tbody>
                   @if(isset($student->quiz1) && $student->quiz1 >0)
                     <tr class="tablerow">
                       <td>1</td>
                       <td class="user-email">Day {{$student->quiz1}}</td>
                       <td class="user-mobile nav-item">
                         <form action = "{{route('user.start_quiz')}}" method = "get" >
                          
                           <input type = 'hidden' name = 'type' value = 'quiz1'           >
                           <input type = 'hidden' name = 'quiz' value = '{{$student->quiz1}}'>
                           <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz1'> -->
                           <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                         </form>

                         
                       </td>
                     </tr>
                   @endif
                   @if(isset($student->quiz2) && $student->quiz2 >0)
                     <tr class="tablerow">
                       <td>2</td>
                       <td class="user-email">Day {{$student->quiz2}}</td>
                       <td class="user-mobile nav-item">
                         <form action = "{{route('user.start_quiz')}}" method = "get" >
                          
                           <input type = 'hidden' name = 'type' value = 'quiz2'>
                           <input type = 'hidden' name = 'quiz' value = '{{$student->quiz2}}'>
                           <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz2'> -->
                           <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                         </form>

                         
                       </td>
                     </tr>
                   @endif
                   @if(isset($student->quiz3) && $student->quiz3 >0)
                     <tr class="tablerow">
                       <td>3</td>
                       <td class="user-email">Day {{$student->quiz3}}</td>
                       <td class="user-mobile nav-item">
                         <form action = "{{route('user.start_quiz')}}" method = "get" >
                          
                           <input type = 'hidden' name = 'type' value = 'quiz3'           >
                           <input type = 'hidden' name = 'quiz' value = '{{$student->quiz3}}'    >
                           <!-- <input type = 'hidden' name = 'quiz_no'   value = 'quiz3'> -->
                           <button type = 'submit' class = 'button-lnk nav-link formButton transparentformButton'>Start</button>
                         </form>

                         
                       </td>
                     </tr>
                   @endif
                   @if((isset($student->quiz1) && $student->quiz1 =='0') && (isset($student->quiz2) && $student->quiz2 =='0') && (isset($student->quiz3) && $student->quiz3 =='0'))
                     <tr class="tablerow">
                        <td colspan="3" class="text-center" >No Quiz Assigned for Today !!</td></tr>
                   @endif 
                 </tbody>
               </table>
             </div>
           </div>
         </div>
      </div>
   </div>
</div>
@endsection('content')
@extends('layouts.user-app')
@section('content')
<div id="content" class="main-content">
  <div class="container">
    <div class="page-header">
      <div class="page-title">
        <h2><b>My Lecture's</b></h2>
      </div>
    </div>
    <div class="row mt-4" id="cancel-row">
      <div class="col-lg-12 layout-spacing">
          <div class="statbox widget box box-shadow">
              <div class="widget-header">
                  <div class="row">
                      <div class="col-xl-12 col-md-12 col-sm-12 col-12 text-center mt-2">
                         @if(isset($student->lecture) && $student->lecture !=0)
                            <h2><b>Today's lecture</h2><hr/>
                         @else
                            <h2><b>Review lecture<span class = 'test_no'> Test 
                              @if(isset($student->review))
                                {{$student->review}}
                              @endif
                              </span></b></h2><hr/>
                         @endif
                      </div>                      
                  </div>
              </div>
              <div class="widget-content widget-content-area">
                  <div class="row">
                      <div class="col-lg-12">
                         @if(isset($student->lecture) && $student->lecture !=0)
                          <section class="pricing-section bg-11">
                            <div class="pricing pricing--norbu">
                               <div class="pricing__item">
                                   <h3 class="pricing__title">READING</h3>
                                   <a  href = "{{route('user.view_lectures',['type'=>'lecture','file'=>$student->lecture])}}-1"  class="nav-link formButton transparentformButton pricing__action mx-auto mb-4">Start</a>
                               </div>
                               <div class="pricing__item pricing__item--featured">
                                   <h3 class="pricing__title">WRITING</h3>
                                  <a href = "{{route('user.view_lectures',['type'=>'lecture','file'=>$student->lecture])}}-2" class="nav-link formButton transparentformButton">Start</a>
                               </div>
                               <div class="pricing__item">
                                  <h3 class="pricing__title">Math</h3>
                                  <a href = "{{route('user.view_lectures',['type'=>'lecture','file'=>$student->lecture])}}-3" class="nav-link formButton transparentformButton">Start</a>
                               </div>
                              
                           </div>
                          </section> 
                          @else
                            <section class="pricing-section bg-11">
                              @if(isset($student->review))
                            <div class="pricing pricing--norbu">
                               <div class="pricing__item p-p-1">
                                   <h3 class="pricing__title">SECTION 1</h3>
                                   <p>READING</p>
                                   <a  href = "{{route('user.view_lectures',['type'=>'review','file'=>$student->review])}}-1"  class="nav-link formButton transparentformButton mb-4">Start</a>
                               </div>
                               <div class="pricing__item pricing__item--featured">
                                   <h3 class="pricing__title">SECTION 2</h3>
                                   <p>WRITING & LANGUAGE</p>
                                  <a href = "{{route('user.view_lectures',['type'=>'review','file'=>$student->review])}}-2" class="nav-link formButton transparentformButton mb-4">Start</a>
                               </div>
                               <div class="pricing__item">
                                  <h3 class="pricing__title">SECTION 3</h3>
                                  <p>MATH</p>
                                  <a href = "{{route('user.view_lectures',['type'=>'review','file'=>$student->review])}}-3" class="nav-link formButton transparentformButton mb-4">Start</a>
                               </div>
                               <div class="pricing__item">
                                  <h3 class="pricing__title">SECTION 4</h3>
                                  <p>MATH</p>
                                  <a href = "{{route('user.view_lectures',['type'=>'review','file'=>$student->review])}}-3" class="nav-link formButton transparentformButton mb-4">Start</a>
                               </div>
                              
                           </div>
                           @endif
                          </section> 

                          @endif   
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
@extends('layouts.home-app')
@section('content')
<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP ACADEMIC PROGRAMS</h1>
          <span>Introduction to the study program</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-7">
        <h3 class="mb-4">RealPrep's flagship program: Summer SAT Short-Term Intensive Camp In</h3>
        <p>2008, RealPrep (then'Real SAT') held its first summer short-term intensive program with 4 students. The summer SAT camp, which has already reached its 12th year, is for students who want to spend the hottest and most valuable summer in school, such as international students, international school students, students who have recently decided to study abroad, and students who want to take a leap in their skills. It has been established as a program that brings results.</p>

<p>RealPREP's summer SAT camp runs from morning to evening (9 to 22:00), 6 days a week, 8 weeks. It's obviously not an easy program, so it got the nickname of Boot Camp. In a complete learning environment without smartphones, computers, and TVs, spending time with devoted teachers and classmates looking at the same goal is a precious time that turns ordinary students into candidates, just as civilians become soldiers in training camps. is.</p>
      </div>
      <div class="col-md-5"> <div class="courseThumb">
        <img src="{{ asset('home/images/1_2_orig.jpg') }}" alt="" class="imgResponsive"></div> 
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
    	<div class="col-md-5"> <div class="courseThumb">
        <img src="{{ asset('home/images/sat-camp_orig.png') }}" alt="" class="imgResponsive"></div> 
      </div>
      <div class="col-md-7">
        <h3 class="mb-4">RealPrep's Ambitious Work: Summer CORE Camp In</h3>
        <p>2015, RealPrep was having a long time ago. I was wondering,'What should I do when I was in the lower grades? The result of that concern is the Summer CORE Camp.</p>

					<p>RealPREP's Summer CORE Camp is a short-term intensive program for lower grades, applying college seminar classes to basic humanities, reading, listening, speaking, and writing, as well as basic learning abilities such as reading, listening, speaking, and writing. Interaction experience, ability to ask and communicate with teachers, and basic math skills are the most basic, but that is the most important time for study.</p>

					<p>In addition, in order to protect the character and quality of life of students, the program is held from 9 to 18:00 so that they can spend evenings with their families. Are being implemented.</p>
      </div>
      
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
    	<div class="col-md-12">
        <h3 class="mb-4">As intensely as short: Winter SAT Short-Term Intensive Camp and CORE Camp</h3>
        <p>Students who want to spend a relatively short winter vacation most cherished, students who want to finish the SAT in March and focus on applications and other tests, and start the New Year with a fresh mind. This is a 4-week program prepared for students who want to. Although the time has been shortened, the quality of classes and programs is no different from summer programs.</p>
			</div>
    </div>
    <div class="row">
   		<div class="col-md-6"> 
   			<div class="courseThumb">
      		<img src="{{ asset('home/images/3_1_orig.jpg') }}" alt="" class="imgResponsive">
      	</div> 
      </div>
      <div class="col-md-6"> 
      	<div class="courseThumb">
      		<img src="{{ asset('home/images/sat-camp-1_orig.png') }}" alt="" class="imgResponsive">
    		</div> 
    	</div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
    	<div class="col-md-5"> <div class="courseThumb">
        <img src="{{ asset('home/images/5_1_orig.jpg') }}" alt="" class="imgResponsive"></div> 
      </div>
      <div class="col-md-7">
        <h3 class="mb-4">Small classes tailored to your needs: AP & SAT Subject Tests</h3>
        <p>RealPrep has a wide educational network, including AP World History, US History, Calculus, Physics, Chemistry, Biology and SAT Math2, US History, Physics, Chemistry, Biology, We cooperate with teachers in various subjects such as Chinese and French.</p>

					<p>In consideration of various factors such as student's disposition, level, goal, and given time, teachers who have verified their level and ability are selected and small-scale tutoring classes are customized at our school.</p>
      </div>
      
    </div>
  </div>
</section>
@endsection('content')
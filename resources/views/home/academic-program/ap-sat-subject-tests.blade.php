@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP YEAR ROUND AP & SAT SUBJECT TEST PROGRAM</h1>
          <span>AP & SAT Subject Tests Class Program</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">Small classes tailored to your needs: AP & SAT Subject Tests</h3>
        <p>RealPrep has a wide educational network, including AP World History, US History, Calculus, Physics, Chemistry, Biology and SAT Math2, US History, Physics, Chemistry, Biology, We cooperate with teachers in various subjects such as Chinese and French.</p>

        <p>In consideration of various factors such as student's disposition, level, goal, and given time, teachers who have verified their level and ability are selected and small-scale tutoring classes are customized at our school.</p>
      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('academic_program') }}">View other study program</a>
  </div>

</section>

@endsection('content')
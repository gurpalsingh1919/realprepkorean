@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP SUMMER SAT BOOT CAMP</h1>
          <span>Summer SAT Short-Term Intensive Camp</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">RealPrep's flagship program: Summer SAT Short-Term Intensive Camp In</h3>
        <p>2008, RealPrep (then'Real SAT') held its first summer short-term intensive program with 4 students. The summer SAT camp, which has already reached its 12th year, is for students who want to spend the hottest and most valuable summer in school, such as international students, international school students, students who have recently decided to study abroad, and students who want to take a leap in their skills. It has been established as a program that brings results.</p>

<p>RealPREP's summer SAT camp runs from morning to evening (9 to 22:00), 6 days a week, 8 weeks. It's obviously not an easy program, so it got the nickname of Boot Camp. In a complete learning environment without smartphones, computers, and TVs, spending time with devoted teachers and classmates looking at the same goal is a precious time that turns ordinary students into candidates, just as civilians become soldiers in training camps. is.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12"> 
        <h3 class="mb-4">Program sample timetable</h3>
        <div class="courseThumb">
          <img src="{{ asset('home/images/kakaotalk-photo-2019-06-11-07-26-42.png') }}" alt="" class="imgResponsive">
        </div> 
      </div>
      <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('academic_program') }}">View other study program</a>
    </div>
  </div>

</section>

@endsection('content')
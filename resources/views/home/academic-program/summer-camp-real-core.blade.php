@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP SUMMER CORE BOOT CAMP</h1>
          <span>Summer REAL CORE Camp</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">Real Prep's Ambitious Work: Summer REAL CORE Camp In</h3>
        <p>2015, Real Prep was having a long time ago. I was wondering,'What should I do when I was in the lower grades? The result of that worry is the summer REAL CORE camp.</p>

<p>RealPREP's summer REAL CORE camp is a short-term intensive program for the lower grades, applying college seminar classes to the basic humanities, reading, listening, speaking, writing, and basic learning skills such as reading, listening, speaking, and writing. It is the most basic, but the most important time to study, such as the experience of exchanging opinions, the ability to ask and communicate with teachers, and basic math skills.</p>

<p>In addition, in order to protect the character and quality of life of students, the program is held from 9 to 18:00 so that they can spend evenings with their families. Are being implemented.</p>
      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('academic_program') }}">View other study program</a>
  </div>

</section>

@endsection('content')
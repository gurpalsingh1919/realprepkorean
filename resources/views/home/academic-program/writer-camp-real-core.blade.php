@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP WINTER CORE BOOT CAMP</h1>
          <span>Winter Real Core Camp</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">As intensely as short: Winter SAT Short-Term Intensive Camp and REAL CORE Camp</h3>
        <p>Students who want to spend a relatively short winter break the most, students who want to finish the SAT in March and focus on applications and other tests, and start the New Year with a fresh mind. This is a 4 week program prepared for students who want to do it Although the time has been shortened, the quality of classes and programs is no different from summer programs.</p>
      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('academic_program') }}">View other study program</a>
  </div>

</section>

@endsection('content')
@extends('layouts.home-app')
@section('content')
<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">About US</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="aboutSection contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2 class="line">Think Behind the<br>
          Most Reliable<br>
          Company </h2>
      </div>
      <div class="col-md-7">
        <p>There are two types of Latin etymology for the word Education. 'Educare' means'to train' and'to make a shape by trimming clay,' and'Educere' means'to lead' and'to come out'. It is a concept that is in line with education that uses the teachings to teach and the spirit and the flesh.</p>
        <p>All study is difficult. But everyone has a desire to learn. Real PREP aims for an education that leads to the world so that children can be trained and taught with clear rules and systems, and at the same time, children can go outside their parents' arms. Separating outcomes and processes is not education, and it is not growth that separates grades and character. This is the idea of Real PREP.</p>
      </div>
    </div>
  </div>
</section>
<section class="background01 contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="padding150LR text-center line lineCenter">RealPrep for over 10 years, we have been privileged to offer efficient solutions in Education and Training.</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 flexCol">
        <div class="iconBlocks wow fadeInLeft">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-01.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">10+</div>
            <p class="mb-0"><em>Years of Experience</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-md-4 flexCol">
        <div class="iconBlocks">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-02.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">850+</div>
            <p class="mb-0"><em>Educational Programs</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-md-4 flexCol">
        <div class="iconBlocks wow fadeInRight">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-03.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">650+</div>
            <p class="mb-0"><em>Cosnulting Programs</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="aboutSection contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-5"> <img src="{{ asset('home/images/thumb-03.jpeg') }}" alt="" class="imgResponsive"> </div>
      <div class="col-md-7">
        <h2>Person</h2>
        <p>Unlike chicks that break their eggs on their own, humans need a lot of help to become a member of society. In order to have an interest in character and process, leading to successful college entrance results for children, good grades and applications, outstanding teachers and management with a clear educational philosophy are needed.</p>
        <p>Not only that. We need parents who believe in our philosophy and students who remember Real Prep as a good and grateful place. There is an African proverb that says,'To raise one child, you need one village.' RealPrep wants to be a senior to follow and a teacher to learn, and to parents to be a trusted neighbor and a trusted advisor.</p>
      </div>
    </div>
  </div>
</section>
<section class="background01 position-relative contentContainer text-center">
  <div class="container">
    <div class="row">
      <div class="col-xl-8 col-lg-10 col-md-12 m-auto">
        <h2 class="text-white mb-5 text-green">Testimonails</h2>
        <div id="testimonialSlider" class="carousel slide shadowBox wow bounceInUp" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="testimonial">
                <p class="">“Julie knows her stuff. There is no better way to get into the little details of what makes your essay work and what doesn’t work than with a professional hand-holding from her.” <span class="clientName mt-4">-Tasman (UC Berkeley, Computer Science Major)</span> </p>
              </div>
            </div>
            <div class="carousel-item">
              <div class="testimonial">
                <p class="">“Julie knows her stuff. There is no better way to get into the little details of what makes your essay work and what doesn’t work than with a professional hand-holding from her.” <span class="clientName mt-4">-Tasman (UC Berkeley, Computer Science Major)</span> </p>
              </div>
            </div>
            <div class="carousel-item">
              <div class="testimonial">
                <p class="">“Julie knows her stuff. There is no better way to get into the little details of what makes your essay work and what doesn’t work than with a professional hand-holding from her.” <span class="clientName mt-4">-Tasman (UC Berkeley, Computer Science Major)</span> </p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#testimonialSlider" role="button" data-slide="prev"><img src="{{ asset('home/images/icons/left-arrow.png') }}" /> </a> <a class="carousel-control-next" href="#testimonialSlider" role="button" data-slide="next"> <img src="{{ asset('home/images/icons/right-arrow.png') }}" /> </a> </div>
      </div>
    </div>
  </div>
</section>

@endsection('content')
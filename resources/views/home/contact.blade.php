@extends('layouts.home-app')
@section('content')
<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">Contact US</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="aboutSection contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-md-6">
        <h3>Please get in touch with us</h3>
        <div class="contactInfo">
          <address>
          <div class="contactInfoIcon"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
          <div class="contactInfoContent"> <strong>Pangyo Headquarters:</strong> (13524) Real Prep Language <br>
            9th floor of Prime Square Building,<br>
            652,
            
            Sampyeong-dong,<br>
            Bundang-gu, Seongnam-si, Gyeonggi-do<br>
            (Seongnam-5431, Gyeonggi-do)</div>
          <div class="clearfix"></div>
          </address>
          <address>
          <div class="contactInfoIcon"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
          <div class="contactInfoContent"> +031-698-4292<br>
            +031-698-4292</div>
          <div class="clearfix"></div>
          </address>
          <address>
          <div class="contactInfoIcon"> <i class="fa fa-envelope" aria-hidden="true"></i> </div>
          <div class="contactInfoContent"> realprep@naver.com</div>
          <div class="clearfix"></div>
          </address>
        </div>
      </div>
      <div class="col-lg-7 col-md-6">
        <h3>Questions, Comments or Concern?</h3>
        <div class="contactForm">
          <div class="row">
            <div class="col-md-12">
              <input type="text" value="" placeholder="Name" class="">
            </div>
            <div class="col-md-12">
              <input type="text" value="" placeholder="Email Address" class="">
            </div>
            <div class="col-md-12">
              <input type="text" value="" placeholder="Subject" class="">
            </div>
            <div class="col-md-12">
              <textarea class="" placeholder="Your Message"></textarea>
            </div>
            <div class="col-lg-12">
              <input type="submit" value="Send Message" class="submitButton mt-4">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mapBlock">
  <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d12678.881606078901!2d127.11192!3d37.396444!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1606998154323!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
@endsection('content')
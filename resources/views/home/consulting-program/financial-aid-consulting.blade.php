@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP FINANCIAL AID CONSULTING</h1>
          <span>Financial aid consulting</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">Financial Assistance Consulting</h3>
        <p>Financial Assistance Consulting is a service designed to ease the concerns of parents on behalf of the preparation and collection of documents required for the process of receiving scholarships and financial aid at the university. Even if it seems to be a simple document, it is often the case that there are tens of thousands of dollars in difference between several numbers. We have prepared to give both Korean students and students with difficult family circumstances an opportunity to receive the world's best education if they have the ability and motivation.</p>


      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('consulting_program') }}">View other consulting program</a>
  </div>

</section>

@endsection('content')
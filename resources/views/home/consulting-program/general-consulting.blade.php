@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP  REGULAR CONSULTING</h1>
          <span>General consulting</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">General Consulting</h3>
        <p>General Consulting is recommended for parents who need advice and management for student reports or special activities, excluding applications. It can be a process of seeking some advice from a student who is already doing well on your own, or it can be a process of making sure RealPrep is a trustworthy place without burden. RealPrep values ​​all inquiries from parents with gratitude.</p>


      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('consulting_program') }}">View other consulting program</a>
  </div>

</section>

@endsection('content')
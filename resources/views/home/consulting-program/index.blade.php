@extends('layouts.home-app')
@section('content')
<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP CONSULTING PROGRAMS</h1>
          <span>Introduction of Consulting Program</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-7">
        <h3 class="mb-4">The Standard and Peak of Study Abroad Application Consulting: The REAL Application Consulting</h3>
        <p>College Application is the most important step in completing a student's high school life and admissions efforts. Many companies provide a variety of services, from simple admission application agency of agent to overall essay consulting, but RealPrep's REAL Application Consulting has sublimated application consulting into an art rather than a service.</p>

        <p>The student moves the emotions of the admission officer and moves the emotions of the admissions officer into the student's mind by dissolving the process of growth, values, hardships and hardships, challenges and successes, dreams and ambitions, personality and service, interest and preparedness toward the college to which they apply, and the trend of the times. In order to create an application with the name of “RealPrep”, RealPrep requires application consulting members to write at least 10,000 words and refines it.</p>

        <p>We receive REAL consulting members from January and no longer receive them after September. This is because in order to ensure the best quality. They guide and manage application essays, GPA, special activities, and letters to teachers who need letters of recommendation.</p>

        <p>We guide students to thoroughly fulfill their responsibilities. Management should not be on behalf of. That is because we know that now is a special and important moment in a student's life. It is the promise of REAL application consulting, the most complete application, the most carefully packaged while unfolding all the possibilities that the child has now.</p>
      </div>
      <div class="col-md-5"> <div class="courseThumb">
        <img src="{{ asset('home/images/6_2_orig.jpg') }}" alt="" class="imgResponsive"></div> 
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
    	<div class="col-md-5"> <div class="courseThumb">
        <img src="{{ asset('home/images/7_3_orig.jpg') }}" alt="" class="imgResponsive"></div> 
      </div>
      <div class="col-md-7">
        <h3 class="mb-4">Tap Then it will take place: PREP Application Consulting</h3>
        <p>Many students are confronted with the reality of having to write applications after September after summer camp. Although we cannot promise complete management and completeness from start to finish as in REAL application consulting, we prepared PREP application consulting in response to the expectations of students and the support of parents.</p>

<p>While REAL application consulting is the work of maximizing and packaging students' possibilities, PREP is the work of packing and refining the reality brought by students. It is not difficult to plan a special activity on your own or communicate with a recommendation letter, but it is the best consulting package for students who think they need help in the essay writing process.</p>

<p>However, there is no compromise on the completeness of the application. This is because it is impossible without emotion and truth to engrave students in the minds of admission officers with 4 or 5 pages. This is the promise of PREP application consulting, which is the most beautifully packaged good application by synthesizing all the contents that the student has worked and accumulated.</p>
      </div>
      
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
    	<div class="col-md-12">
        <h3 class="mb-4">General Consulting and Financial Assistance Consulting: We will be your trusted advisor to parents.</h3>
        <p>General consulting is recommended to parents who need advice and management for student reports or special activities, excluding application forms. It can be a process of seeking some advice from a student who is already doing well on your own, or it can be a process of making sure RealPrep is a trustworthy place without burden. RealPrep values ​​all inquiries from parents with gratitude.</p>
        <p>Financial Aid Consulting is a service designed to ease the concerns of parents on behalf of the preparation and collection of documents required for the process of receiving scholarships and financial aid at the university. Even if it seems to be a simple document, it is often the case that there are tens of thousands of dollars in difference between several numbers. We have prepared to give both Korean students and students with difficult family circumstances an opportunity to receive the world's best education if they have the ability and motivation.</p>
			</div>
    </div>
    <div class="row">
   		<div class="col-md-6"> 
   			<div class="courseThumb">
      		<img src="{{ asset('home/images/8_2_orig.jpg') }}" alt="" class="imgResponsive">
      	</div> 
      </div>
      <div class="col-md-6"> 
      	<div class="courseThumb">
      		<img src="{{ asset('home/images/9_1_orig.jpg') }}" alt="" class="imgResponsive">
    		</div> 
    	</div>
    </div>
  </div>
</section>

@endsection('content')
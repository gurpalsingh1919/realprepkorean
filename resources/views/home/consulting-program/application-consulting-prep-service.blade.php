@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP PREP APPLICATION CONSULTING</h1>
          <span>PREP application consulting</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">Tap Then it will take place: PREP Application Consulting</h3>
        <p>Many students are confronted with the reality of having to write applications after September after summer camp. Although we cannot promise perfect management and completeness from start to finish as in REAL application consulting, we prepared PREP application consulting in response to the expectations of students and the support of parents.</p>

<p>While REAL application consulting is the work of maximizing and packaging students' possibilities, PREP is the work of packing and refining the reality brought by students. It is not difficult to plan a special activity on your own or communicate with a recommendation letter, but it is the best consulting package for students who think they need help in the essay writing process.</p>

<p>However, there is no compromise on the completeness of the application. This is because it is impossible without emotion and truth to engrave students in the minds of admission officers with 4 or 5 pages. This is the promise of PREP application consulting, which is the most beautifully packaged good application by synthesizing all the contents that the student has worked and accumulated.</p>
      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('consulting_program') }}">View other consulting program</a>
  </div>

</section>

@endsection('content')
@extends('layouts.home-app')
@section('content')

<section class="innerBanner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="wow fadeInLeft">
          <h1 class="text-uppercase text-green">RealPREP REAL APPLICATION CONSULTING</h1>
          <span>REAL application consulting</span>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <h3 class="mb-4">The Standard and Peak of Study Abroad Application Consulting: The REAL Application Consulting</h3>
        <p>College Application is the most important step in completing a student's high school life and admissions efforts. Many companies provide a variety of services, from simple admission application agency of agent to overall essay consulting, but RealPrep's REAL Application Consulting has sublimated application consulting into an art rather than a service.</p>

<p>The student moves the emotions of the admission officer and moves the emotions of the admissions officer into the student's mind by dissolving the process of growth, values, hardships and hardships, challenges and successes, dreams and ambitions, personality and service, interest and preparedness toward the college to which they apply, and the trend of the times. In order to create an application with the name of “RealPrep”, RealPrep requires application consulting members to write at least 10,000 words and refines it.</p>

<p>We accept REAL consulting members from January and no longer receive them after September. This is because in order to ensure the best quality. They guide and manage application essays, GPA, special activities, and letters to teachers who need letters of recommendation.</p>

<p>Students are encouraged to fulfill their responsibilities thoroughly. Management should not be on behalf of. That is because we know that now is a special and important moment in a student's life. It is the promise of REAL application consulting, the most complete application that is most carefully packaged while unfolding all the possibilities of the child now.</p>
      </div>
    </div>
    <a class="mt-4 mb-4 text-uppercase general-btn" href="{{ route('consulting_program') }}">View other consulting program</a>
  </div>

</section>

@endsection('content')
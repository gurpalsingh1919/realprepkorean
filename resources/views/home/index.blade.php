@extends('layouts.home-app')
@section('content')
<section class="banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-10">
        <div class="bannerContent wow fadeInLeft">
          <div class="smallHeading">Aims For An Education</div>
          <h1 class="text-uppercase text-green">Leads The World</h1>
          <p>Real PREP aims for an education that leads to the world so that children can be trained and taught with clear rules and systems, and at the same time, children can go.</p>
          <a href="#" class="customButton01">Explore More</a> </div>
      </div>
    </div>
  </div>
</section>
<section class="position-relative text-center contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>Real Preparation Service Information</h2>
        <p class="padding150LR">It is a concept that is in line with education that uses the teachings to teach and the spirit and the flesh. All study is difficult. But everyone has a desire to learn. </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 flexCol">
        <div class="ourServices shadowBox wow bounceInUp">
          <div><img src="{{ asset('home/images/thumb-01.jpeg') }}" alt="" class="imgResponsive" /></div>
          <div class="info">
            <h3>Acadamic Program</h3>
            <p>Separating outcomes and processes is not education, and it is not growth that separates grades and character. This is the idea of ​​Real PREP.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 flexCol">
        <div class="ourServices shadowBox wow bounceInUp">
          <div><img src="{{ asset('home/images/thumb-02.jpeg') }}" alt="" class="imgResponsive" /></div>
          <div class="info">
            <h3>Consulting Program</h3>
            <p>Real PREP aims for an education that leads to the world so that children can be trained and taught with clear rules and systems.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<section class="aboutSection background01 contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2 class="line">The Story Behind the<br>
          Most Reliable<br>
          Company </h2>
      </div>
      <div class="col-md-7">
        <p> There are two kinds of Latin etymology for the word Education. 'Educare' means'to train' and'to make a shape by trimming clay,' and'Educere' means'to lead' and'to come out'. It is a concept that is in line with education that uses the teachings to teach and the spirit and the flesh.</p>
        <p>All study is difficult. But everyone has a desire to learn. Real PREP aims for an education that leads to the world so that children can be trained and taught with clear rules and systems, and at the same time, children can go out of their parents' arms. Separating outcomes and processes.</p>
        <a href="#" class="customButton01">Read MorE</a> </div>
    </div>
  </div>
</section>
<section class="contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="padding150LR text-center line lineCenter">RealPrep for over 10 years, we have been privileged to offer efficient solutions in Education and Training.</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 flexCol">
        <div class="iconBlocks wow fadeInLeft">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-01.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">10+</div>
            <p class="mb-0"><em>Years of Experience</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-md-4 flexCol">
        <div class="iconBlocks">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-02.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">850+</div>
            <p class="mb-0"><em>Educational Programs</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="col-md-4 flexCol">
        <div class="iconBlocks wow fadeInRight">
          <div class="iconThumb float-left"><img src="{{ asset('home/images/icons/icon-03.png') }}" alt=""></div>
          <div class="float-left stepInfo">
            <div class="stepNumber">650+</div>
            <p class="mb-0"><em>Cosnulting Programs</em></p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="background02 position-relative contentContainer text-center">
  <div class="container">
    <div class="row">
      <div class="col-xl-8 col-lg-10 col-md-12 m-auto">
        <h2 class="text-white">How We Can Help?</h2>
        <p class="highLightButton">Ask yourself below questions:</p>
        <div id="questionSlider" class="carousel slide shadowBox wow bounceInUp" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="questions">
                <h4 class="">Are you facing a challenge to your educations, Lorem ipsum dolor sit amet sit constdse?</h4>
              </div>
            </div>
            <div class="carousel-item">
              <div class="questions">
                <h4 class="">Are you facing a challenge to your educations, Lorem ipsum dolor sit amet sit constdse?</h4>
              </div>
            </div>
            <div class="carousel-item">
              <div class="questions">
                <h4 class="">Are you facing a challenge to your educations, Lorem ipsum dolor sit amet sit constdse?</h4>
              </div>
            </div>
            <div class="carousel-item">
              <div class="questions">
                <h4 class="">Are you facing a challenge to your educations, Lorem ipsum dolor sit amet sit constdse?</h4>
              </div>
            </div>
            <div class="carousel-item">
              <div class="questions">
                <h4 class="">Are you facing a challenge to your educations, Lorem ipsum dolor sit amet sit constdse?</h4>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#questionSlider" role="button" data-slide="prev">
            <img src="{{ asset('home/images/icons/left-arrow.png') }}" /> 
          </a> 
          <a class="carousel-control-next" href="#questionSlider" role="button" data-slide="next"> <img src="{{ asset('home/images/icons/right-arrow.png') }}" /> 
          </a> 
        </div>
      </div>
    </div>
  </div>
</section>
<section class="greyBg position-relative contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="smallOuterBox">
          <h2>Explore Our Gallery</h2>
          <p>It is a concept that is in line with education that uses the teachings to teach and the spirit and the flesh. All study is difficult. But everyone has a desire to learn. </p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-11 col-md-11 m-auto">
        <div class="gallerySlider shadowBox wow fadeInLeft">
          <div class="owl-carousel">
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-01.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-02.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-03.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-04.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-05.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
            <div class="item">
              <div class="galleryThumb"><img src="{{ asset('home/images/gallery-thumb-06.jpeg') }}" alt="image" class="imgResponsive" /></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="position-relative contentContainer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="ctaSection wow bounceInUp">
          <div class="ctaInfo">Are you facing a challenge to your educations?</div>
          <a href="#" class="getStartedButton">Click here to get started</a> </div>
      </div>
    </div>
  </div>
</section>
@endsection('content')
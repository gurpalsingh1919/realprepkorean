@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Assign Lecture & Review</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
      <div class="col-lg-12" >
       @if($errors->all())
        @foreach ($errors->all() as $index=>$error)
         <div class="alert alert-danger">{{$error}}
          </div>
        @endforeach
      @endif
      @if(session('error'))
        <div class="error alert alert-danger alert-dismissable">
            <a href="#"  class="close" data-dismiss="alert"
               aria-label="close">&times;</a>
            <strong>Error : </strong> {!! session('error') !!}
        </div>
      @endif
      @if(session('success'))
        <div class="error alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a>
            {!! session('success') !!}
        </div>
      @endif
    </div>
         <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Assign Lecture</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                 
          
                  <form action="{{route('admin.lecture_management')}}" method="post">
                    @csrf
                    <div class="form-group mb-4">
                      <label for="exampleFormControlInput1">Select Lecture</label><br/>
                      <select class="selectpicker" name="lecture_id"  data-actions-box="true">
                         @for($i=0;$i<10;$i++)
                          <option value="{{$i}}">{{$i}}</option>
                         @endfor
                      </select>
                     </div>
                     <div class="form-group mb-4">
                         <label for="exampleFormControlSelect1">Select Student</label><br/>
                          <select class="selectpicker" name="students[]" multiple data-actions-box="true">
                           @foreach($Users as $user)
                              <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                           @endforeach
                         </select>
                     </div>
                     
                     <input type="submit" name="lecture" value="Assign" class="mt-4 mb-4 btn btn-button-7">
                     <input type="submit" name="lecture" value="Cancel" class="mt-4 mb-4 btn btn-button-7">
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-6 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Assign Review</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                 <form action="{{route('admin.lecture_management')}}" method="post">
                    @csrf
                    <div class="form-group mb-4">
                      <label for="exampleFormControlInput1">Select Review</label><br/>
                      <select class="selectpicker" name="review_id"  data-actions-box="true">
                         @for($i=0;$i<20;$i++)
                          <option value="{{$i}}">{{$i}}</option>
                         @endfor
                      </select>
                     </div>
                     <div class="form-group mb-4">
                         <label for="exampleFormControlSelect1">Select Student</label><br/>
                          <select class="selectpicker" name="students[]" multiple data-actions-box="true">
                           @foreach($Users as $user)
                              <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                           @endforeach
                         </select>
                     </div>
                     
                     <input type="submit" name="review" value="Assign" class="mt-4 mb-4 btn btn-button-7">
                     <input type="submit" name="review" value="Cancel" class="mt-4 mb-4 btn btn-button-7">
                  </form>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Users List</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
         @if($errors->all())
          @foreach ($errors->all() as $index=>$error)
           <div class="alert alert-danger">{{$error}}
            </div>
          @endforeach
        @endif
        @if(session('error'))
          <div class="error alert alert-danger alert-dismissable">
              <a href="#"  class="close" data-dismiss="alert"
                 aria-label="close">&times;</a>
              <strong>Error : </strong> {!! session('error') !!}
          </div>
        @endif
        @if(session('success'))
          <div class="error alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert"
                 aria-label="close">&times;</a>
              {!! session('success') !!}
          </div>
        @endif
         <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                         <h4 class="text-center"><b>User's List</b><hr/></h4>
                     </div>
                     <div class="col-xl-3 col-md-3 col-sm-3 col-3 mt-3">
                         <a href="{{route('admin.add_user')}}" class = 'button-lnk nav-link formButton transparentformButton'>Add User</a>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Student Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($users as $user)
                           <tr>
                              <td>{{$loop->iteration}}</td>

                              <td>{{ucfirst($user->first_name .' '.$user->last_name) }}</td>
                               <td>{{ $user->email}}</td>
                              <td>
                                @if($user->role_id=='1')
                                 {{ 'Admin' }}
                                @elseif($user->role_id=='2')
                                 {{ 'Student' }}
                                @elseif($user->role_id=='3')
                                 {{ 'Teacher' }}
                                @endif

                              </td>
                              <td>
                               @if($user->status=='1')
                                 {{ 'Active' }}
                                @elseif($user->status=='2')
                                 {{ 'Inactive' }}
                                @endif
                              </td>
                              <td class="user-mobile nav-item d-flex text-center">
                                <a href="#" class = 'button-lnk nav-link formButton transparentformButton'>Edit </a>
                                
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="widget-content widget-content-area text-center">
                        <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                          {!! $users->links() !!}
                        </ul>
                    </div>
                  </div>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
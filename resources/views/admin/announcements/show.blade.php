@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Announcements</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
        <!-- <div class="col-lg-7 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                         <h4 class="text-center"><b>Announcements Listing</b><hr/></h4>
                     </div>
                     <div class="col-xl-2 col-md-2 col-sm-2 col-2 mt-2">
                         <a href="{{route('admin.announcement_list')}}" class = 'button-lnk nav-link formButton transparentformButton'>Back </a>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="alert alert-danger">{{$error}}
                    </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Title</th>
                              <th>Date</th>
                              <th class="text-center">Action</th>
                             
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($announcements as $announcement)
                           <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$announcement->title }}</td>
                              <td>{{ \Carbon\Carbon::parse($announcement->updated_at)->format('F jS, Y')}}</td>
                            
                              <td class="user-mobile nav-item d-flex text-center">
                                <a href="{{ route('admin.announcement_view',['id'=>$announcement->id]) }}"  class = 'button-lnk nav-link formButton transparentformButton'>View </a>
                                <a href="{{ route('admin.delete_announcement',['id'=>$announcement->id]) }}" onclick="return confirm('Are you sure to want delete this?')" class = 'button-lnk nav-link formButton ml-2'><span class="icon-trash"></span> Delete</a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="widget-content widget-content-area text-center">
                        <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                          {!! $announcements->links() !!}
                        </ul>
                      </div>
                  </div>
               </div>
            </div>
        </div> -->
        @if($Selectedboard !='')
          <div class="col-lg-12 col-md-12 layout-spacing">
            <div class="widget portlet-widget">
              <div class="widget-content widget-content-area">
                <div class="portlet portlet-warning">
                  <div class="portlet-title portlet-warning d-flex justify-content-between">
                    <div class="caption  align-self-center">
                      <span class="caption-subject text-uppercase white ml-1"> Announcement View</span>
                    </div>
                    <div class="actions  align-self-center">
                     <!--  <a href="{{ route('admin.edit_announcement',['id'=>$Selectedboard->id]) }}" class="btn btn-red btn-circle">
                        <i class="flaticon-edit-7"></i>
                      </a> -->
                      <a href="{{route('admin.announcement_list')}}" class = 'button-lnk nav-link formButton transparentformButton'>Back </a>
                      
                    </div>
                  </div>
                  <div class="portlet-body portlet-common-body">
                      <h4 class="text-center">{!! nl2Br($Selectedboard->title) !!}</h4>
                      <p class="mb-4 ml-5">{!! nl2Br($Selectedboard->content) !!}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif
     </div>
   </div>
</div>
@endsection('content')
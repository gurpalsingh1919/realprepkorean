@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
       <div class="page-title">
          <h3>Announcements</h3>
       </div>
     </div>
     <div class="row layout-spacing ">
        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-10 col-md-10 col-sm-10 col-10">
                         <h4 class="text-center"><b>Update Announcements</b><hr/></h4>
                     </div>
                     <div class="col-xl-2 col-md-2 col-sm-2 col-2 mt-2">
                         <a href="{{route('admin.announcement_list')}}" class = 'button-lnk nav-link formButton transparentformButton'>Back </a>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {{$error}}
                  </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
                   <form action="{{route('admin.update_announcement',$Selectedboard->id)}}" enctype="multipart/form-data" method="post">
                     @csrf
                     <div class="form-group mb-4 col-md-5">
                        <label for="title">Announcement title</label>
                         <input type="text" name="title" value="{{$Selectedboard->title}}" class="form-control" id="title">
                          @error('title')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                          </span>
                        @enderror
                     </div>
                     <div class="form-group mb-4 col-md-5">
                        <label for="Content">Content</label>
                         <textarea name="content" class="form-control" rows="10">{{$Selectedboard->content}}</textarea>
                          @error('content')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('content') }}</strong>
                          </span>
                        @enderror
                     </div>
                     <input type="submit" name="course" value="Update" class="mt-4 mb-4 btn btn-button-7">
                     
                  </form>
               </div>
            </div>
        </div>
     </div>
   </div>
</div>
@endsection('content')
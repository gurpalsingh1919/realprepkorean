@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Announcements</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                         <h4 class="text-center"><b>Announcements Listing</b><hr/></h4>
                     </div>
                     <div class="col-xl-3 col-md-3 col-sm-3 col-3 mt-3">
                         <a href="{{route('admin.add_announcement')}}" class = 'button-lnk nav-link formButton transparentformButton'>Add Announcements</a>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="alert alert-danger">{{$error}}
                    </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Title</th>
                              <th>Date</th>
                              <th class="text-center">Action</th>
                             
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($announcements as $announcement)
                           <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$announcement->title }}</td>
                              <td>{{ \Carbon\Carbon::parse($announcement->updated_at)->format('F jS, Y')}}</td>
                            
                              <td class="user-mobile nav-item d-flex text-center">
                                <a href="{{ route('admin.announcement_view',['id'=>$announcement->id]) }}" class = 'button-lnk nav-link formButton transparentformButton'>View </a>
                                <a href="{{ route('admin.delete_announcement',['id'=>$announcement->id]) }}" onclick="return confirm('Are you sure to want delete this?')" class = 'button-lnk nav-link formButton ml-2'><span class="icon-trash"></span> Delete</a>
                                <a href="{{ route('admin.edit_announcement',['id'=>$announcement->id]) }}" class = 'button-lnk nav-link formButton transparentformButton ml-2'>Edit </a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="widget-content widget-content-area text-center">
                        <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                          {!! $announcements->links() !!}
                        </ul>
                      </div>
                  </div>
               </div>
            </div>
        </div>
     </div>
   </div>
</div>
@endsection('content')
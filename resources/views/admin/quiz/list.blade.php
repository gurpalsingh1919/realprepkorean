@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Quiz List</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
         @if($errors->all())
          @foreach ($errors->all() as $index=>$error)
           <div class="alert alert-danger">{{$error}}
            </div>
          @endforeach
        @endif
        @if(session('error'))
          <div class="error alert alert-danger alert-dismissable">
              <a href="#"  class="close" data-dismiss="alert"
                 aria-label="close">&times;</a>
              <strong>Error : </strong> {!! session('error') !!}
          </div>
        @endif
        @if(session('success'))
          <div class="error alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert"
                 aria-label="close">&times;</a>
              {!! session('success') !!}
          </div>
        @endif
         <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>User's Quizzes List</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Student Name</th>
                            <th>Date</th>
                            <th>Quiz</th>
                            <th>Score</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($users_quizzes as $quiz)
                           <tr>
                              <td>{{$loop->iteration}}</td>

                              <td>{{ucfirst($quiz->userDetail['first_name'] .' '.$quiz->userDetail['last_name']) }}</td>
                               <td>{{ \Carbon\Carbon::parse($quiz->quiz_date)->format('F jS, Y')}}</td>
                              <td>Day {{$quiz->quiz}}</td>
                              <td>{{$quiz->score}}</td>
                              <td class="user-mobile nav-item d-flex text-center">
                                <a href="{{route('admin.quiz_review',['quiz'=>$quiz->quiz,'date'=>$quiz->quiz_date,'user'=>$quiz->user_id])}}" class = 'button-lnk nav-link formButton transparentformButton'>Review </a>
                                
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="widget-content widget-content-area text-center">
                        <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                          {!! $users_quizzes->links() !!}
                        </ul>
                    </div>
                  </div>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Assign Quiz</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
         <div class="col-lg-5 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Assign Quiz to User</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="alert alert-danger">{{$error}}
                    </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
          
                  <form action="{{route('admin.assign_quiz')}}" method="post">
                     @csrf
                     <div class="row">
                       <div class="form-group mb-4 col-md-4">
                           <label for="exampleFormControlInput1">Quiz 1</label>
                           <input type="number" name="quiz_1" class="form-control" id="quiz1" placeholder="Quiz 1">
                       </div>
                       <div class="form-group mb-4 col-md-4">
                           <label for="quiz2">Quiz 2</label>
                           <input type="number" name="quiz_2" class="form-control" id="quiz2" placeholder="Quiz 2">
                       </div>
                       <div class="form-group mb-4 col-md-4">
                           <label for="exampleFormControlInput1">Quiz 3</label>
                           <input type="number" name="quiz_3" class="form-control" id="quiz3" placeholder="Quiz 3">
                       </div>
                      </div>
                     <div class="form-group mb-4">
                         <label for="exampleFormControlSelect1">Select Student</label><br/>
                           <select class="selectpicker" name="students[]" multiple data-actions-box="true">
                           @foreach($Users as $user)
                              <option value="{{$user->id}}">
                                {{$user->user->first_name .' '.$user->user->last_name}}
                                ({{'Quiz1-'.$user->quiz1 .', Quiz2-'. $user->quiz2 .', Quiz3-'. $user->quiz3}})

                              </option>
                           @endforeach
                         </select>
                     </div>
                     
                     <input type="submit" name="quiz" value="Assign" class="mt-4 mb-4 btn btn-button-7">
                     <input type="submit" name="quiz" value="Cancel" class="mt-4 mb-4 btn btn-button-7">
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-7 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Recent Quizzes</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Student Name</th>
                            <th>Quiz</th>
                            <th>Score</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($user_recent_quiz as $recent_quiz)
                           <tr>
                              <td>{{$loop->iteration}}</td>

                              <td>{{$recent_quiz->userDetail['first_name'] .' '.$recent_quiz->userDetail['last_name'] }}</td>
                              <td>{{$recent_quiz->quiz}}</td>
                              <td>{{$recent_quiz->score}}</td>
                              
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
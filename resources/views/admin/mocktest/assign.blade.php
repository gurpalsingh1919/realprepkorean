@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Assign Mock Test</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
         <div class="col-lg-5 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Assign Test to User</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="alert alert-danger">{{$error}}
                    </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
          
                  <form action="{{route('admin.mocktest_management')}}" method="post">
                     @csrf
                     <div class="form-group mb-4">
                         <label for="exampleFormControlInput1">Please Enter Test number</label>
                         <input type="number" name="test_id" class="form-control" id="exampleFormControlInput1" placeholder="Test number">
                     </div>
                     <div class="form-group mb-4">
                         <label for="exampleFormControlSelect1">Select User</label><br/>
                          <select class="selectpicker" name="students[]" multiple data-actions-box="true">
                           @foreach($Users as $user)
                              <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                           @endforeach
                         </select>
                     </div>
                     
                     <input type="submit" name="mocktest" value="Assign" class="mt-4 mb-4 btn btn-button-7">
                     <input type="submit" name="mocktest" value="Cancel" class="mt-4 mb-4 btn btn-button-7">
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-7 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Recent Mock Tests</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Total</th>
                              <th>Reading & writing</th>
                              <th class="text-center">Math</th>
                             
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($recentUsers as $recent_user)
                           <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$recent_user->userDetail['first_name'] .' '.$recent_user->userDetail['last_name'] }}</td>
                              <td>{{$recent_user->total}}</td>
                              <td class="text-center">
                                {{$recent_user->section_rw}}
                              </td>
                              <td class="text-center">
                                 
                                 {{$recent_user->section_m}}
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
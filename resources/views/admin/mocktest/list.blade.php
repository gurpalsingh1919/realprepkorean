@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
         <div class="page-title">
             <h3>Mock Test</h3>
         </div>
     </div>

     <div class="row layout-spacing ">
         
         <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                         <h4 class="text-center"><b>Assigned Users mock test</b><hr/></h4>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                  <div class="table-responsive">
                     <table class="table table-bordered table-hover table-striped mb-4">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Date</th>
                              
                              <th>Reading & writing</th>
                              <th class="text-center">Math</th>
                              <th>Total</th>
                              <th class="text-center">Action</th>
                             
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($users as $user)
                           <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{ucfirst($user->userDetail['first_name'] .' '.$user->userDetail['last_name']) }}</td>

                               <td>{{ \Carbon\Carbon::parse($user->test_date)->format('F jS, Y')}}</td>

                              
                              <td class="text-center">
                                {{$user->section_rw}}
                              </td>
                              <td class="text-center">
                                 
                                 {{$user->section_m}}
                              </td>
                              <td>{{$user->total}}</td>
                              <td class="user-mobile nav-item d-flex text-center">
                                <a href="{{route('admin.test_review',['test'=>$user->test,'date'=>$user->test_date,'user'=>$user->user_id])}}" class = 'button-lnk nav-link formButton transparentformButton'>Review </a>
                                
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="widget-content widget-content-area text-center">
                        <ul class="pagination pagination-style-13 pagination-bordered justify-content-center mb-5">
                          {!! $users->links() !!}
                        </ul>
                    </div>
                  </div>
               </div>
            </div>
         </div>
     </div>
   </div>
</div>
@endsection('content')
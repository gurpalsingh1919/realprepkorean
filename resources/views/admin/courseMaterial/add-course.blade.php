@extends('layouts.admin-app')
@section('content')
<div id="content" class="main-content">
   <div class="container">
     <div class="page-header">
       <div class="page-title">
          <h3>Course Materials</h3>
       </div>
     </div>
     <div class="row layout-spacing ">
        <div class="col-lg-12 layout-spacing">
            <div class="statbox widget box box-shadow">
               <div class="widget-header">
                  <div class="row">
                     <div class="col-xl-10 col-md-10 col-sm-10 col-10">
                         <h4 class="text-center"><b>Add New Course Material</b><hr/></h4>
                     </div>
                     <div class="col-xl-2 col-md-2 col-sm-2 col-2 mt-2">
                         <a href="{{route('admin.course_list')}}" class = 'button-lnk nav-link formButton transparentformButton'>Back </a>
                     </div>
                  </div>
               </div>
               <div class="widget-content widget-content-area">
                @if($errors->all())
                  @foreach ($errors->all() as $index=>$error)
                   <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {{$error}}
                  </div>
                  @endforeach
                @endif
                @if(session('error'))
                  <div class="error alert alert-danger alert-dismissable">
                      <a href="#"  class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      <strong>Error : </strong> {!! session('error') !!}
                  </div>
                @endif
                @if(session('success'))
                  <div class="error alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert"
                         aria-label="close">&times;</a>
                      {!! session('success') !!}
                  </div>
                @endif
                   <form action="{{route('admin.add_course_post')}}" enctype="multipart/form-data" method="post">
                     @csrf
                     <div class="form-group mb-4 col-md-4">
                        <label for="coursefile">Please Upload File</label>
                         <input type="file" name="course_file" class="form-control" id="coursefile">
                          @error('course_file')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('course_file') }}</strong>
                          </span>
                        @enderror
                     </div>
                     <input type="submit" name="course" value="Upload" class="mt-4 mb-4 btn btn-button-7">
                     
                  </form>
               </div>
            </div>
        </div>
     </div>
   </div>
</div>
@endsection('content')
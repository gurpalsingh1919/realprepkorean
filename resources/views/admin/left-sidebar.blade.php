<div class="sidebar-wrapper sidebar-theme">
  <div id="dismiss" class="d-lg-none"><i class="flaticon-cancel-12"></i></div>
  <nav id="sidebar">
    <ul class="navbar-nav theme-brand flex-row  d-none d-lg-flex mt-4">
      <li class="nav-item">
        <a href="index.html" >
            <img src="{{ asset('home/images/logo/Logo-RP-KR-03.png') }}" 
            style="padding: 15px 30px 0px 30px;" class="img-fluid" alt="logo">
        </a>
      </li>
    </ul>


      <ul class="list-unstyled menu-categories" id="accordionExample">
          <li class="menu">
              <a href="{{route('admin.dashboard')}}" class="dropdown-toggle">
                  <div class="">
                      <i class="flaticon-computer-6 ml-3"></i>
                      <span>Dashboard</span>
                  </div>

                  <div>
                      <span class="badge badge-pill badge-secondary mr-2">6</span>
                  </div>
              </a>
             
          </li>
          <li class="menu">
              <a href="{{route('admin.assign_lecture')}}" class="dropdown-toggle">
                  <div class="">
                      <i class="flaticon-line-chart"></i>
                      <span>Lecture</span>
                  </div>
                  <!-- <div>
                      <i class="flaticon-right-arrow"></i>
                  </div> -->
              </a>
              
          </li>
          <li class="menu">
              <a href="#Users" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                  <div class="">
                      <i class="flaticon-users"></i>
                      <span>Users</span>
                  </div>
                  <div>
                      <i class="flaticon-right-arrow"></i>
                  </div>
              </a>
              <ul class="collapse submenu list-unstyled" id="Users" data-parent="#accordionExample">
                <li>
                  <a href="{{route('admin.users_list')}}"> Users List </a>
                </li>
                <li>
                  <a href="{{route('admin.add_user')}}">Add New User</a>
                </li>
            </ul>
            
          </li>
          <li class="menu">
              <a href="#quiz_list" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                  <div class="">
                      <i class="flaticon-elements"></i>
                      <span>Daily Quiz</span>
                  </div>
                  <div>
                      <i class="flaticon-right-arrow"></i>
                  </div>
              </a>
              <ul class="collapse submenu list-unstyled" id="quiz_list" data-parent="#accordionExample">
                <li>
                  <a href="{{route('admin.quiz_list')}}"> Users Assigned List </a>
                </li>
                <li>
                  <a href="{{route('admin.daily_quiz')}}">Assign Quizzes</a>
                </li>
            </ul>
            
          </li>

          <li class="menu">
              <a href="#moacktest" class="dropdown-toggle" data-toggle="collapse" aria-expanded="false" >
                  <div class="">
                      <i class="flaticon-3d-cube"></i>
                      <span>Mock Test</span>
                  </div>
                  <div>
                      <i class="flaticon-right-arrow"></i>
                  </div>
              </a>
              <ul class="collapse submenu list-unstyled" id="moacktest" data-parent="#accordionExample">
                <li>
                  <a href="{{route('admin.mocktest_list')}}"> Users Assigned List </a>
                </li>
                <li>
                  <a href="{{route('admin.assign_mocktest')}}">Assign Mock Test</a>
                </li>
            </ul>
          </li>

          <li class="menu">
            <a href="#course_materials" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
              <div class="">
                  <i class="flaticon-copy-line"></i>
                  <span>Course Material</span>
              </div>
              <div>
                  <i class="flaticon-right-arrow"></i>
              </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="course_materials" data-parent="#accordionExample">
              <li>
                <a href="{{route('admin.course_list')}}"> List </a>
              </li>
              <li>
                <a href="{{route('admin.add_course')}}"> Add New Course</a>
              </li>
            </ul>
          </li>

          <li class="menu">
              <a href="#Announcements" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                  <div class="">
                      <i class="flaticon-layers"></i>
                      <span>Announcements</span>
                  </div>
                  <div>
                      <i class="flaticon-right-arrow"></i>
                  </div>
              </a>
              <ul class="collapse submenu list-unstyled" id="Announcements" data-parent="#accordionExample">
              <li>
                <a href="{{route('admin.announcement_list')}}"> List </a>
              </li>
              <li>
                <a href="{{route('admin.add_announcement')}}"> Add New Announcement</a>
              </li>
            </ul>
            
          </li>
          <li class="menu">
            <a class="dropdown-toggle" href="{{ route('logout') }}" 
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <div class="">
                   <i class="mr-1 flaticon-power-button"></i>
                  <span>{{ __('Logout') }}</span>
              </div>

             
            </a>
             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
      </ul>
  </nav>
</div>
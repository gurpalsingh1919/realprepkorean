<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('home/images/RP-favicon.png') }}">
 <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ env('app.name', 'RealPREP | Improving Lives With Education SInce 2008 - Real PREP') }}</title>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
     <link rel="stylesheet" type="text/css" href="{{ asset('home/css/core.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('admin/assets/css/support-chat.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/plugins/charts/chartist/chartist.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/default-dashboard/style.css') }}" rel="stylesheet" type="text/css" />    
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->   
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{ asset('admin/assets/css/components/portlets/portlet.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('admin/assets/css/classic-dashboard/style.css') }}" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE FILE  -->
    
<style type="text/css">
   .table > thead > tr > th {
    color: #fff;
    font-weight: 600;
    background: #309940;
    border-radius: 5px;
}
.btn-button-7 {
    background: #309940 !important;
    color: #fff !important;
    border-color: #309940;
}
.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: 444px;
}
footer {
     padding: 0px !important;
    }
.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #309940;
    border-color: #309940;
}



ul.pagination.pagination-style-13 li a {
    background-color: #fff;
    color: #309940;
}
ul.pagination.pagination-style-13 li a:hover:not(.active) {
    background-color: #309940;
    color: #fff;
    border-color: #309940;
}
</style>
</head>
<body class="default-sidebar">

    <!-- Tab Mobile View Header -->
    <header class="tabMobileView header navbar fixed-top d-lg-none">
        <div class="nav-toggle">
                <a href="javascript:void(0);" class="nav-link sidebarCollapse" data-placement="bottom">
                    <i class="flaticon-menu-line-2"></i>
                </a>
            <a href="index.html" class=""> <img src="{{ asset('home/images/logo/Logo-RP-KR-03.png') }}" class="img-fluid" alt="logo"></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="nav-item d-lg-none"> 
                <form class="form-inline justify-content-end" role="search">
                    <input type="text" class="form-control search-form-control mr-3">
                </form>
            </li>
        </ul>
    </header>
    <!-- Tab Mobile View Header -->

    <!--  BEGIN NAVBAR  -->
    <header class="header navbar fixed-top navbar-expand-sm">
        <a href="javascript:void(0);" class="sidebarCollapse d-none d-lg-block" data-placement="bottom"><i class="flaticon-menu-line-2"></i></a>
        <div class="row ml-lg-auto">
          <div class="col-xl-12 col-md-12 col-sm-12 col-12">
              <h5 style="color: #309940"><span class="flaticon-idea-bulb"><b>  REALPREP ADMINISTRATOR</b></span></h5>
          </div>
        </div>
        <ul class="navbar-nav flex-row ml-lg-auto">
            <li class="nav-item dropdown user-profile-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="flaticon-user-12"></span>
                </a>
                <div class="dropdown-menu  position-absolute" aria-labelledby="userProfileDropdown">
                   <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <i class="mr-1 flaticon-power-button"></i> <span>{{ __('Logout') }}</span>
                    </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="cs-overlay"></div>

        <!--  BEGIN SIDEBAR  -->

        @include('admin.left-sidebar')
        <!--  END SIDEBAR  -->
        
        <!--  BEGIN CONTENT PART  -->
         @yield('content')
        
        <!--  END CONTENT PART  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!--  BEGIN CHAT  -->
    
    <!--  END CHAT  -->

    <!--  BEGIN FOOTER  -->
    <footer class="footer-section theme-footer">

        <div class="footer-section-1  sidebar-theme">
            
        </div>

        <div class="footer-section-2 container-fluid">
            <div class="row">
                <div id="toggle-grid" class="col-xl-7 col-md-6 col-sm-6 col-12 text-sm-left text-center">
                    <ul class="list-inline links ml-sm-5">
                        <!-- <li class="list-inline-item mr-3">
                            <a href="javascript:void(0);">Home</a>
                        </li>
                        <li class="list-inline-item mr-3">
                            <a href="javascript:void(0);">Blog</a>
                        </li>
                        <li class="list-inline-item mr-3">
                            <a href="javascript:void(0);">About</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript:void(0);">Buy</a>
                        </li> -->
                    </ul>
                </div>
                <div class="col-xl-5 col-md-6 col-sm-6 col-12">
                    <ul class="list-inline mb-0 d-flex justify-content-sm-end justify-content-center mr-sm-3 ml-sm-0 mx-3">
                        <li class="list-inline-item  mr-3">
                            <p class="bottom-footer">&#xA9; 2021 <a href="{{route('admin.dashboard')}}">RealPrep</a></p>
                        </li>
                        <li class="list-inline-item align-self-center">
                            <div class="scrollTop"><i class="flaticon-up-arrow-fill-1"></i></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--  END FOOTER  -->

    

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('admin/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('admin/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{ asset('admin/assets/js/custom.js') }}"></script>
    <script src="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <!-- <script src="{{ asset('admin/plugins/charts/chartist/chartist.js') }}"></script>
    <script src="{{ asset('admin/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/maps/vector/jvector/worldmap_script/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('admin/plugins/calendar/pignose/moment.latest.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/calendar/pignose/pignose.calendar.js') }}"></script>
    <script src="{{ asset('admin/plugins/progressbar/progressbar.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/default-dashboard/default-custom.js') }}"></script>
    <script src="{{ asset('admin/assets/js/support-chat.js') }}"></script> -->
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->


    <!--  BEGIN CUSTOM SCRIPT FILES  -->
    <script src="{{ asset('admin/plugins/charts/amcharts/amcharts.js') }}"></script>
    <script src="{{ asset('admin/plugins/maps/vector/ammaps/ammap_amcharts_extension.js') }}"></script>
    <script src="{{ asset('admin/plugins/maps/vector/ammaps/worldLow.js') }}"></script>
    <script src="{{ asset('admin/plugins/charts/amcharts/serial.js') }}"></script>
    <script src="{{ asset('admin/plugins/charts/amcharts/pie.js') }}"></script>
    <script src="{{ asset('admin/plugins/progressbar/progressbar.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/classic-dashboard/classic-custom.js') }}"></script>
    <!--  END CUSTOM SCRIPT FILES  -->
    
</body>
</html>
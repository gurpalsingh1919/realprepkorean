<!DOCTYPE html>
<html lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('home/images/RP-favicon.png') }}">
 <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ env('app.name', 'RealPREP | Improving Lives With Education SInce 2008 - Real PREP') }}</title>
   
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('home/css/core.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link href="{{ asset('user/assets/css/loader.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('user/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />

    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('user/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user/plugins/charts/chartist/chartist.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('user/assets/css/default-dashboard/style.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('user/plugins/pricing-table/css/demo.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user/plugins/pricing-table/css/component.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('user/plugins/table/datatable/datatables.css') }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('user/assets/css/ui-kit/custom-pagination.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user/assets/css/ui-kit/tabs-accordian/custom-accordions.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('user/plugins/table/sticky-table-header/component.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('user/assets/css/analytics-dashboard/style.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('user/assets/css/ecommerce-dashboard/timeline.css') }}" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
 /*   tr.tablerow
    {
        line-height: 40px;
        font-size: 18px;
        font-weight: bold;
    }
    .pricing-section a:hover, .pricing-section a:focus {
    color: #fff;
}
.pricing--norbu .pricing__item:hover a {
    background-color: #fff;
    color: #309940;
    border: 2px solid #309940;
    
}
.pricing--norbu .pricing__item:hover a:hover {
    background-color: #333;
    color: #fff;
    border: 2px solid #333;
    
}

.active i:after{
        position: absolute;
    content: '';
    height: 12px;
    width: 12px;
    background-color: #db0000;
    border-radius: 50%;
    top: 21px;
    right: 44px;
    z-index: 1;
    border: 2px solid #e9ecef;
}
.button-lnk.nav-link
{
    padding: 0px !important;
}

.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #309940;
    border-color: #309940;
}
ul.pagination.pagination-style-13 li a {
    background-color: #fff;
    color: #309940;
}
ul.pagination.pagination-style-13 li a:hover:not(.active) {
    background-color: #309940;
    color: #fff;
    border-color: #309940;
}
.creative2-accordion-content .card-header h5 .icon {
    color: #fff;
    background-color: #309940;
   
}
.sticky-wrap {
    margin: 0px;
}
.data-widgets .widget.t-sales-widget i {
    background-color: #309940;
    color: #fff;
}
.start .close {
    color: #309940;
    
}*/
</style>
<body>
    <div id="eq-loader">
      <div class="eq-loader-div">
          <div class="eq-loading dual-loader mx-auto mb-5"></div>
      </div>
    </div>
<?php $arr=array('user.load_mock_test','user.test_break','user.test_result','user.load_quiz_omr','user.quiz_result'); ?>
    <!--  BEGIN NAVBAR  -->
     @if( !in_array(Route::current()->getName(), $arr))
    <header class="desktop-nav header navbar fixed-top">
        <div class="nav-logo mr-sm-5 ml-sm-4">
            <a href="javascript:void(0);" class="nav-link sidebarCollapse d-inline-block mr-sm-5" data-placement="bottom">
                <i class="flaticon-menu-line-3"></i>
            </a>
            <a href="{{route('user.dashboard')}}" class=""> 
                <img src="{{ asset('home/images/Logo-RP-KR-001.png') }}" class="img-fluid" alt="logo" >
            </a>
        </div>
        
        <ul class="navbar-nav flex-row ml-lg-auto">

           <li class="nav-item dropdown user-profile-dropdown pl-4 pr-lg-0 pr-2 ml-lg-2 mr-lg-4  align-self-center">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user">
                    <div class="user-profile d-lg-block d-none">
                        <img src="{{ asset('home/images/icon-03.jpg') }}" alt="admin-profile" class="img-fluid">
                    </div>
                    <i class="flaticon-user-7 d-lg-none d-block"></i>
                </a>
            </li>
        </ul>
        
    </header>
    @endif
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
   @if( !in_array(Route::current()->getName(), $arr))
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="ps-overlay"></div>
        <div class="search-overlay"></div>
        
        <!--  BEGIN MODERN  -->
        
            @include('user.left-sidebar')
        @endif
        
        <!--  END MODERN  -->
        
        <!--  BEGIN CONTENT PART  -->
       
         @yield('content')
        <!--  END CONTENT PART  -->
    @if( !in_array(Route::current()->getName(), $arr))
    </div>
    @endif
    <!-- END MAIN CONTAINER -->

    <!--  BEGIN FOOTER  -->
    
     @if( !in_array(Route::current()->getName(), $arr))
    <footer class="footer-section theme-footer">

        <div class="footer-section-1  sidebar-theme">
            
        </div>

        <div class="footer-section-2 container-fluid">
            <div class="row">
                <div id="toggle-grid" class="col-xl-7 col-md-6 col-sm-6 col-12 text-sm-left text-center">
                    <!-- <ul class="list-inline links ml-sm-5">
                        <li class="list-inline-item">
                            <a target="_blank" href="#"></a>
                        </li>
                    </ul> -->
                </div>
                <div class="col-xl-5 col-md-6 col-sm-6 col-12">
                    <ul class="list-inline mb-0 d-flex justify-content-sm-end justify-content-center mr-sm-3 ml-sm-0 mx-3">
                        <li class="list-inline-item  mr-3">
                            <p class="bottom-footer">&#xA9; 2019 <a target="_blank" href="#">Realpreponline</a></p>
                        </li>
                        <li class="list-inline-item align-self-center">
                            <div class="scrollTop"><i class="flaticon-up-arrow-fill-1"></i></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--  END FOOTER  -->

    <!--  BEGIN PROFILE SIDEBAR  -->
    <aside class="profile-sidebar text-center">
        <div class="profile-content profile-content-scroll">
            <!-- <div class="usr-profile">
                <img src="assets/img/90x90.jpg" alt="admin-profile" class="img-fluid">
            </div>
            <p class="user-name mt-4 mb-4">Vincent Carpenter</p>
            <div class="">
                <div class="accordion" id="user-stats">
                    <div class="card">
                        <div class="card-header pb-4 mb-4" id="status">
                            <h6 class="mb-0" data-toggle="collapse" data-target="#user-status" aria-expanded="true" aria-controls="user-status"><i class="flaticon-view-3 mr-2"></i> Status <i class="flaticon-down-arrow ml-2"></i> </h6>
                        </div>
                        <div id="user-status" class="collapse show" aria-labelledby="status" data-parent="#user-stats">
                            <div class="card-body text-left">
                                <ul class="list-unstyled pb-4">
                                    <li class="status-online"><a href="javascript:void(0);">Online</a></li>
                                    <li class="status-away"><a href="javascript:void(0);">Away</a></li>
                                    <li class="status-no-disturb"><a href="javascript:void(0);">Not Disturb</a></li>
                                    <li class="status-invisible"><a href="javascript:void(0);">Invisible</a></li>
                                    <li class="status-offline"><a href="javascript:void(0);">Offline</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="user-links text-left">
                <ul class="list-unstyled">
                    <li><a href="#"><i class="flaticon-mail-22"></i> Inbox</a></li>
                    <li><a href="#"><i class="flaticon-user-11"></i> My Profile</a></li>
                    <li>
                        
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <i class="flaticon-power-off"></i>  <span>{{ __('Logout') }}</span>
                    </a>

                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </li>

                </ul>
            </div>
        </div>
    </aside>
    @endif
    <!--  BEGIN PROFILE SIDEBAR  -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('user/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('user/assets/js/loader.js') }}"></script>
    <script src="{{ asset('user/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('user/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('user/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('user/plugins/blockui/jquery.blockUI.min.js') }}"></script>
    <script src="{{ asset('user/assets/js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{ asset('user/assets/js/custom.js') }}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="{{ asset('user/plugins/charts/chartist/chartist.js') }}"></script>
    <script src="{{ asset('user/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('user/plugins/maps/vector/jvector/worldmap_script/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('user/plugins/calendar/pignose/moment.latest.min.js') }}"></script>
    <script src="{{ asset('user/plugins/calendar/pignose/pignose.calendar.js') }}"></script>
    <script src="{{ asset('user/plugins/progressbar/progressbar.min.js') }}"></script>
    <script src="{{ asset('user/assets/js/default-dashboard/default-custom.js') }}"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('user/plugins/table/sticky-table-header/jquery.ba-throttle-debounce.min.js') }}"></script>
    <script src="{{ asset('user/plugins/table/sticky-table-header/jquery.stickyheader.js') }}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('user/assets/js/analytics-dashboard/analytics-custom.js') }}"></script>
    
</body>
</html>
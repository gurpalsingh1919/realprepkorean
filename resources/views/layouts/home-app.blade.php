<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('home/images/RP-favicon.png') }}">
 <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ env('app.name', 'RealPREP | Improving Lives With Education SInce 2008 - Real PREP') }}</title>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('home/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('home/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('home/css/core.css') }}">
<link rel="stylesheet" href="{{ asset('home/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('home/css/animate.css') }}">
</head>
<style type="text/css">
  .bannerSliderInfo {
    float: left;
    max-width: 310px;
}
.bannerIcon {
    float: left;
    padding: 11px 25px 0px 0px;
}
.invalid-feedback {
    display: block;
   
}
.navbar-nav li:hover > ul.dropdown-menu {
    display: block;
}
.dropdown-submenu {
    position:relative;
}
.dropdown-submenu>.dropdown-menu {
        top: -4px;
    left: 100%;
    /* left: 10.2rem; */
    margin-top: -6px;
}

/* rotate caret on hover */
.dropdown-menu > li > a:hover:after {
    text-decoration: underline;
    transform: rotate(-90deg);
} 

</style>
<body>
<header class="bg-white">
  <nav class="navbar navbar-expand-lg">
    <div class="container"> <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('home/images/logo.png') }}" alt="" class="imgResponsive"></a>
      <button class="navbar-toggler orangeBg" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active"> <a class="nav-link" href="{{ url('/') }}">Homes</a> </li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('about_us') }}">About</a> </li>
          <li class="nav-item dropdown"> 
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" href="#">Programs</a>
            <ul class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownMenuLink">

              <li class="dropdown-submenu">
                <a class="dropdown-item dropdown-toggle" href="{{ route('academic_program') }}">Acadamic Program</a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="{{ route('academic_program.summer_camp_sat') }}">Summer Camp-SAT</a></li>
                  <li><a class="dropdown-item" href="{{ route('academic_program.summer_camp_realcore') }}">Summer Camp-REAL CORE</a></li>
                  <li><a class="dropdown-item" href="{{ route('academic_program.writer_camp_sat') }}">Writer Camp-SAT</a></li>
                  <li><a class="dropdown-item" href="{{ route('academic_program.writer_camp_realcore') }}">Witer Camp-REAL CORE</a></li>
                  <li><a class="dropdown-item" href="{{ route('academic_program.ap_sat_subject_tests') }}">AP & SAT Subject Tests</a></li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a class="dropdown-item dropdown-toggle" href="{{ route('consulting_program') }}">Consulting Program</a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="{{ route('consulting_program.application_consulting_real_service') }}">Application Consulting-REAL Service</a></li>
                  <li><a class="dropdown-item" href="{{ route('consulting_program.application_consulting_prep_service') }}">Application Consulting-PREP Service</a></li>
                  <li><a class="dropdown-item" href="{{ route('consulting_program.general_consulting') }}">General Consulting</a></li>
                  <li><a class="dropdown-item" href="{{ route('consulting_program.financial_aid_consulting') }}">Financial aid Consulting</a></li>
                  
                </ul>
              </li>

            </ul>
          </li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('news') }}">News</a> </li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('contact_us') }}">Contact</a> </li>
          @auth
            <li class="nav-item"> 
                               
            <a class="nav-link navButton" href="{{ route('logout') }}" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">{{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
          @else
            <li class="nav-item"> 
            <a class="nav-link navButton " href="{{route('login')}}">Login</a> 
          </li>
         <!--  <li class="nav-item"> 
            <a class="nav-link navButton transparentButton" href="{{route('register')}}">Register</a> 
          </li> -->
          @endauth
          
          
        </ul>
      </div>
    </div>
  </nav>
</header>
    @yield('content')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footerMenu">
          <ul>
            <li><i class="fa fa-phone" aria-hidden="true"></i> 031-698-4292</li>
            <li><i class="fa fa-envelope" aria-hidden="true"></i> Email: <a href="#">realprep@naver.com</a> <span>|</span></li>
            <li><a href="#">KakaoTalk: realprep</a><span>|</span></li>
            <li><a href="#">Line: realprep</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-12">
        <div class="addressInfo">
          <p>Representative Director: Hyojin Kang</p>
          <p>Business Registration Number: 699-86-00084</p>
          <p>Pangyo Headquarters: (13524) Real Prep Language Academy on the 9th floor of Prime Square Building, 652, </p>
          <p>Sampyeong-dong, Bundang-gu, Seongnam-si, Gyeonggi-do (Seongnam-5431, Gyeonggi-do)</p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="copyRight">&copy; Copyright 2020 RealPrep Co., Ltd. All rights reserved.</div>
      </div>
    </div>
  </div>
</footer>
<script src="{{ asset('home/js/jquery.min.js') }}"></script> 
<script src="{{ asset('home/js/bootstrap.min.js') }}"></script> 
<script>$('.carousel').carousel() </script> 
<script src="{{ asset('home/js/owl.carousel.min.js') }}"></script> 
<script src="{{ asset('home/js/core.js') }}"></script> 
<script src="{{ asset('home/js/wow.js') }}"></script> 
<script>
 new WOW().init();
</script>
</body>
</html>

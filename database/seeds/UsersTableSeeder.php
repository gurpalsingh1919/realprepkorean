<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
					'role_id' => '1',
					'first_name' => 'Admin',
					'last_name' => 'Admin',
					'status' => '1',
					'email' => 'admin@gmail.com',
					'password' => bcrypt('pass@admin'),
				]);

				DB::table('users')->insert([
					'role_id' => '2',
					'first_name' => 'User',
					'last_name' => 'User',
					'status' => '1',
					'email' => 'user@gmail.com',
					'password' => bcrypt('pass@user'),
				]);

				DB::table('users')->insert([
					'role_id' => '3',
					'first_name' => 'Manager',
					'last_name' => 'Manager',
					'status' => '1',
					'email' => 'manager@gmail.com',
					'password' => bcrypt('pass@manager'),
				]);
    }
}

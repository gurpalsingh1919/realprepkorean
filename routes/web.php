<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact', 'HomeController@contactUs')->name('contact_us');
Route::get('/about', 'HomeController@aboutus')->name('about_us');


/********************  academic program **************************/
Route::group(['prefix' => 'academic-program'], function () {


Route::get('/', 'HomeController@academicProgram')->name('academic_program');
Route::get('/summer-camp-sat', 'HomeController@summerCampSat')->name('academic_program.summer_camp_sat');
Route::get('/summer-camp-real-core', 'HomeController@summerCampRealCore')->name('academic_program.summer_camp_realcore');
Route::get('/writer-camp-sat', 'HomeController@writerCampSat')->name('academic_program.writer_camp_sat');
Route::get('/writer-camp-real-core', 'HomeController@writerCampRealcore')->name('academic_program.writer_camp_realcore');
Route::get('/ap-sat-subject-tests', 'HomeController@apSatSubjectTests')->name('academic_program.ap_sat_subject_tests');
	});

/********************  consulting program **************************/
Route::group(['prefix' => 'news'], function () {

Route::get('/', 'HomeController@news')->name('news');


	});


/********************  news **************************/
Route::group(['prefix' => 'consulting-program'], function () {


Route::get('/', 'HomeController@consultingProgram')->name('consulting_program');

 Route::get('/application-consulting-real-service', 'HomeController@applicationConsultingRealService')->name('consulting_program.application_consulting_real_service');
 Route::get('/application-consulting-prep-service', 'HomeController@applicationConsultingPrepService')->name('consulting_program.application_consulting_prep_service');
Route::get('/general-consulting', 'HomeController@generalConsulting')->name('consulting_program.general_consulting');
Route::get('/financial-aid-consulting', 'HomeController@financialAidConsulting')->name('consulting_program.financial_aid_consulting');

	});




Route::group(['as'=>'admin.','prefix' => 'admin','namespace'=>'Admin','middleware'=>['auth','admin']], function () {
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
		/************************* Users  ****************************************/
		Route::get('users-list', 'DashboardController@usersList')->name('users_list');
		Route::get('users/add', 'DashboardController@addNewUser')->name('add_user');
		Route::post('users/add', 'DashboardController@addNewUserPost')->name('add_user_post');
		/************************* Mock test *************************************/
		Route::get('assign-mocktest', 'DashboardController@assignMocktests')->name('assign_mocktest');
		Route::get('mocktest-list', 'DashboardController@mocktestList')->name('mocktest_list');
		Route::get('mocktest/review', 'DashboardController@MocktestReviews')->name('test_review');

		Route::post('mocktest-management', 'DashboardController@assignMocktestPost')->name('mocktest_management');
		/******************** Quiz management **********************************/
		Route::get('daily-quiz', 'DashboardController@dailyQuiz')->name('daily_quiz');
		Route::get('quiz-list', 'DashboardController@quizList')->name('quiz_list');
		Route::get('quiz-review', 'DashboardController@quizReview')->name('quiz_review');
		Route::post('assign-quiz', 'DashboardController@assignDailyQuiz')->name('assign_quiz');

		/************************ Course Action ***********************************/
		Route::get('course-material/list', 'DashboardController@courseMaterialList')->name('course_list');
		Route::get('course-material/add', 'DashboardController@addCourseMaterial')->name('add_course');
		Route::post('course-material/add', 'DashboardController@addCourseMaterialPost')->name('add_course_post');
		Route::get('course-material/delete/{id}', 'DashboardController@deleteCourse')->name('delete_course');
		/************************ Announcement actions ****************************/
		Route::get('announcement/list', 'DashboardController@announcementsListing')->name('announcement_list');
		Route::get('announcement/list/{id}', 'DashboardController@announcementsView')->name('announcement_view');
		Route::get('announcement/add', 'DashboardController@addAnnouncement')->name('add_announcement');
		Route::post('announcement/add', 'DashboardController@addAnnouncementPost')->name('add_announcement_post');
		Route::get('announcement/delete/{id}', 'DashboardController@deleteAnnouncement')->name('delete_announcement');
		Route::get('announcement/edit', 'DashboardController@announcementsEdit')->name('edit_announcement');
		Route::post('announcement/edit/{id}', 'DashboardController@addAnnouncementEditPost')->name('update_announcement');
		/********************************** Assign Lecture ***********************/
		Route::get('assign-lecture', 'DashboardController@assignLectures')->name('assign_lecture');
		
		Route::post('lecture-management', 'DashboardController@assignLecturesPost')->name('lecture_management');



});
Route::group(['as'=>'user.','prefix' => 'user','namespace'=>'User','middleware'=>['auth','user']], function () {
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
		Route::get('lectures', 'DashboardController@myLectures')->name('my_lectures');
		Route::get('view-lecture', 'DashboardController@ViewLecture')->name('view_lectures');

		Route::get('mock-test', 'DashboardController@mockTest')->name('mock_test');
		Route::get('mock-test-start', 'DashboardController@getStartTest')->name('start_mock_test');
		Route::get('mock-test/start', 'DashboardController@getLoadTest')->name('load_mock_test');
		Route::post('answer-check', 'DashboardController@answerCheck')->name('answer_check');
		Route::get('break', 'DashboardController@Testbreak')->name('test_break');
		Route::get('test-result', 'DashboardController@testResult')->name('test_result');

		Route::get('quiz', 'DashboardController@userDailyQuiz')->name('quiz');
		Route::get('quiz/start', 'DashboardController@startDailyQuiz')->name('start_quiz');
		Route::get('quiz/load-omr', 'DashboardController@loadQuizOmr')->name('load_quiz_omr');
		Route::post('quiz-submit', 'DashboardController@submitQuizAnswers')->name('quiz_answer_check');
		Route::get('quiz-result', 'DashboardController@quizResult')->name('quiz_result');
		Route::get('quiz-review', 'DashboardController@quizReview')->name('quiz_review');

		Route::get('mock-test/review', 'DashboardController@reviewTestResult')->name('test_review');
		Route::get('all-mock-test/review', 'DashboardController@allMocktestReviews')->name('all_test_review');
		Route::get('course-material', 'DashboardController@allCourseMaterials')->name('course_material');

		Route::get('course-material-pdf','DashboardController@downloadCourseMaterial')->name('download_course_material');
		Route::get('announcements', 'DashboardController@userAnnoucements')->name('announcements');


});